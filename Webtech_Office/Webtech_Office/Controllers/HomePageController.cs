﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    public class HomePageController : Controller
    {

        private CMS_DbContext db = new CMS_DbContext();


        // GET: HomePage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Banner()
        {
            return View();
        }

        public ActionResult Novidades()
        {
            return View();
        }

        public ActionResult Parceiros()
        {
            return View();
        }




        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<HOME_HomePageBanner> query = db.HOME_HomePageBanner
                .Include("HOME_HomePageBannerTexto")

                .Include("CONF_Idioma")
                .Where(m => m.HOME_HomePageBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.HOME_HomePageBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.HOME_HomePageBannerTexto.FirstOrDefault().Titulo
                        : m.HOME_HomePageBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.HOME_HomePageBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.HOME_HomePageBannerTexto.FirstOrDefault().Introducao
                        : m.HOME_HomePageBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.HOME_HomePageBannerTexto.FirstOrDefault().Link,
                    PDF = m.HOME_HomePageBannerTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarHomePageBanner()
        {
            HomePageBannerViewModel model = new HomePageBannerViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                HOME_HomePageBannerTexto HomePageTexto = new HOME_HomePageBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.HomePageBannerTexto.Add(HomePageTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarHomePageBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarHomePageBanner(HomePageBannerViewModel model)
        {


            model.HomePageBanner.DataCriacao = DateTime.Now;
            model.HomePageBanner.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.HomePageBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.HomePageBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (HOME_HomePageBannerTexto textos in model.HomePageBannerTexto)
            {

                textos.HOME_HomePageBanner = model.HomePageBanner;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.HomePageBanner.HOME_HomePageBannerTexto = model.HomePageBannerTexto;
            db.HOME_HomePageBanner.Add(model.HomePageBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesHomePageBanner(int? id)
        {
            HOME_HomePageBanner model = db.HOME_HomePageBannerTexto.Include("CONF_Idioma").Include("HOME_HomePageBanner")
               .Select(x => x.HOME_HomePageBanner).Where(c => c.Id == id).FirstOrDefault();



            HomePageBannerViewModel BannerModel = new HomePageBannerViewModel();
            BannerModel.HomePageBanner = model;
            BannerModel.HomePageBannerTexto = model.HOME_HomePageBannerTexto.ToList();
            BannerModel.Idioma = model.HOME_HomePageBannerTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesHomePageBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarHomePageBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HOME_HomePageBanner model = db.HOME_HomePageBannerTexto.Include("CONF_Idioma").Include("HOME_HomePageBanner")
              .Select(x => x.HOME_HomePageBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            HomePageBannerViewModel BannerModel = new HomePageBannerViewModel();
            BannerModel.HomePageBanner = model;
            BannerModel.HomePageBannerTexto = model.HOME_HomePageBannerTexto.ToList();
            BannerModel.Idioma = model.HOME_HomePageBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarHomePageBanner", BannerModel);


        }


        [HttpPost]
        public ActionResult EditarHomePageBanner(HomePageBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.HomePageBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.HomePageBannerTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update HOME_HomePageBanner SET  Activo = '" + model.HomePageBanner.Activo
                  + "'  WHERE ID= " + model.HomePageBanner.Id);

            /*
            db.HOME_HomePage.Attach(model.HomePage);
            db.Entry(model.HomePage).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (HOME_HomePageBannerTexto texto in model.HomePageBannerTexto)
            {


                db.Database.ExecuteSqlCommand("Update HOME_HomePageBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de HomePage");

                return PartialView("_EditarCategoriaHomePagePartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarHomePageBanner(int? id)
        {

            HOME_HomePageBanner model = db.HOME_HomePageBannerTexto.Include("CONF_Idioma").Include("HOME_HomePageBanner")
                   .Select(x => x.HOME_HomePageBanner).Where(c => c.Id == id).FirstOrDefault();



            HomePageBannerViewModel BannerModel = new HomePageBannerViewModel();
            BannerModel.HomePageBanner = model;
            BannerModel.HomePageBannerTexto = model.HOME_HomePageBannerTexto.ToList();
            BannerModel.Idioma = model.HOME_HomePageBannerTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarHomePageBannerPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult HomePageBannerImagem(int? id)
        {

            HOME_HomePageBannerTexto model = db.HOME_HomePageBannerTexto.Include("CONF_Idioma")
                .Where(m => m.HomePageBannerId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemHomePagePreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarHomePageBannerConfirmar(HomePageBannerViewModel model)
        {
            HOME_HomePageBanner HomePageBanner = db.HOME_HomePageBannerTexto.Include("CONF_Idioma").Include("HOME_HomePage")
                  .Select(x => x.HOME_HomePageBanner).Where(c => c.Id == model.HomePageBanner.Id).FirstOrDefault();

            foreach (var item in HomePageBanner.HOME_HomePageBannerTexto.ToList())
            {
                db.HOME_HomePageBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.HOME_HomePageBanner.Attach(HomePageBanner);

            db.HOME_HomePageBanner.Remove(HomePageBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        //Destaques
        public JsonResult ListaDestaques([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<HOME_Destaques> query = db.HOME_Destaques
                .Include("HOME_DestaquesTexto")

                .Include("CONF_Idioma")
                .Where(m => m.HOME_DestaquesTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.HOME_DestaquesTexto.FirstOrDefault().Titulo.Length <= 70 ? m.HOME_DestaquesTexto.FirstOrDefault().Titulo
                        : m.HOME_DestaquesTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.HOME_DestaquesTexto.FirstOrDefault().Introducao.Length <= 70 ? m.HOME_DestaquesTexto.FirstOrDefault().Introducao
                        : m.HOME_DestaquesTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.HOME_DestaquesTexto.FirstOrDefault().Link,
                    PDF = m.HOME_DestaquesTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarDestaques()
        {
            DestaquesViewModel model = new DestaquesViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                HOME_DestaquesTexto LojaTexto = new HOME_DestaquesTexto()
                {
                    CONF_Idioma = idioma
                };
                model.DestaquesTexto.Add(LojaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarDestaques", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarDestaques(DestaquesViewModel model)
        {


            model.Destaques.DataCriacao = DateTime.Now;
            model.Destaques.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.DestaquesTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.DestaquesTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (HOME_DestaquesTexto textos in model.DestaquesTexto)
            {

                textos.HOME_Destaques = model.Destaques;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.Destaques.HOME_DestaquesTexto = model.DestaquesTexto;
            db.HOME_Destaques.Add(model.Destaques);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesDestaques(int? id)
        {
            HOME_Destaques model = db.HOME_DestaquesTexto.Include("CONF_Idioma").Include("HOME_Destaques")
               .Select(x => x.HOME_Destaques).Where(c => c.Id == id).FirstOrDefault();



            DestaquesViewModel BannerModel = new DestaquesViewModel();
            BannerModel.Destaques = model;
            BannerModel.DestaquesTexto = model.HOME_DestaquesTexto.ToList();
            BannerModel.Idioma = model.HOME_DestaquesTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesDestaques", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarDestaques(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HOME_Destaques model = db.HOME_DestaquesTexto.Include("CONF_Idioma").Include("HOME_Destaques")
              .Select(x => x.HOME_Destaques).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            DestaquesViewModel BannerModel = new DestaquesViewModel();
            BannerModel.Destaques = model;
            BannerModel.DestaquesTexto = model.HOME_DestaquesTexto.ToList();
            BannerModel.Idioma = model.HOME_DestaquesTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarDestaques", BannerModel);


        }


        [HttpPost]
        public ActionResult EditarDestaques(DestaquesViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.DestaquesTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.DestaquesTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update HOME_Destaques SET  Activo = '" + model.Destaques.Activo
                  + "'  WHERE ID= " + model.Destaques.Id);

            /*
            db.HOME_Loja.Attach(model.Loja);
            db.Entry(model.Loja).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (HOME_DestaquesTexto texto in model.DestaquesTexto)
            {


                db.Database.ExecuteSqlCommand("Update HOME_DestaquesTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Loja");

                return PartialView("_EditarCategoriaLojaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarDestaques(int? id)
        {

            HOME_Destaques model = db.HOME_DestaquesTexto.Include("CONF_Idioma").Include("HOME_Destaques")
                   .Select(x => x.HOME_Destaques).Where(c => c.Id == id).FirstOrDefault();



            DestaquesViewModel BannerModel = new DestaquesViewModel();
            BannerModel.Destaques = model;
            BannerModel.DestaquesTexto = model.HOME_DestaquesTexto.ToList();
            BannerModel.Idioma = model.HOME_DestaquesTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarDestaquesPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult DestaquesImagem(int? id)
        {

            HOME_DestaquesTexto model = db.HOME_DestaquesTexto.Include("CONF_Idioma")
                .Where(m => m.DestaquesId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemDestaquesPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarDestaquesConfirmar(DestaquesViewModel model)
        {
            HOME_Destaques Destaques = db.HOME_DestaquesTexto.Include("CONF_Idioma").Include("HOME_Loja")
                  .Select(x => x.HOME_Destaques).Where(c => c.Id == model.Destaques.Id).FirstOrDefault();

            foreach (var item in Destaques.HOME_DestaquesTexto.ToList())
            {
                db.HOME_DestaquesTexto.Remove(item);
            }

            db.SaveChanges();

            db.HOME_Destaques.Attach(Destaques);

            db.HOME_Destaques.Remove(Destaques);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        //Promocoes
        public JsonResult ListaPromocoes([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<HOME_Promocoes> query = db.HOME_Promocoes
                .Include("HOME_PromocoesTexto")

                .Include("CONF_Idioma")
                .Where(m => m.HOME_PromocoesTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.HOME_PromocoesTexto.FirstOrDefault().Titulo.Length <= 70 ? m.HOME_PromocoesTexto.FirstOrDefault().Titulo
                        : m.HOME_PromocoesTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.HOME_PromocoesTexto.FirstOrDefault().Introducao.Length <= 70 ? m.HOME_PromocoesTexto.FirstOrDefault().Introducao
                        : m.HOME_PromocoesTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.HOME_PromocoesTexto.FirstOrDefault().Link,
                    PDF = m.HOME_PromocoesTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarPromocoes()
        {
            PromocoesViewModel model = new PromocoesViewModel();
            model.Promocoes.Activo = true;
            model.Promocoes.UsarCaixaTexto = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                HOME_PromocoesTexto LojaTexto = new HOME_PromocoesTexto()
                {
                    CONF_Idioma = idioma
                };
                model.PromocoesTexto.Add(LojaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarPromocoes", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarPromocoes(PromocoesViewModel model)
        {


            model.Promocoes.DataCriacao = DateTime.Now;
            model.Promocoes.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.PromocoesTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.PromocoesTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (HOME_PromocoesTexto textos in model.PromocoesTexto)
            {

                textos.HOME_Promocoes = model.Promocoes;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.Promocoes.HOME_PromocoesTexto = model.PromocoesTexto;
            db.HOME_Promocoes.Add(model.Promocoes);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesPromocoes(int? id)
        {
            HOME_Promocoes model = db.HOME_PromocoesTexto.Include("CONF_Idioma").Include("HOME_Promocoes")
               .Select(x => x.HOME_Promocoes).Where(c => c.Id == id).FirstOrDefault();



            PromocoesViewModel BannerModel = new PromocoesViewModel();
            BannerModel.Promocoes = model;
            BannerModel.PromocoesTexto = model.HOME_PromocoesTexto.ToList();
            BannerModel.Idioma = model.HOME_PromocoesTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesPromocoes", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarPromocoes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HOME_Promocoes model = db.HOME_PromocoesTexto.Include("CONF_Idioma").Include("HOME_Promocoes")
              .Select(x => x.HOME_Promocoes).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            PromocoesViewModel BannerModel = new PromocoesViewModel();
            BannerModel.Promocoes = model;
            BannerModel.PromocoesTexto = model.HOME_PromocoesTexto.ToList();
            BannerModel.Idioma = model.HOME_PromocoesTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarPromocoes", BannerModel);


        }


        [HttpPost]
        public ActionResult EditarPromocoes(PromocoesViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.PromocoesTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.PromocoesTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update HOME_Promocoes SET  UsarCaixaTexto = '" + model.Promocoes.UsarCaixaTexto+"', Activo = '" + model.Promocoes.Activo
                  + "'  WHERE ID= " + model.Promocoes.Id);

            /*
            db.HOME_Loja.Attach(model.Loja);
            db.Entry(model.Loja).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (HOME_PromocoesTexto texto in model.PromocoesTexto)
            {


                db.Database.ExecuteSqlCommand("Update HOME_PromocoesTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Loja");

                return PartialView("_EditarCategoriaLojaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarPromocoes(int id)
        {

            HOME_Promocoes model = db.HOME_PromocoesTexto.Include("CONF_Idioma").Include("HOME_Promocoes")
                   .Select(x => x.HOME_Promocoes).Where(c => c.Id == id).FirstOrDefault();



            PromocoesViewModel BannerModel = new PromocoesViewModel();
            BannerModel.Promocoes = model;
            BannerModel.PromocoesTexto = model.HOME_PromocoesTexto.ToList();
            BannerModel.Idioma = model.HOME_PromocoesTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarPromocoesPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult PromocoesImagem(int? id)
        {

            HOME_PromocoesTexto model = db.HOME_PromocoesTexto.Include("CONF_Idioma")
                .Where(m => m.PromocoesId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPromocoesPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarPromocoesConfirmar(PromocoesViewModel model)
        {
            HOME_Promocoes Promocoes = db.HOME_PromocoesTexto.Include("CONF_Idioma").Include("HOME_Loja")
                  .Select(x => x.HOME_Promocoes).Where(c => c.Id == model.Promocoes.Id).FirstOrDefault();

            foreach (var item in Promocoes.HOME_PromocoesTexto.ToList())
            {
                db.HOME_PromocoesTexto.Remove(item);
            }

            db.SaveChanges();

            db.HOME_Promocoes.Attach(Promocoes);

            db.HOME_Promocoes.Remove(Promocoes);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaNovidades([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<HOME_Novidades> query = db.HOME_Novidades
                .Include("HOME_NovidadesTexto")

                .Include("CONF_Idioma")
                .Where(m => m.HOME_NovidadesTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.HOME_NovidadesTexto.FirstOrDefault().Titulo.Length <= 70 ? m.HOME_NovidadesTexto.FirstOrDefault().Titulo
                        : m.HOME_NovidadesTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.HOME_NovidadesTexto.FirstOrDefault().Introducao.Length <= 70 ? m.HOME_NovidadesTexto.FirstOrDefault().Introducao
                        : m.HOME_NovidadesTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,


                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarNovidades()
        {
            NovidadesViewModel model = new NovidadesViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                HOME_NovidadesTexto NovidadesTexto = new HOME_NovidadesTexto()
                {
                    CONF_Idioma = idioma
                };
                model.NovidadesTexto.Add(NovidadesTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarNovidades", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarNovidades(NovidadesViewModel model)
        {


            model.Novidades.DataCricao = DateTime.Now;
            //model.Novidades.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.NovidadesTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.NovidadesTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (HOME_NovidadesTexto textos in model.NovidadesTexto)
            {

                textos.HOME_Novidades = model.Novidades;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.Novidades.HOME_NovidadesTexto = model.NovidadesTexto;
            db.HOME_Novidades.Add(model.Novidades);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesNovidades(int? id)
        {
            HOME_Novidades model = db.HOME_NovidadesTexto.Include("CONF_Idioma").Include("HOME_Novidades")
               .Select(x => x.HOME_Novidades).Where(c => c.Id == id).FirstOrDefault();



            NovidadesViewModel IntroducaoModel = new NovidadesViewModel();
            IntroducaoModel.Novidades = model;
            IntroducaoModel.NovidadesTexto = model.HOME_NovidadesTexto.ToList();
            IntroducaoModel.Idioma = model.HOME_NovidadesTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesNovidades", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarNovidades(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HOME_Novidades model = db.HOME_NovidadesTexto.Include("CONF_Idioma").Include("HOME_Novidades")
              .Select(x => x.HOME_Novidades).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            NovidadesViewModel IntroducaoModel = new NovidadesViewModel();
            IntroducaoModel.Novidades = model;
            IntroducaoModel.NovidadesTexto = model.HOME_NovidadesTexto.ToList();
            IntroducaoModel.Idioma = model.HOME_NovidadesTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarNovidades", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarNovidades(NovidadesViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.NovidadesTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.NovidadesTexto[i].PDF = fileName;

                }
                i++;

            }




            /*
            db.HOME_Novidades.Attach(model.Novidades);
            db.Entry(model.Novidades).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (HOME_NovidadesTexto texto in model.NovidadesTexto)
            {

                db.Database.ExecuteSqlCommand("Update HOME_NovidadesTexto SET  Titulo = '" + texto.Titulo + "', Conteudo = '" + texto.Introducao + "', "
                  + " Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Novidades");

                return PartialView("_EditarCategoriaNovidadesPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarNovidades(int id)
        {

            HOME_Novidades model = db.HOME_NovidadesTexto.Include("CONF_Idioma").Include("HOME_Novidades")
                   .Select(x => x.HOME_Novidades).Where(c => c.Id == id).FirstOrDefault();



            NovidadesViewModel IntroducaoModel = new NovidadesViewModel();
            IntroducaoModel.Novidades = model;
            IntroducaoModel.NovidadesTexto = model.HOME_NovidadesTexto.ToList();
            IntroducaoModel.Idioma = model.HOME_NovidadesTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarNovidadesPartial", IntroducaoModel);

        }

        [HttpPost]
        public ActionResult ApagarNovidadesConfirmar(NovidadesViewModel model)
        {
            HOME_Novidades NovidadesIntroducao = db.HOME_NovidadesTexto.Include("CONF_Idioma").Include("HOME_Novidades")
                  .Select(x => x.HOME_Novidades).Where(c => c.Id == model.Novidades.Id).FirstOrDefault();

            foreach (var item in NovidadesIntroducao.HOME_NovidadesTexto.ToList())
            {
                db.HOME_NovidadesTexto.Remove(item);
            }

            db.SaveChanges();

            db.HOME_Novidades.Attach(NovidadesIntroducao);

            db.HOME_Novidades.Remove(NovidadesIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult NovidadesImagem(int? id)
        {

            HOME_NovidadesTexto model = db.HOME_NovidadesTexto.Include("CONF_Idioma")
                .Where(m => m.NovidadeId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }

    }



}