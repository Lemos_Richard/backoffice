﻿using Webtech_Office.Models;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;



namespace Webtech_Office.Controllers
{
    [Authorize]
    public class RecrutamentoController : Controller
    {
        private CMS_DbContext DbContext = new CMS_DbContext();

        // GET: Recrutamento
        public ActionResult Index()
        {
            
            return View();
        }

        //Recrutamento Conteudo para o Site
        public ActionResult CriarRecConteudo()
        {

            RecrutamentoConteudoViewModel model = new RecrutamentoConteudoViewModel();

          
           
            foreach (CONF_Idioma idioma in DbContext.CONF_Idioma.ToList())
            {

                RECR_ConteudoTexto texto = new RECR_ConteudoTexto
                {
                    IdiomaId = idioma.Id,
                    CONF_Idioma = idioma
                    
                    
                };
                model.ConteudoTexto.Add(texto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                model.Idioma.Add(idioma);


                
               

            }           

            return PartialView("_CriarRecConteudoPartial", model);
        }


        // POST: Asset/Create  
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarRecConteudo(RecrutamentoConteudoViewModel model)
        {
           

              
                model.Conteudo.DataCriacao = DateTime.Now;
                model.Conteudo.DataModificacao = DateTime.Now;

                int i = 0;
                foreach (ImageUpload file in model.File)
                {
                    if(file.File.FileName != null)
                    {
                        string directory = @"~/Content/files/anexos/";

                        var fileName = Path.GetFileName(file.File.FileName);
                        file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                        model.ConteudoTexto[i].Imagem = file.File.FileName;
                        i++;
                    }
                }

                 i = 0;
                foreach(RECR_ConteudoTexto textos in model.ConteudoTexto)
                {
                    
                    textos.ConteudoId = model.Conteudo.Id;
                    textos.IdiomaId = model.Idioma[i].Id;
                    i++;

                }

               

                Menu menu = null;
                foreach (MenuTexto menuTexto in DbContext.MenuTexto)
                {
                    if (menuTexto.Titulo.ToUpper() == "RECRUTAMENTO".ToUpper())
                    {
                        menu = menuTexto.Menu;
                        
                    } 
                    
                }

            
           



           
            model.Conteudo.RECR_ConteudoTexto = model.ConteudoTexto;
            DbContext.RECR_Conteudo.Add(model.Conteudo);
            
           
           
           
            DbContext.SaveChanges();

      

                return Content("sucesso");


          


            //return View("_CriarRecConteudoPartial", model);
        }

        public JsonResult ListaRecConteudo([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<RECR_Conteudo> query = DbContext.RECR_ConteudoTexto
                .Where(m => m.CONF_Idioma.Codigo.ToUpper().Equals("PT"))
                .Select(m => m.RECR_Conteudo);



            var totalCount = query.Count();
         
            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();
                query = query.Include(m => m.RECR_ConteudoTexto.Where(mt => mt.Descricao.Contains(value)))
                         .Include(m => m.RECR_ConteudoTexto.Where(mt => mt.Introducao.Contains(value)));
            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);
         

            var data =
                query.Select(m => new
                {
                   
                    ConteudoID = m.Id,
                    Titulo = m.RECR_ConteudoTexto.FirstOrDefault().Descricao,
                    Introducao = m.RECR_ConteudoTexto.FirstOrDefault().Introducao,
                    Activo = m.Activo,
                    Imagem = m.RECR_ConteudoTexto.FirstOrDefault().Imagem,
                    Destaque = m.Destaque


                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        public JsonResult ListaRegisto([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<RECR_Registo> query = DbContext.RECR_Registo
                .Include("CONF_Provincia").Include("CONF_Idioma").AsQueryable();
                



            var totalCount = query.Count();
           
            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();
                query = query.Where(m =>
                                        
                                         m.CONF_Provincia.Descricao.Contains(value) ||
                                         m.Nome.Contains(value) || m.Email.Contains(value)
                                      

                         );
            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

           
            query = query.OrderBy(x => orderByString == string.Empty ? "Idioma asc" : orderByString);
         

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);
      

            var data =
                query.Select(m => new
                {
                    
                    Provincia = m.CONF_Provincia.Descricao,
                    RegistoID = m.Id,
                    Nome = m.Nome,
                    Email = m.Email,
                    Telefone = m.Telefone,
                    Anexo =  m.PDF,
                    DataCriacao = SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", m.Data).Trim().Length) + SqlFunctions.DateName("dd", m.Data).Trim() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)m.Data.Month).TrimStart().Length) + SqlFunctions.StringConvert((double)m.Data.Month).TrimStart() + "/" + SqlFunctions.DateName("year", m.Data)
                   


                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        public ActionResult CriarRegisto()
        {

            RegistoViewModel model = new RegistoViewModel();

            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");
            /*
            if (Request.IsAjaxRequest())
                return PartialView("_CriarRegistoPartial", model);
                */

            return PartialView("_CriarRegistoPartial", model);
        }

        [HttpPost]
        public async Task<ActionResult> CriarRegisto(RegistoViewModel model)
        {
            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");
            /*
            if (Request.IsAjaxRequest())
                return PartialView("_CriarRegistoPartial", model);
                */
            //FileUpload
            var TiposValidos = new string[]
                {
                    "application/pdf"
                };
            /*

            if (!TiposValidos.Contains(model.FileUpload.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Por favor seleccionar documento pdf.");
            }

    */




            if (ModelState.IsValid)
            {
                /*
                if (model.FileUpload != null && model.FileUpload.ContentLength > 0)
                {
                    var uploadDir = "~/Content/files/docs";
                    if (!Directory.Exists(uploadDir))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(uploadDir);
                    }
                    var docsPath = Path.Combine(Server.MapPath(uploadDir), model.FileUpload.FileName);
                    var docsUrl = Path.Combine(uploadDir, model.FileUpload.FileName);
                    model.FileUpload.SaveAs(Server.MapPath(docsPath));
                    model.Registo.PDF = docsUrl;

                    Response.Write(docsUrl);
                }
                */

                Menu menu = null;
                foreach (MenuTexto menuTexto in DbContext.MenuTexto)
                {
                    if (menuTexto.Titulo.ToUpper() == "RECRUTAMENTO".ToUpper())
                    {
                        menu = menuTexto.Menu;
                    }
                }
               

              
                DbContext.RECR_Registo.Add(model.Registo);

                var task = DbContext.SaveChangesAsync();
                await task;



            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    ModelState.AddModelError("", "Impossivel criar o seu Registo.");
                    return PartialView("_CriarRegistoPartial", model);
                }
            }






            return Content("sucesso"); ;
        }

        public ActionResult EditarRegisto(int id)
        {
            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");

            var registo = DbContext.RECR_Registo.FirstOrDefault(x => x.Id == id);
            RegistoViewModel model = new RegistoViewModel { Registo = registo }; 


            if (Request.IsAjaxRequest())
                return PartialView("_EditarRegistoPartial", model);
            return View("_EditarRegistoPartial", model);
        }

        [HttpPost]
        public async Task<ActionResult> EditarRegistoPost(RegistoViewModel model)
        {

            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");

            if (ModelState.IsValid)
            {
                DbContext.RECR_Registo.Attach(model.Registo);
                DbContext.Entry(model.Registo).State = EntityState.Modified;
                //DbContext.SaveChanges();

                var task = DbContext.SaveChangesAsync();
                await task;


                if (task.Exception != null)
                {
                    ModelState.AddModelError("", "Impossivel editar Registo");
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return View(Request.IsAjaxRequest() ? "_EditarRegistoPartial" : "_EditarRegistoPartial", model);
                }
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    ModelState.AddModelError("", "Impossivel editar Registo.");
                    return PartialView("_CriarRegistoPartial", model);
                }
            }
            
           return Content("successo");

        }


        

        //Detalhes do Menu
        public ActionResult DetalhesRegisto(int id)
        {
            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");

            var registo =  DbContext.RECR_Registo.FirstOrDefault(x => x.Id == id);
            RegistoViewModel model = new RegistoViewModel { Registo = registo};

            if (Request.IsAjaxRequest())
                return PartialView("_DetalhesRegistoPartial", model);

            return PartialView("_DetalhesRegistoPartial", model);
        }

        /* Apagar Menu */
        public ActionResult ApagarRegisto(int id)
        {
            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");


            var registo = DbContext.RECR_Registo.FirstOrDefault(x => x.Id == id);

            RegistoViewModel model = new RegistoViewModel();
            model.Registo = registo;

            if (Request.IsAjaxRequest())
                return PartialView("_ApagarRegistoPartial", model);
            return View("_ApagarRegistoPartial", model);
        }

        /*Apagar Menu Post*/
        [HttpPost]
        public async Task<ActionResult> ApagarRegistoConfirmar(RegistoViewModel model)
        {
            ViewBag.Idiomas = new SelectList(DbContext.CONF_Idioma, "Id", "Descricao");
            ViewBag.Provincias = new SelectList(DbContext.CONF_Provincia, "Id", "Descricao");

            RECR_Registo registo = new RECR_Registo { Id = model.Registo.Id };
            
            DbContext.RECR_Registo.Attach(registo);
            DbContext.RECR_Registo.Remove(registo);

            var task = DbContext.SaveChangesAsync();
            await task;

            if (task.Exception != null)
            {
                ModelState.AddModelError("", "Impossivel apagar Registo.");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

              
               
                return View("_ApagarMenuPartial", model);
            }



            return Content("sucesso");

        }


    }




}