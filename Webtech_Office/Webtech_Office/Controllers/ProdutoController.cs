﻿ using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    [Authorize]
    public class ProdutoController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        public JsonResult ListaCategoriaProduto([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<PROD_CategoriaProduto> query = db.PROD_CategoriaProduto.Include("PROD_CategoriaProdutoTexto")
                .Include("CONF_Idioma")
                .Where(m => m.PROD_CategoriaProdutoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(m => new
                {
                    CategoriaId = m.Id,
                    Descricao = (m.PROD_CategoriaProdutoTexto.FirstOrDefault().Descricao.Length <= 70 ? m.PROD_CategoriaProdutoTexto.FirstOrDefault().Descricao
                                        : m.PROD_CategoriaProdutoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),
                    Activo = m.Activo,



                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        // GET: Produto
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Introducao()
        {
            return View("Introducao");
        }

        public ActionResult CategoriaProduto()
        {
            return View("CategoriaProduto");
        }

        public ActionResult Produtos()
        {
            return View("Produtos");
        }

        //Tipo de Receitas
        [HttpGet]
        public ActionResult CriarCategoriaProduto()
        {
            CategoriaProdutoViewModel model = new CategoriaProdutoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                PROD_CategoriaProdutoTexto texto = new PROD_CategoriaProdutoTexto()
                {

                    CONF_Idioma = idioma

                };
                model.CategoriaProdutoTexto.Add(texto);

            }

            return PartialView("_CriarCategoriaProduto", model);

        }


        [HttpPost]
        public async Task<ActionResult> CriarCategoriaProduto(CategoriaProdutoViewModel model)
        {
            model.CategoriaProduto.DataCriacao = DateTime.Now;


            int i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (PROD_CategoriaProdutoTexto textos in model.CategoriaProdutoTexto)
            {

                textos.PROD_CategoriaProduto = model.CategoriaProduto;
                textos.CONF_Idioma = idioma[i];
                i++;

            }

            model.CategoriaProduto.PROD_CategoriaProdutoTexto = model.CategoriaProdutoTexto;
            db.PROD_CategoriaProduto.Add(model.CategoriaProduto);
            var task = db.SaveChangesAsync();
            await task;

            if (task.Exception != null)
            {
                ModelState.AddModelError("", "Impossivel criar tipo de receita");

                return View("_CriarCategoriaProduto", model);
            }

            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }


            return RedirectToAction("CategoriaProduto");

        }


        [HttpGet]
        public ActionResult DetalhesCategoriaProduto(int? id)
        {
            PROD_CategoriaProduto model = db.PROD_CategoriaProdutoTexto.Include("CONF_Idioma").Include("PROD_CategoriaProduto")
               .Select(x => x.PROD_CategoriaProduto).Where(c => c.Id == id).FirstOrDefault();





            CategoriaProdutoViewModel CategoriaProdutoModel = new CategoriaProdutoViewModel();
            CategoriaProdutoModel.CategoriaProduto = model;
            CategoriaProdutoModel.CategoriaProdutoTexto = model.PROD_CategoriaProdutoTexto.ToList();
            CategoriaProdutoModel.Idioma = model.PROD_CategoriaProdutoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesCategoriaProduto", CategoriaProdutoModel);

        }

        [HttpGet]
        public ActionResult EditarCategoriaProduto(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROD_CategoriaProduto model = db.PROD_CategoriaProdutoTexto.Include("CONF_Idioma").Include("PROD_CategoriaProduto")
               .Select(x => x.PROD_CategoriaProduto).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }


            CategoriaProdutoViewModel CategoriaProdutoModel = new CategoriaProdutoViewModel();
            CategoriaProdutoModel.CategoriaProduto = model;
            CategoriaProdutoModel.CategoriaProdutoTexto = model.PROD_CategoriaProdutoTexto.ToList();
            CategoriaProdutoModel.Idioma = model.PROD_CategoriaProdutoTexto.Select(x => x.CONF_Idioma).ToList();


            return PartialView("_EditarCategoriaProdutoPartial", CategoriaProdutoModel);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarCategoriaProduto(CategoriaProdutoViewModel model)
        {


            db.PROD_CategoriaProduto.Attach(model.CategoriaProduto);
            db.Entry(model.CategoriaProduto).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            //int i = 0;

            foreach (PROD_CategoriaProdutoTexto texto in model.CategoriaProdutoTexto)
            {

                db.Database.ExecuteSqlCommand("Update PROD_CategoriaProdutoTexto SET  IdiomaId = " + texto.IdiomaId + ", "
                    + "Descricao = '" + texto.Descricao + "' WHERE ID= " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Produto");

                return PartialView("_EditarCategoriaProdutoPartial", model);
            }
            */

            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }


            return RedirectToAction("CategoriaProduto");


        }


        [HttpGet]
        public ActionResult ApagarCategoriaProduto(int? id)
        {

            PROD_CategoriaProduto model = db.PROD_CategoriaProdutoTexto.Include("CONF_Idioma").Include("PROD_CategoriaProduto")
               .Select(x => x.PROD_CategoriaProduto).Where(c => c.Id == id).FirstOrDefault();





            CategoriaProdutoViewModel CategoriaProdutoModel = new CategoriaProdutoViewModel();
            CategoriaProdutoModel.CategoriaProduto = model;
            CategoriaProdutoModel.CategoriaProdutoTexto = model.PROD_CategoriaProdutoTexto.ToList();
            CategoriaProdutoModel.Idioma = model.PROD_CategoriaProdutoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_ApagarCategoriaProdutoPartial", CategoriaProdutoModel);

        }


        [HttpPost]
        public ActionResult ApagarCategoriaProdutoConfirmar(int? id)
        {
            PROD_CategoriaProduto CategoriaProduto = db.PROD_CategoriaProduto.Include("PROD_CategoriaProdutoTexto")
                    .Where(c => c.Id == id).FirstOrDefault();

            foreach (var item in CategoriaProduto.PROD_CategoriaProdutoTexto.ToList())
            {
                db.PROD_CategoriaProdutoTexto.Remove(item);
            }

            db.SaveChanges();

            db.PROD_CategoriaProduto.Attach(CategoriaProduto);

            db.PROD_CategoriaProduto.Remove(CategoriaProduto);

            db.SaveChanges();


            return RedirectToAction("CategoriaProduto");
        }


        public JsonResult ListaProduto([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<PROD_Produto> query = db.PROD_Produto.Include("PROD_ProdutoCategoria")
                .Include("PROD_ProdutoCategoriaTexto")
                .Include("PROD_ProdutoTexto")
                .Include("CONF_Idioma")
                .OrderBy(x => x.Ordem)
                .Where(m => m.PROD_ProdutoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));
                

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Categoria asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {
                    m.Ordem,
                    Categoria = (m.PROD_CategoriaProduto.PROD_CategoriaProdutoTexto.FirstOrDefault().Descricao.Length <= 70 ? m.PROD_CategoriaProduto.PROD_CategoriaProdutoTexto.FirstOrDefault().Descricao
                        : m.PROD_CategoriaProduto.PROD_CategoriaProdutoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                    Descricao = (m.PROD_ProdutoTexto.FirstOrDefault().Descricao.Length <= 70 ? m.PROD_ProdutoTexto.FirstOrDefault().Descricao
                        : m.PROD_ProdutoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                    //Preco = ( m.Preco == null ? 0 : m.Preco),
                    //Desconto = (m.Desconto == null ? 0 : m.Desconto),
                    Preco = (m.Preco),
                    Desconto = (m.Desconto),

                    Imagem = m.Id,
                    Link = m.Id,
                    PDF = m.Id,
                    Destaque = m.Destaques,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarProduto()
        {
            ProdutoViewModel model = new ProdutoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                PROD_ProdutoTexto produtoTexto = new PROD_ProdutoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ProdutoTexto.Add(produtoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            var query = from tr in db.PROD_CategoriaProduto
                        join trt in db.PROD_CategoriaProdutoTexto on tr.Id equals trt.CategoriaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.CategoriaProduto = new SelectList(query, "Id", "Descricao");


            return View("CriarProduto", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarProduto(ProdutoViewModel model)
        {


            model.Produto.DataCriacao = DateTime.Now;
            model.Produto.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName =  Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (PROD_ProdutoTexto textos in model.ProdutoTexto)
            {

                textos.PROD_Produto = model.Produto;
                textos.CONF_Idioma = idioma[i];
                textos.Conteudo = System.Net.WebUtility.HtmlDecode(textos.Conteudo);

                i++;

            }



            model.Produto.PROD_ProdutoTexto = model.ProdutoTexto;
            db.PROD_Produto.Add(model.Produto);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Produtos");

        }


        [HttpGet]
        public ActionResult ProdutoImagem(int? id)
        {

            PROD_ProdutoTexto model = db.PROD_ProdutoTexto.Include("CONF_Idioma")
                .Where(m => m.ProdutoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemProdutoPreviewPartial", imgModel);

        }



        [HttpGet]
        public ActionResult DetalhesProduto(int? id)
        {
            PROD_Produto model = db.PROD_ProdutoTexto.Include("CONF_Idioma").Include("PROD_Produto")
               .Select(x => x.PROD_Produto).Where(c => c.Id == id).FirstOrDefault();



            ProdutoViewModel ProdutoModel = new ProdutoViewModel();
            ProdutoModel.Produto = model;
            ProdutoModel.ProdutoTexto = model.PROD_ProdutoTexto.ToList();
            ProdutoModel.Idioma = model.PROD_ProdutoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.PROD_CategoriaProduto
                        join trt in db.PROD_CategoriaProdutoTexto on tr.Id equals trt.CategoriaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.CategoriaProduto = new SelectList(query, "Id", "Descricao");

            return PartialView("_DetalhesProduto", ProdutoModel);

        }



        [HttpGet]
        public ActionResult EditarProduto(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROD_Produto model = db.PROD_ProdutoTexto.Include("CONF_Idioma").Include("PROD_Produto")
              .Select(x => x.PROD_Produto).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ProdutoViewModel ProdutoModel = new ProdutoViewModel();
            ProdutoModel.Produto = model;
            ProdutoModel.ProdutoTexto = model.PROD_ProdutoTexto.ToList();
            ProdutoModel.Idioma = model.PROD_ProdutoTexto.Select(x => x.CONF_Idioma).ToList();

          
            //ProdutoModel.Preco = (Int32) ProdutoModel.Produto.Preco;
            //ProdutoModel.Desconto = (Int32)ProdutoModel.Produto.Desconto;
         


            var query = from tr in db.PROD_CategoriaProduto
                        join trt in db.PROD_CategoriaProdutoTexto on tr.Id equals trt.CategoriaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.CategoriaProduto = new SelectList(query, "Id", "Descricao");


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                ProdutoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                ProdutoModel.FileDocumento.Add(doc);

            }


            return View("EditarProduto", ProdutoModel);


        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarProduto(ProdutoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null )
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoTexto[i].PDF = fileName;

                }
                i++;

            }

          

            

            db.Database.ExecuteSqlCommand("Update PROD_Produto SET Ordem = " + model.Produto.Ordem + ",  CategoriaId = " + model.Produto.CategoriaId + ", Codigo = '" + model.Produto.Codigo + "', Preco = " + model.Produto.Preco + ", PrecoComDesconto = " + model.Produto.PrecoComDesconto + ", "
                  + "Desconto = '" + model.Produto.Desconto + "', Promocoes = '" + model.Produto.Promocoes + "', Destaques = '" + model.Produto.Destaques + "', Novidades='" + model.Produto.Novidades + "', PartilhaFacebook = '" + model.Produto.PartilhaFacebook + "', PartilhaTwitter = '" + model.Produto.PartilhaTwitter + "', Activo = '" + model.Produto.Activo + "'  WHERE ID= " + model.Produto.Id);

            /*
            db.PROD_Produto.Attach(model.Produto);
            db.Entry(model.Produto).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            db.SaveChanges();

            foreach (PROD_ProdutoTexto texto in model.ProdutoTexto)
            {

                db.Database.ExecuteSqlCommand("Update PROD_ProdutoTexto SET  Introducao = '" + texto.Introducao + "', "
                  + "Descricao = '" + texto.Descricao + "', Conteudo = '" + texto.Conteudo + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID= " + texto.Id);
            }

            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Produto");

                return PartialView("_EditarCategoriaProdutoPartial", model);
            }
            */


            return RedirectToAction("Produtos");


        } 


        [HttpGet]
        public ActionResult ApagarProduto(int? id)
        {

            PROD_Produto model = db.PROD_ProdutoTexto.Include("CONF_Idioma").Include("PROD_Produto")
                   .Select(x => x.PROD_Produto).Where(c => c.Id == id).FirstOrDefault();



            ProdutoViewModel ProdutoModel = new ProdutoViewModel();
            ProdutoModel.Produto = model;
            ProdutoModel.ProdutoTexto = model.PROD_ProdutoTexto.ToList();
            ProdutoModel.Idioma = model.PROD_ProdutoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.PROD_CategoriaProduto
                        join trt in db.PROD_CategoriaProdutoTexto on tr.Id equals trt.CategoriaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.CategoriaProduto = new SelectList(query, "Id", "Descricao");

            return PartialView("_ApagarProdutoPartial", ProdutoModel);

        }


        [HttpPost]
        public ActionResult ApagarProdutoConfirmar(int? id)
        {
            PROD_Produto Produto = db.PROD_Produto.Include("PROD_ProdutoTexto")          
                .Where(c => c.Id == id)
               .FirstOrDefault();

            foreach (var item in Produto.PROD_ProdutoTexto.ToList())
            {
                db.PROD_ProdutoTexto.Remove(item);
            }

            db.SaveChanges();

            db.PROD_Produto.Attach(Produto);

            db.PROD_Produto.Remove(Produto);

            db.SaveChanges();


            return RedirectToAction("Produtos");
        }   


        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
            {


                IQueryable<PROD_ProdutoBanner> query = db.PROD_ProdutoBanner
                    .Include("PROD_ProdutoBannerTexto")
                   
                    .Include("CONF_Idioma")
                    .Where(m => m.PROD_ProdutoBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

                var totalCount = query.Count();

                #region Filtering  
                // Applicar filtros para pesquisas 
                if (requestModel.Search.Value != string.Empty)
                {
                    var value = requestModel.Search.Value.Trim();

                    /*
                      query = 

                          query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                               .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                               */

                }

                var filteredCount = query.Count();

                #endregion Filtering  


                #region Sorting  
                // Sorting  
                var sortedColumns = requestModel.Columns.GetSortedColumns();
                var orderByString = String.Empty;

                foreach (var column in sortedColumns)
                {
                    orderByString += orderByString != String.Empty ? "," : "";
                    orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
                }

                query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

                #endregion Sorting  

                // Paging  
                query = query.Skip(requestModel.Start).Take(requestModel.Length);



                var data =
                    query.Select(m => new
                    {

                        Titulo = (m.PROD_ProdutoBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.PROD_ProdutoBannerTexto.FirstOrDefault().Titulo
                            : m.PROD_ProdutoBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                        Introducao = (m.PROD_ProdutoBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.PROD_ProdutoBannerTexto.FirstOrDefault().Introducao
                            : m.PROD_ProdutoBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),

                    
                        Imagem = m.Id,
                        Link = m.PROD_ProdutoBannerTexto.FirstOrDefault().Link,
                        PDF = m.PROD_ProdutoBannerTexto.FirstOrDefault().PDF,
                        Activo = m.Activo,
                        Opcoes = m.Id

                    }).ToList();


                return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





            }

        [HttpGet]
        public ActionResult CriarProdutoBanner()
        {
            ProdutoBannerViewModel model = new ProdutoBannerViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                PROD_ProdutoBannerTexto produtoTexto = new PROD_ProdutoBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ProdutoBannerTexto.Add(produtoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }       


            

            return View("CriarProdutoBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarProdutoBanner(ProdutoBannerViewModel model)
        {


            model.ProdutoBanner.DataCriacao = DateTime.Now;
            model.ProdutoBanner.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (PROD_ProdutoBannerTexto textos in model.ProdutoBannerTexto)
            {

                textos.PROD_ProdutoBanner = model.ProdutoBanner;
                textos.CONF_Idioma = idioma[i];
              

                i++;

            }



            model.ProdutoBanner.PROD_ProdutoBannerTexto = model.ProdutoBannerTexto;
            db.PROD_ProdutoBanner.Add(model.ProdutoBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesProdutoBanner(int? id)
        {
            PROD_ProdutoBanner model = db.PROD_ProdutoBannerTexto.Include("CONF_Idioma").Include("PROD_ProdutoBanner")
               .Select(x => x.PROD_ProdutoBanner).Where(c => c.Id == id).FirstOrDefault();



            ProdutoBannerViewModel BannerModel = new ProdutoBannerViewModel();
            BannerModel.ProdutoBanner = model;
            BannerModel.ProdutoBannerTexto = model.PROD_ProdutoBannerTexto.ToList();
            BannerModel.Idioma = model.PROD_ProdutoBannerTexto.Select(x => x.CONF_Idioma).ToList();           

            return PartialView("_DetalhesProdutoBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarProdutoBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROD_ProdutoBanner model = db.PROD_ProdutoBannerTexto.Include("CONF_Idioma").Include("PROD_ProdutoBanner")
              .Select(x => x.PROD_ProdutoBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ProdutoBannerViewModel BannerModel = new ProdutoBannerViewModel();
            BannerModel.ProdutoBanner = model;
            BannerModel.ProdutoBannerTexto = model.PROD_ProdutoBannerTexto.ToList();
            BannerModel.Idioma = model.PROD_ProdutoBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarProdutoBanner", BannerModel);


        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarProdutoBanner(ProdutoBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoBannerTexto[i].PDF = fileName;

                }
                i++;

            }

        

            db.Database.ExecuteSqlCommand("Update PROD_ProdutoBanner SET  Activo = '" + model.ProdutoBanner.Activo 
                  + "'  WHERE ID= " + model.ProdutoBanner.Id);

            /*
            db.PROD_Produto.Attach(model.Produto);
            db.Entry(model.Produto).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (PROD_ProdutoBannerTexto texto in model.ProdutoBannerTexto)
            {

                db.Database.ExecuteSqlCommand("Update PROD_ProdutoBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Produto");

                return PartialView("_EditarCategoriaProdutoPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarProdutoBanner(int? id)
        {

            PROD_ProdutoBanner model = db.PROD_ProdutoBannerTexto.Include("CONF_Idioma").Include("PROD_ProdutoBanner")
                   .Select(x => x.PROD_ProdutoBanner).Where(c => c.Id == id).FirstOrDefault();



            ProdutoBannerViewModel BannerModel = new ProdutoBannerViewModel();
            BannerModel.ProdutoBanner = model;
            BannerModel.ProdutoBannerTexto = model.PROD_ProdutoBannerTexto.ToList();
            BannerModel.Idioma = model.PROD_ProdutoBannerTexto.Select(x => x.CONF_Idioma).ToList();


         
            return PartialView("_ApagarProdutoBannerPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult ProdutoBannerImagem(int? id)
        {

            PROD_ProdutoBannerTexto model = db.PROD_ProdutoBannerTexto.Include("CONF_Idioma")
                .Where(m => m.LojaBannerId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemProdutoBannerPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarProdutoBannerConfirmar(ProdutoBannerViewModel model)
        {
            PROD_ProdutoBanner ProdutoBanner = db.PROD_ProdutoBannerTexto.Include("CONF_Idioma").Include("PROD_Produto")
                  .Select(x => x.PROD_ProdutoBanner).Where(c => c.Id == model.ProdutoBanner.Id).FirstOrDefault();

            foreach (var item in ProdutoBanner.PROD_ProdutoBannerTexto.ToList())
            {
                db.PROD_ProdutoBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.PROD_ProdutoBanner.Attach(ProdutoBanner);

            db.PROD_ProdutoBanner.Remove(ProdutoBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaProdutoIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<PROD_ProdutoIntroducao> query = db.PROD_ProdutoIntroducao
                .Include("PROD_ProdutoIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.PROD_ProdutoIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.PROD_ProdutoIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.PROD_ProdutoIntroducaoTexto.FirstOrDefault().Titulo
                        : m.PROD_ProdutoIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.PROD_ProdutoIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.PROD_ProdutoIntroducaoTexto.FirstOrDefault().Introducao
                        : m.PROD_ProdutoIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                  
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarProdutoIntroducao()
        {
            ProdutoIntroducaoViewModel model = new ProdutoIntroducaoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                PROD_ProdutoIntroducaoTexto produtoTexto = new PROD_ProdutoIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ProdutoIntroducaoTexto.Add(produtoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarProdutoIntroducao", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarProdutoIntroducao(ProdutoIntroducaoViewModel model)
        {


            model.ProdutoIntroducao.DataCriacao = DateTime.Now;
            model.ProdutoIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (PROD_ProdutoIntroducaoTexto textos in model.ProdutoIntroducaoTexto)
            {

                textos.PROD_ProdutoIntroducao = model.ProdutoIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.ProdutoIntroducao.PROD_ProdutoIntroducaoTexto = model.ProdutoIntroducaoTexto;
            db.PROD_ProdutoIntroducao.Add(model.ProdutoIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Introducao");

        }


        [HttpGet]
        public ActionResult DetalhesProdutoIntroducao(int? id)
        {
            PROD_ProdutoIntroducao model = db.PROD_ProdutoIntroducaoTexto.Include("CONF_Idioma").Include("PROD_ProdutoIntroducao")
               .Select(x => x.PROD_ProdutoIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ProdutoIntroducaoViewModel IntroducaoModel = new ProdutoIntroducaoViewModel();
            IntroducaoModel.ProdutoIntroducao = model;
            IntroducaoModel.ProdutoIntroducaoTexto = model.PROD_ProdutoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.PROD_ProdutoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesProdutoIntroducao", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarProdutoIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROD_ProdutoIntroducao model = db.PROD_ProdutoIntroducaoTexto.Include("CONF_Idioma").Include("PROD_ProdutoIntroducao")
              .Select(x => x.PROD_ProdutoIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ProdutoIntroducaoViewModel IntroducaoModel = new ProdutoIntroducaoViewModel();
            IntroducaoModel.ProdutoIntroducao = model;
            IntroducaoModel.ProdutoIntroducaoTexto = model.PROD_ProdutoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.PROD_ProdutoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarProdutoIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarProdutoIntroducao(ProdutoIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ProdutoIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update PROD_ProdutoIntroducao SET  Activo = '" + model.ProdutoIntroducao.Activo
                  + "'  WHERE ID= " + model.ProdutoIntroducao.Id);

            /*
            db.PROD_Produto.Attach(model.Produto);
            db.Entry(model.Produto).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (PROD_ProdutoIntroducaoTexto texto in model.ProdutoIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update PROD_ProdutoIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Produto");

                return PartialView("_EditarCategoriaProdutoPartial", model);
            }
            */


            return RedirectToAction("Introducao");


        }


        [HttpGet]
        public ActionResult ApagarProdutoIntroducao(int id)
        {

            PROD_ProdutoIntroducao model = db.PROD_ProdutoIntroducaoTexto.Include("CONF_Idioma").Include("PROD_ProdutoIntroducao")
                   .Select(x => x.PROD_ProdutoIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ProdutoIntroducaoViewModel IntroducaoModel = new ProdutoIntroducaoViewModel();
            IntroducaoModel.ProdutoIntroducao = model;
            IntroducaoModel.ProdutoIntroducaoTexto = model.PROD_ProdutoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.PROD_ProdutoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarProdutoIntroducaoPartial", IntroducaoModel);

        }



        [HttpGet]
        public ActionResult ProdutoIntroducaoImagem(int? id)
        {

            PROD_ProdutoIntroducaoTexto model = db.PROD_ProdutoIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.ProdutoIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemProdutoIntroducaoPreviewPartial", imgModel);

        }



        [HttpPost]
        public ActionResult ApagarProdutoIntroducaoConfirmar(int id)
        {
            PROD_ProdutoIntroducao ProdutoIntroducao = db.PROD_ProdutoIntroducao
                .Include("PROD_ProdutoIntroducaoTexto")
                  .Where(c => c.Id == id).FirstOrDefault();

            foreach (var item in ProdutoIntroducao.PROD_ProdutoIntroducaoTexto.ToList())
            {
                db.PROD_ProdutoIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.PROD_ProdutoIntroducao.Attach(ProdutoIntroducao);

            db.PROD_ProdutoIntroducao.Remove(ProdutoIntroducao);

            db.SaveChanges();


            return Redirect("Introducao");
        }


    }

}