﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    public class ResponsabilidadeSocialController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        // GET: RespSocial
        public ActionResult Index()
        {
            return View();
        }




        public JsonResult ListaRespSocialIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<RESP_RespSocialIntroducao> query = db.RESP_RespSocialIntroducao
                .Include("RESP_RespSocialIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.RESP_RespSocialIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.RESP_RespSocialIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.RESP_RespSocialIntroducaoTexto.FirstOrDefault().Titulo
                        : m.RESP_RespSocialIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.RESP_RespSocialIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.RESP_RespSocialIntroducaoTexto.FirstOrDefault().Introducao
                        : m.RESP_RespSocialIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarRespSocialIntroducao()
        {
            RespSocialIntroducaoViewModel model = new RespSocialIntroducaoViewModel();
            model.RespSocialIntroducao.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RESP_RespSocialIntroducaoTexto RespSocialTexto = new RESP_RespSocialIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.RespSocialIntroducaoTexto.Add(RespSocialTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarRespSocialIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarRespSocialIntroducao(RespSocialIntroducaoViewModel model)
        {


            model.RespSocialIntroducao.DataCriacao = DateTime.Now;
            model.RespSocialIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (RESP_RespSocialIntroducaoTexto textos in model.RespSocialIntroducaoTexto)
            {

                textos.RESP_RespSocialIntroducao = model.RespSocialIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.RespSocialIntroducao.RESP_RespSocialIntroducaoTexto = model.RespSocialIntroducaoTexto;
            db.RESP_RespSocialIntroducao.Add(model.RespSocialIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesRespSocialIntroducao(int? id)
        {
            RESP_RespSocialIntroducao model = db.RESP_RespSocialIntroducaoTexto.Include("CONF_Idioma").Include("RESP_RespSocialIntroducao")
               .Select(x => x.RESP_RespSocialIntroducao).Where(c => c.Id == id).FirstOrDefault();



            RespSocialIntroducaoViewModel IntroducaoModel = new RespSocialIntroducaoViewModel();
            IntroducaoModel.RespSocialIntroducao = model;
            IntroducaoModel.RespSocialIntroducaoTexto = model.RESP_RespSocialIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.RESP_RespSocialIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesRespSocialIntroducao", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarRespSocialIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RESP_RespSocialIntroducao model = db.RESP_RespSocialIntroducaoTexto.Include("CONF_Idioma").Include("RESP_RespSocialIntroducao")
              .Select(x => x.RESP_RespSocialIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            RespSocialIntroducaoViewModel IntroducaoModel = new RespSocialIntroducaoViewModel();
            IntroducaoModel.RespSocialIntroducao = model;
            IntroducaoModel.RespSocialIntroducaoTexto = model.RESP_RespSocialIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.RESP_RespSocialIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarRespSocialIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarRespSocialIntroducao(RespSocialIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update RESP_RespSocialIntroducao SET  Activo = '" + model.RespSocialIntroducao.Activo
                  + "'  WHERE ID= " + model.RespSocialIntroducao.Id);

            /*
            db.RESP_RespSocial.Attach(model.RespSocial);
            db.Entry(model.RespSocial).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (RESP_RespSocialIntroducaoTexto texto in model.RespSocialIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update RESP_RespSocialIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de RespSocial");

                return PartialView("_EditarCategoriaRespSocialPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarRespSocialIntroducao(int id)
        {

            RESP_RespSocialIntroducao model = db.RESP_RespSocialIntroducaoTexto.Include("CONF_Idioma").Include("RESP_RespSocialIntroducao")
                   .Select(x => x.RESP_RespSocialIntroducao).Where(c => c.Id == id).FirstOrDefault();



            RespSocialIntroducaoViewModel IntroducaoModel = new RespSocialIntroducaoViewModel();
            IntroducaoModel.RespSocialIntroducao = model;
            IntroducaoModel.RespSocialIntroducaoTexto = model.RESP_RespSocialIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.RESP_RespSocialIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarRespSocialIntroducaoPartial", IntroducaoModel);

        }


        [HttpPost]
        public ActionResult ApagarRespSocialIntroducaoConfirmar(RespSocialIntroducaoViewModel model)
        {
            RESP_RespSocialIntroducao RespSocialIntroducao = db.RESP_RespSocialIntroducaoTexto.Include("CONF_Idioma").Include("RESP_RespSocial")
                  .Select(x => x.RESP_RespSocialIntroducao).Where(c => c.Id == model.RespSocialIntroducao.Id).FirstOrDefault();

            foreach (var item in RespSocialIntroducao.RESP_RespSocialIntroducaoTexto.ToList())
            {
                db.RESP_RespSocialIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.RESP_RespSocialIntroducao.Attach(RespSocialIntroducao);

            db.RESP_RespSocialIntroducao.Remove(RespSocialIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }






        [HttpGet]
        public ActionResult RespSocialIntroducaoImagem(int? id)
        {

            RESP_RespSocialIntroducaoTexto model = db.RESP_RespSocialIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.RespSocialIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }



        public JsonResult ListaRespSocial([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<RESP_RespSocial> query = db.RESP_RespSocial
                .Include("RESP_RespSocialTexto")

                .Include("CONF_Idioma")
                .Where(m => m.RESP_RespSocialTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.RESP_RespSocialTexto.FirstOrDefault().Titulo.Length <= 70 ? m.RESP_RespSocialTexto.FirstOrDefault().Titulo
                        : m.RESP_RespSocialTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.RESP_RespSocialTexto.FirstOrDefault().Introducao.Length <= 70 ? m.RESP_RespSocialTexto.FirstOrDefault().Introducao
                        : m.RESP_RespSocialTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    
                    Data = SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", m.DataEvento).Trim().Length) + SqlFunctions.DateName("dd", m.DataEvento).Trim() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)m.DataEvento.Month).TrimStart().Length) + SqlFunctions.StringConvert((double)m.DataEvento.Month).TrimStart() + "/" + SqlFunctions.DateName("year", m.DataEvento),

                    Imagem = m.Id,


                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarRespSocial()
        {
            RespSocialViewModel model = new RespSocialViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RESP_RespSocialTexto RespSocialTexto = new RESP_RespSocialTexto()
                {
                    CONF_Idioma = idioma
                };
                model.RespSocialTexto.Add(RespSocialTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarRespSocial", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarRespSocial(RespSocialViewModel model)
        {


            model.RespSocial.DataCriacao = DateTime.Now;
           

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (RESP_RespSocialTexto textos in model.RespSocialTexto)
            {

                textos.RESP_RespSocial = model.RespSocial;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.RespSocial.RESP_RespSocialTexto = model.RespSocialTexto;
            db.RESP_RespSocial.Add(model.RespSocial);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesRespSocial(int? id)
        {
            RESP_RespSocial model = db.RESP_RespSocialTexto.Include("CONF_Idioma").Include("RESP_RespSocial")
               .Select(x => x.RESP_RespSocial).Where(c => c.Id == id).FirstOrDefault();



            RespSocialViewModel IntroducaoModel = new RespSocialViewModel();
            IntroducaoModel.RespSocial = model;
            IntroducaoModel.RespSocialTexto = model.RESP_RespSocialTexto.ToList();
            IntroducaoModel.Idioma = model.RESP_RespSocialTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesRespSocial", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarRespSocial(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RESP_RespSocial model = db.RESP_RespSocialTexto.Include("CONF_Idioma").Include("RESP_RespSocial")
              .Select(x => x.RESP_RespSocial).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            RespSocialViewModel IntroducaoModel = new RespSocialViewModel();
            IntroducaoModel.RespSocial = model;
            IntroducaoModel.RespSocialTexto = model.RESP_RespSocialTexto.ToList();
            IntroducaoModel.Idioma = model.RESP_RespSocialTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarRespSocial", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarRespSocial(RespSocialViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetFileName(file.File.FileName);
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.RespSocialTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update RESP_RespSocial SET  Activo = '" + model.RespSocial.Activo
                     + "'  WHERE ID= " + model.RespSocial.Id);

            i = 0;

            foreach (RESP_RespSocialTexto texto in model.RespSocialTexto)
            {

                db.Database.ExecuteSqlCommand("Update RESP_RespSocialTexto SET  Titulo = '" + texto.Titulo + "', Conteudo = '" + texto.Introducao + "', "
                  + " Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de RespSocial");

                return PartialView("_EditarCategoriaRespSocialPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarRespSocial(int id)
        {

            RESP_RespSocial model = db.RESP_RespSocialTexto.Include("CONF_Idioma").Include("RESP_RespSocial")
                   .Select(x => x.RESP_RespSocial).Where(c => c.Id == id).FirstOrDefault();



            RespSocialViewModel IntroducaoModel = new RespSocialViewModel();
            IntroducaoModel.RespSocial = model;
            IntroducaoModel.RespSocialTexto = model.RESP_RespSocialTexto.ToList();
            IntroducaoModel.Idioma = model.RESP_RespSocialTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarRespSocialPartial", IntroducaoModel);

        }

        [HttpPost]
        public ActionResult ApagarRespSocialConfirmar(RespSocialViewModel model)
        {
            RESP_RespSocial RespSocialIntroducao = db.RESP_RespSocialTexto.Include("CONF_Idioma").Include("RESP_RespSocial")
                  .Select(x => x.RESP_RespSocial).Where(c => c.Id == model.RespSocial.Id).FirstOrDefault();

            foreach (var item in RespSocialIntroducao.RESP_RespSocialTexto.ToList())
            {
                db.RESP_RespSocialTexto.Remove(item);
            }

            db.SaveChanges();

            db.RESP_RespSocial.Attach(RespSocialIntroducao);

            db.RESP_RespSocial.Remove(RespSocialIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult RespSocialImagem(int? id)
        {

            RESP_RespSocialTexto model = db.RESP_RespSocialTexto.Include("CONF_Idioma")
                .Where(m => m.RespSocialId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }


    }
}