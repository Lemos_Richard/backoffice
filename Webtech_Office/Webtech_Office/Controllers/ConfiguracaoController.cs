﻿

using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data;

using System.Data.Entity.SqlServer;
using System.Linq;

using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    public class ConfiguracaoController : Controller
    {
        private CMS_DbContext DbContext = new CMS_DbContext();

        // GET: Configuracao
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListaMenu([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<Menu> query = DbContext.MenuTexto
                .Where(m => m.CONF_Idioma.Codigo.ToUpper().Equals("PT"))
                .Select(m => m.Menu);



            var totalCount = query.Count();
            /*
            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();
                query = query.Where(m =>
                                         m.Ordem.ToString().Contains(value) ||
                                         m.TipoDeEstrutura.Contains(value)

                         ).Include(m => m.MenuTexto.Where(mt => mt.Titulo.Contains(value)))
                         .Include(m => m.SuperMenu.MenuTexto.Where(mt => mt.Titulo.Contains(value)));
            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy<Menu>(orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);
            */

            var data =
                query.Select(m => new
                {
                    MenuID = m.Id,
                    Titulo = m.MenuTexto.FirstOrDefault().Titulo,
                    Ordem = m.Ordem,
                    Estado = m.Estado,
                    TipoDeEstrutura = m.TipoDeEstrutura,
                    //MenuParente = m.SuperMenu.MenuTexto.FirstOrDefault().Titulo,
                    DataCriacao = SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", m.DataCriacao).Trim().Length) + SqlFunctions.DateName("dd", m.DataCriacao).Trim() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)m.DataCriacao.Month).TrimStart().Length) + SqlFunctions.StringConvert((double)m.DataCriacao.Month).TrimStart() + "/" + SqlFunctions.DateName("year", m.DataCriacao),
                    DataModificacao = SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", m.DataModificação).Trim().Length) + SqlFunctions.DateName("dd", m.DataModificação).Trim() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)m.DataModificação.Month).TrimStart().Length) + SqlFunctions.StringConvert((double)m.DataModificação.Month).TrimStart() + "/" + SqlFunctions.DateName("year", m.DataModificação)


                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, 4, 19), JsonRequestBehavior.AllowGet);





        }

        public ActionResult CriarMenu()
        {

            MenuViewModel menuView = new MenuViewModel();
          

            menuView.Idioma = DbContext.CONF_Idioma.ToList();
            foreach (CONF_Idioma idioma in menuView.Idioma)
            {
                idioma.DataCriacao = DateTime.Now;
                MenuTexto texto = new MenuTexto()  ;
                menuView.MenuTexto.Add(texto);
             
                
            }

            ViewBag.Idiomas = DbContext.CONF_Idioma.ToList();          

            /* Estrutura de Menu */
            List<SelectListItem> EstruturaMenuItems = new List<SelectListItem>();
            EstruturaMenuItems.Add(new SelectListItem
            {
                Text = "Fixa",
                Value = "Fixa"
            });

            EstruturaMenuItems.Add(new SelectListItem
            {
                Text = "Drug&Drop",
                Value = "Drug&Drop"
            });

            ViewBag.TipoDeEstrutura_DDL = EstruturaMenuItems;

            ViewBag.SuperMenus = new SelectList(DbContext.MenuTexto, "MenuId", "Titulo");

            if (Request.IsAjaxRequest())
                return PartialView("_CreateMenuPartial", menuView);

          return View("_CreateMenuPartial", menuView);
        }



        // POST: Asset/Create  
        [HttpPost]
        public  ActionResult CriarMenu(MenuViewModel menuView)
        {
            /* Estrutura de Menu */
            List<SelectListItem> EstruturaMenuItems = new List<SelectListItem>();
            EstruturaMenuItems.Add(new SelectListItem
            {
                Text = "Fixa",
                Value = "Fixa"
            });
            EstruturaMenuItems.Add(new SelectListItem
            {
                Text = "Drug&Drop",
                Value = "Drug&Drop"
            });
            ViewBag.TipoDeEstrutura_DDL = EstruturaMenuItems;

      
            ViewBag.SuperMenus = new SelectList(DbContext.MenuTexto, "MenuId", "Titulo");


            menuView.Menu.DataCriacao = DateTime.Now;
            menuView.Menu.DataModificação = DateTime.Now;

            int i = 0;
            foreach(var textos in menuView.MenuTexto)
            {
                
                textos.IdiomaId = menuView.Idioma[i++].Id;
                textos.MenuId = menuView.Menu.Id;
            }
            menuView.Menu.MenuTexto = menuView.MenuTexto;


            DbContext.Menu.Add(menuView.Menu);
         DbContext.SaveChanges();
          

       



            menuView.Idioma = DbContext.CONF_Idioma.ToList();
            i = 0;
            foreach (CONF_Idioma idioma in menuView.Idioma)
            {
                MenuTexto texto = new MenuTexto()
                {
                    MenuId = menuView.Menu.Id,
                    IdiomaId = idioma.Id,
                    Menu = menuView.Menu,
                    CONF_Idioma = idioma,

                };
                menuView.MenuTexto.Add(texto);

            }


            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel criar este menu.");
                return PartialView("_CreateMenuPartial", menuView);
            }  




            return Content("successo");
        }





    }
}