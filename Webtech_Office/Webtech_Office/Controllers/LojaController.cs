﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    [Authorize]
    public class LojaController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

     

        // GET: Loja
        public ActionResult Index()
        {
            return View();
        }

      


        


        public JsonResult ListaLoja([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<LOJA_Loja> query = db.LOJA_Loja              
                .Include("CONF_Provincia")
                .Include("CONF_Idioma");

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Categoria asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                 
                    Ordem = m.Ordem,
                   Nome = (m.Nome.Length <= 70 ? m.Nome
                        : m.Nome.Substring(0, 70) + "..."),

                   Morada = (m.Morada.Length <= 70 ? m.Morada
                        : m.Morada.Substring(0, 70) + "..."),

                    Provincia = (m.CONF_Provincia.Descricao.Length <= 70 ? m.CONF_Provincia.Descricao
                        : m.CONF_Provincia.Descricao.Substring(0, 70) + "..."),

                    Imagem = m.Id,
                   
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarLoja()
        {
            LojaViewModel model = new LojaViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                LOJA_LojaTexto LojaTexto = new LOJA_LojaTexto()
                {
                    CONF_Idioma = idioma
                };
                model.LojaTexto.Add(LojaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }



            var Provincia = db.CONF_Provincia.ToList();
         

            ViewBag.Provincia = new SelectList(Provincia, "Id", "Descricao");
           




            return View("CriarLoja", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarLoja(LojaViewModel model)
        {


            model.Loja.DataCriacao = DateTime.Now;
            model.Loja.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (LOJA_LojaTexto textos in model.LojaTexto)
            {

                textos.LOJA_Loja = model.Loja;
                textos.CONF_Idioma = idioma[i];
                textos.Conteudo = System.Net.WebUtility.HtmlDecode(textos.Conteudo);

                i++;

            }



            model.Loja.LOJA_LojaTexto = model.LojaTexto;
            db.LOJA_Loja.Add(model.Loja);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult LojaImagem(int? id)
        {

            LOJA_LojaTexto model = db.LOJA_LojaTexto.Include("CONF_Idioma")
                .Where(m => m.LojaId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemLojaPreviewPartial", imgModel);

        }



        [HttpGet]
        public ActionResult DetalhesLoja(int? id)
        {
            LOJA_Loja model = db.LOJA_LojaTexto.Include("CONF_Idioma").Include("LOJA_Loja")
               .Select(x => x.LOJA_Loja).Where(c => c.Id == id).FirstOrDefault();



            LojaViewModel LojaModel = new LojaViewModel();
            LojaModel.Loja = model;
            LojaModel.LojaTexto = model.LOJA_LojaTexto.ToList();
            LojaModel.Idioma = model.LOJA_LojaTexto.Select(x => x.CONF_Idioma).ToList();


            var Provincia = db.CONF_Provincia.ToList();


            ViewBag.Provincia = new SelectList(Provincia, "Id", "Descricao");






            return PartialView("_DetalhesLoja", LojaModel);

        }



        [HttpGet]
        public ActionResult EditarLoja(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOJA_Loja model = db.LOJA_LojaTexto.Include("CONF_Idioma").Include("LOJA_Loja")
              .Select(x => x.LOJA_Loja).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            LojaViewModel LojaModel = new LojaViewModel();
            LojaModel.Loja = model;
            LojaModel.LojaTexto = model.LOJA_LojaTexto.ToList();
            LojaModel.Idioma = model.LOJA_LojaTexto.Select(x => x.CONF_Idioma).ToList();




            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                LojaModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                LojaModel.FileDocumento.Add(doc);

            }

            var Provincia = db.CONF_Provincia.ToList();


            ViewBag.Provincia = new SelectList(Provincia, "Id", "Descricao");



            return View("EditarLoja", LojaModel);


        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarLoja(LojaViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaTexto[i].PDF = fileName;

                }
                i++;

            }




            db.Database.ExecuteSqlCommand("Update LOJA_Loja SET Nome = '" + model.Loja.Nome + "', Morada = '" + model.Loja.Morada + "', Email = '"+ model.Loja.Email +"', Horarios = '" + model.Loja.Horarios + "', Telefone = '" + model.Loja.Telefone +"', "
                  + " Responsavel = '" + model.Loja.Responsavel + "', Provincia = "+ model.Loja.Provincia +", Destaque = '"+ model.Loja.Destaque +"', PartilhaFacebook = '"+ model.Loja.PartilhaFacebook +"', PartilhaTwitter = '" + model.Loja.PartilhaTwitter + "', Activo = '" + model.Loja.Activo + "'  WHERE ID = " + model.Loja.Id);

            /*
            db.LOJA_Loja.Attach(model.Loja);
            db.Entry(model.Loja).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (LOJA_LojaTexto texto in model.LojaTexto)
            {

                db.Database.ExecuteSqlCommand("Update LOJA_LojaTexto SET  Introducao = '" + texto.Introducao + "', "
                  +" Conteudo = '" + texto.Conteudo + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID= " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Loja");

                return PartialView("_EditarCategoriaLojaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarLoja(int? id)
        {

            LOJA_Loja model = db.LOJA_LojaTexto.Include("CONF_Idioma").Include("LOJA_Loja")
                   .Select(x => x.LOJA_Loja).Where(c => c.Id == id).FirstOrDefault();



            LojaViewModel LojaModel = new LojaViewModel();
            LojaModel.Loja = model;
            LojaModel.LojaTexto = model.LOJA_LojaTexto.ToList();
            LojaModel.Idioma = model.LOJA_LojaTexto.Select(x => x.CONF_Idioma).ToList();

            var Provincia = db.CONF_Provincia.ToList();


            ViewBag.Provincia = new SelectList(Provincia, "Id", "Descricao");



            return PartialView("_ApagarLojaPartial", LojaModel);

        }


        [HttpPost]
        public ActionResult ApagarLojaConfirmar(LojaViewModel model)
        {
            LOJA_Loja Loja = db.LOJA_LojaTexto.Include("CONF_Idioma").Include("LOJA_Loja")
                  .Select(x => x.LOJA_Loja).Where(c => c.Id == model.Loja.Id).FirstOrDefault();

            foreach (var item in Loja.LOJA_LojaTexto.ToList())
            {
                db.LOJA_LojaTexto.Remove(item);
            }

            db.SaveChanges();

            db.LOJA_Loja.Attach(Loja);

            db.LOJA_Loja.Remove(Loja);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<LOJA_LojaBanner> query = db.LOJA_LojaBanner
                .Include("LOJA_LojaBannerTexto")

                .Include("CONF_Idioma")
                .Where(m => m.LOJA_LojaBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.LOJA_LojaBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.LOJA_LojaBannerTexto.FirstOrDefault().Titulo
                        : m.LOJA_LojaBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.LOJA_LojaBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.LOJA_LojaBannerTexto.FirstOrDefault().Introducao
                        : m.LOJA_LojaBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.LOJA_LojaBannerTexto.FirstOrDefault().Link,
                    PDF = m.LOJA_LojaBannerTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarLojaBanner()
        {
            LojaBannerViewModel model = new LojaBannerViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                LOJA_LojaBannerTexto LojaTexto = new LOJA_LojaBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.LojaBannerTexto.Add(LojaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarLojaBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarLojaBanner(LojaBannerViewModel model)
        {


            model.LojaBanner.DataCriacao = DateTime.Now;
            model.LojaBanner.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (LOJA_LojaBannerTexto textos in model.LojaBannerTexto)
            {

                textos.LOJA_LojaBanner = model.LojaBanner;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.LojaBanner.LOJA_LojaBannerTexto = model.LojaBannerTexto;
            db.LOJA_LojaBanner.Add(model.LojaBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesLojaBanner(int? id)
        {
            LOJA_LojaBanner model = db.LOJA_LojaBannerTexto.Include("CONF_Idioma").Include("LOJA_LojaBanner")
               .Select(x => x.LOJA_LojaBanner).Where(c => c.Id == id).FirstOrDefault();



            LojaBannerViewModel BannerModel = new LojaBannerViewModel();
            BannerModel.LojaBanner = model;
            BannerModel.LojaBannerTexto = model.LOJA_LojaBannerTexto.ToList();
            BannerModel.Idioma = model.LOJA_LojaBannerTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesLojaBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarLojaBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOJA_LojaBanner model = db.LOJA_LojaBannerTexto.Include("CONF_Idioma").Include("LOJA_LojaBanner")
              .Select(x => x.LOJA_LojaBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            LojaBannerViewModel BannerModel = new LojaBannerViewModel();
            BannerModel.LojaBanner = model;
            BannerModel.LojaBannerTexto = model.LOJA_LojaBannerTexto.ToList();
            BannerModel.Idioma = model.LOJA_LojaBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarLojaBanner", BannerModel);


        }


        [HttpPost]
        
        public ActionResult EditarLojaBanner(LojaBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaBannerTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update LOJA_LojaBanner SET  Activo = '" + model.LojaBanner.Activo
                  + "'  WHERE ID= " + model.LojaBanner.Id);

            /*
            db.LOJA_Loja.Attach(model.Loja);
            db.Entry(model.Loja).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (LOJA_LojaBannerTexto texto in model.LojaBannerTexto)
            {
              

                db.Database.ExecuteSqlCommand("Update LOJA_LojaBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Loja");

                return PartialView("_EditarCategoriaLojaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarLojaBanner(int? id)
        {

            LOJA_LojaBanner model = db.LOJA_LojaBannerTexto.Include("CONF_Idioma").Include("LOJA_LojaBanner")
                   .Select(x => x.LOJA_LojaBanner).Where(c => c.Id == id).FirstOrDefault();



            LojaBannerViewModel BannerModel = new LojaBannerViewModel();
            BannerModel.LojaBanner = model;
            BannerModel.LojaBannerTexto = model.LOJA_LojaBannerTexto.ToList();
            BannerModel.Idioma = model.LOJA_LojaBannerTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarLojaBannerPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult LojaBannerImagem(int? id)
        {

            LOJA_LojaBannerTexto model = db.LOJA_LojaBannerTexto.Include("CONF_Idioma")
                .Where(m => m.LojaBannerId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemLojaBannerPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarLojaBannerConfirmar(LojaBannerViewModel model)
        {
            LOJA_LojaBanner LojaBanner = db.LOJA_LojaBannerTexto.Include("CONF_Idioma").Include("LOJA_Loja")
                  .Select(x => x.LOJA_LojaBanner).Where(c => c.Id == model.LojaBanner.Id).FirstOrDefault();

            foreach (var item in LojaBanner.LOJA_LojaBannerTexto.ToList())
            {
                db.LOJA_LojaBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.LOJA_LojaBanner.Attach(LojaBanner);

            db.LOJA_LojaBanner.Remove(LojaBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaLojaIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<LOJA_LojaIntroducao> query = db.LOJA_LojaIntroducao
                .Include("LOJA_LojaIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.LOJA_LojaIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.LOJA_LojaIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.LOJA_LojaIntroducaoTexto.FirstOrDefault().Titulo
                        : m.LOJA_LojaIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.LOJA_LojaIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.LOJA_LojaIntroducaoTexto.FirstOrDefault().Introducao
                        : m.LOJA_LojaIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarLojaIntroducao()
        {
            LojaIntroducaoViewModel model = new LojaIntroducaoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                LOJA_LojaIntroducaoTexto LojaTexto = new LOJA_LojaIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.LojaIntroducaoTexto.Add(LojaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarLojaIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarLojaIntroducao(LojaIntroducaoViewModel model)
        {


            model.LojaIntroducao.DataCriacao = DateTime.Now;
            model.LojaIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (LOJA_LojaIntroducaoTexto textos in model.LojaIntroducaoTexto)
            {

                textos.LOJA_LojaIntroducao = model.LojaIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.LojaIntroducao.LOJA_LojaIntroducaoTexto = model.LojaIntroducaoTexto;
            db.LOJA_LojaIntroducao.Add(model.LojaIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }  

        [HttpGet]
        public ActionResult DetalhesLojaIntroducao(int? id)
        {
            LOJA_LojaIntroducao model = db.LOJA_LojaIntroducaoTexto.Include("CONF_Idioma").Include("LOJA_LojaIntroducao")
               .Select(x => x.LOJA_LojaIntroducao).Where(c => c.Id == id).FirstOrDefault();



            LojaIntroducaoViewModel IntroducaoModel = new LojaIntroducaoViewModel();
            IntroducaoModel.LojaIntroducao = model;
            IntroducaoModel.LojaIntroducaoTexto = model.LOJA_LojaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.LOJA_LojaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesLojaIntroducao", IntroducaoModel);

        }
        [HttpGet]
        public ActionResult EditarLojaIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOJA_LojaIntroducao model = db.LOJA_LojaIntroducaoTexto.Include("CONF_Idioma").Include("LOJA_LojaIntroducao")
              .Select(x => x.LOJA_LojaIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            LojaIntroducaoViewModel IntroducaoModel = new LojaIntroducaoViewModel();
            IntroducaoModel.LojaIntroducao = model;
            IntroducaoModel.LojaIntroducaoTexto = model.LOJA_LojaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.LOJA_LojaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarLojaIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarLojaIntroducao(LojaIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.LojaIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update LOJA_LojaIntroducao SET  Activo = '" + model.LojaIntroducao.Activo
                  + "'  WHERE ID= " + model.LojaIntroducao.Id);

            /*
            db.LOJA_Loja.Attach(model.Loja);
            db.Entry(model.Loja).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (LOJA_LojaIntroducaoTexto texto in model.LojaIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update LOJA_LojaIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Loja");

                return PartialView("_EditarCategoriaLojaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }

        [HttpGet]
        public ActionResult ApagarLojaIntroducao(int id)
        {

            LOJA_LojaIntroducao model = db.LOJA_LojaIntroducaoTexto.Include("CONF_Idioma").Include("LOJA_LojaIntroducao")
                   .Select(x => x.LOJA_LojaIntroducao).Where(c => c.Id == id).FirstOrDefault();



            LojaIntroducaoViewModel IntroducaoModel = new LojaIntroducaoViewModel();
            IntroducaoModel.LojaIntroducao = model;
            IntroducaoModel.LojaIntroducaoTexto = model.LOJA_LojaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.LOJA_LojaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarLojaIntroducaoPartial", IntroducaoModel);

        }

        [HttpGet]
        public ActionResult LojaIntroducaoImagem(int? id)
        {

            LOJA_LojaIntroducaoTexto model = db.LOJA_LojaIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.LojaIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemLojaIntroducaoPreviewPartial", imgModel);

        }

        [HttpPost]
        public ActionResult ApagarLojaIntroducaoConfirmar(LojaIntroducaoViewModel model)
        {
            LOJA_LojaIntroducao LojaIntroducao = db.LOJA_LojaIntroducaoTexto.Include("CONF_Idioma").Include("LOJA_Loja")
                  .Select(x => x.LOJA_LojaIntroducao).Where(c => c.Id == model.LojaIntroducao.Id).FirstOrDefault();

            foreach (var item in LojaIntroducao.LOJA_LojaIntroducaoTexto.ToList())
            {
                db.LOJA_LojaIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.LOJA_LojaIntroducao.Attach(LojaIntroducao);

            db.LOJA_LojaIntroducao.Remove(LojaIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        public JsonResult ListaLojaContacto([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<LOJA_Contacto> query = db.LOJA_Contacto
               
                .Include("Loja_ContactoTexto")
                .Include("CONT_TipoContacto")
                .Include("CONT_TipoContactoTexto")
                .Include("CONF_Idioma")
                .Where(m => m.Loja_ContactoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Tipo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Ordem = m.Ordem,
                    Tipo = (m.CONT_TipoContacto.CONT_TipoContactoTexto.FirstOrDefault().Descricao.Length < 70
                                ? m.CONT_TipoContacto.CONT_TipoContactoTexto.FirstOrDefault().Descricao
                                : m.CONT_TipoContacto.CONT_TipoContactoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                    Descricao = (m.Loja_ContactoTexto.FirstOrDefault().Descricao.Length < 70
                                ? m.Loja_ContactoTexto.FirstOrDefault().Descricao
                                : m.Loja_ContactoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                  
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }
        
        [HttpGet]
        public ActionResult CriarLojaContacto()
        {

            LojaContactoViewModel model = new LojaContactoViewModel();

            List<Loja_ContactoTexto> lista = new List<Loja_ContactoTexto>();

            Loja_ContactoTexto texto = new Loja_ContactoTexto();
            texto.CONF_Idioma = db.CONF_Idioma.Where(i => i.Codigo.ToUpper() == "PT").FirstOrDefault();
            lista.Add(texto);

            model.LojaContactoTexto = lista;           

            
            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
            ViewBag.Lojas = new SelectList(db.LOJA_Loja.ToList(), "ID", "Nome");

            return View("CriarLojaContacto", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarLojaContacto(LojaContactoViewModel model)
        {


            model.LojaContacto.DataCriacao = DateTime.Now;
            model.LojaContacto.DataModificacao = DateTime.Now;

            int i = 0;

            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (Loja_ContactoTexto textos in model.LojaContactoTexto)
            {

                textos.LOJA_Contacto = model.LojaContacto;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.LojaContacto.Loja_ContactoTexto = model.LojaContactoTexto;
            db.LOJA_Contacto.Add(model.LojaContacto);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesLojaContacto(int? id)
        {
           LOJA_Contacto model = db.LOJA_Contacto
              .Include("Loja_ContactoTexto")    
              .Where(c => c.Id == id).FirstOrDefault();
             



            LojaContactoViewModel ContactoModel = new LojaContactoViewModel();
            ContactoModel.LojaContacto = model;
            ContactoModel.LojaContactoTexto =  model.Loja_ContactoTexto.ToList();
            //ContactoModel.Idioma = model.Loja_ContactoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
            ViewBag.Lojas = new SelectList(db.LOJA_Loja.ToList(), "ID", "Nome");


            return PartialView("_DetalhesLojaContactoPartial", ContactoModel);

        }



        [HttpGet]
        public ActionResult EditarLojaContacto(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOJA_Contacto model = db.Loja_ContactoTexto.Include("CONF_Idioma").Include("LOJA_Contacto")
              .Select(x => x.LOJA_Contacto).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            LojaContactoViewModel ContactoModel = new LojaContactoViewModel();
            ContactoModel.LojaContacto = model;
            ContactoModel.LojaContactoTexto = model.Loja_ContactoTexto.ToList();
            ContactoModel.Idioma = model.Loja_ContactoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
            ViewBag.Lojas = new SelectList(db.LOJA_Loja.ToList(), "ID", "Nome");
          


            return View("EditarLojaContacto", ContactoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarLojaContacto(LojaContactoViewModel model)
        {


            //int i = 0;
           



            db.Database.ExecuteSqlCommand("Update LOJA_Contacto SET TipoContactoId = " + model.LojaContacto.TipoContactoId + ", LojaId = " + model.LojaContacto.LojaId + ", " 
                + " HorarioInicio = '" + model.LojaContacto.HorarioInicio + "', HorarioFim = '"+ model.LojaContacto.HorarioFim + "',  Activo = '" + model.LojaContacto.Activo + "', Ordem = "+ model.LojaContacto.Ordem + " " 
                + "  WHERE ID= " + model.LojaContacto.Id);

            /*
            db.LOJA_Loja.Attach(model.Loja);
            db.Entry(model.Loja).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            //i = 0;

            foreach (Loja_ContactoTexto texto in model.LojaContactoTexto)
            {

                db.Database.ExecuteSqlCommand("Update Loja_ContactoTexto SET  Descricao = '" + texto.Descricao + "'  " + " WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Loja");

                return PartialView("_EditarCategoriaLojaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }



        [HttpGet]
        public ActionResult ApagarLojaContacto(int id)
        {

            LOJA_Contacto model = db.LOJA_Contacto
             .Include("Loja_ContactoTexto")
             .Where(c => c.Id == id).FirstOrDefault();




            LojaContactoViewModel ContactoModel = new LojaContactoViewModel();
            ContactoModel.LojaContacto = model;
            ContactoModel.LojaContactoTexto = model.Loja_ContactoTexto.ToList();
            //ContactoModel.Idioma = model.Loja_ContactoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
            ViewBag.Lojas = new SelectList(db.LOJA_Loja.ToList(), "ID", "Nome");


            return PartialView("_ApagarLojaContactoPartial", ContactoModel);


        }


        [HttpPost]
        public ActionResult ApagarLojaContactoConfirmar(LojaContactoViewModel model)
        {
            LOJA_Contacto LojaContacto = db.Loja_ContactoTexto.Include("CONF_Idioma").Include("LOJA_Loja")
                  .Select(x => x.LOJA_Contacto).Where(c => c.Id == model.LojaContacto.Id).FirstOrDefault();

            foreach (var item in LojaContacto.Loja_ContactoTexto.ToList())
            {
                db.Loja_ContactoTexto.Remove(item);
            }

            db.SaveChanges();

            db.LOJA_Contacto.Attach(LojaContacto);

            db.LOJA_Contacto.Remove(LojaContacto);

            db.SaveChanges();


            return RedirectToAction("Index");
        }



    }
}