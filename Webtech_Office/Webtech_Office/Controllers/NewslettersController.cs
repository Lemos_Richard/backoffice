﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    public class NewslettersController : Controller
    {

        private CMS_DbContext db = new CMS_DbContext();

        // GET: Newsletters
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListaNewsletters([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<CONT_Newsletters> query = db.CONT_Newsletters;
              

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

          

                      query.Where(mt => mt.Email.Contains(value));
                      

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Email asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {


                    Email = m.Email,
                    Data = SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", m.Data).Trim().Length) + SqlFunctions.DateName("dd", m.Data).Trim() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)m.Data.Month).TrimStart().Length) + SqlFunctions.StringConvert((double)m.Data.Month).TrimStart() + "/" + SqlFunctions.DateName("year", m.Data)

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

    }
}