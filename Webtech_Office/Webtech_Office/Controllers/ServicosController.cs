﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    public class ServicosController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        // GET: Servicos
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListaServico([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<SERV_Servico> query = db.SERV_Servico
               .Include("SERV_ServicoTexto").Include("SERV_TipoServico")
               .Include("SERV_TipoServicoTexto").Include("CONF_Idioma")
           .Where(m => m.SERV_ServicoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Tipo = (m.SERV_TipoServico.SERV_TipoServicoTexto.FirstOrDefault().Descricao.Length <= 70 ? m.SERV_TipoServico.SERV_TipoServicoTexto.FirstOrDefault().Descricao
                        : m.SERV_TipoServico.SERV_TipoServicoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),


                    Descricao = (m.SERV_ServicoTexto.FirstOrDefault().Descricao.Length <= 70 ? m.SERV_ServicoTexto.FirstOrDefault().Descricao
                        : m.SERV_ServicoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                    Introducao = (m.SERV_ServicoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.SERV_ServicoTexto.FirstOrDefault().Introducao
                        : m.SERV_ServicoTexto.FirstOrDefault().Introducao.Substring(0, 70) + "..."),                  

                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarServico()
        {
            ServicoViewModel model = new ServicoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                SERV_ServicoTexto ServicoTexto = new SERV_ServicoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ServicoTexto.Add(ServicoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }



            var query = from tr in db.SERV_TipoServico
                        join trt in db.SERV_TipoServicoTexto on tr.Id equals trt.TipoServicoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoServicos = new SelectList(query, "Id", "Descricao");





            return View("CriarServico", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarServico(ServicoViewModel model)
        {


            model.Servico.DataCriacao = DateTime.Now;
            model.Servico.Destaque = false;
            model.Servico.PartilhaFacebook = false;
            model.Servico.PartilhaTwitter = false;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoTexto[i].Banner = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (SERV_ServicoTexto textos in model.ServicoTexto)
            {

                textos.SERV_Servico = model.Servico;
                textos.CONF_Idioma = idioma[i];
                textos.Conteudo = System.Net.WebUtility.HtmlDecode(textos.Conteudo);

                i++;

            }



            model.Servico.SERV_ServicoTexto = model.ServicoTexto;
            db.SERV_Servico.Add(model.Servico);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult ServicoImagem(int? id)
        {

            SERV_ServicoTexto model = db.SERV_ServicoTexto.Include("CONF_Idioma")
                .Where(m => m.ServicoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Banner;

            return PartialView("_ImagemServicoPreviewPartial", imgModel);

        }



        [HttpGet]
        public ActionResult DetalhesServico(int? id)
        {
            SERV_Servico model = db.SERV_ServicoTexto.Include("CONF_Idioma").Include("SERV_Servico")
               .Select(x => x.SERV_Servico).Where(c => c.Id == id).FirstOrDefault();



            ServicoViewModel ServicoModel = new ServicoViewModel();
            ServicoModel.Servico = model;
            ServicoModel.ServicoTexto = model.SERV_ServicoTexto.ToList();
            ServicoModel.Idioma = model.SERV_ServicoTexto.Select(x => x.CONF_Idioma).ToList();



            var query = from tr in db.SERV_TipoServico
                        join trt in db.SERV_TipoServicoTexto on tr.Id equals trt.TipoServicoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoServicos = new SelectList(query, "Id", "Descricao");







            return PartialView("_DetalhesServico", ServicoModel);

        }



        [HttpGet]
        public ActionResult EditarServico(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERV_Servico model = db.SERV_ServicoTexto.Include("CONF_Idioma").Include("SERV_Servico")
              .Select(x => x.SERV_Servico).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ServicoViewModel ServicoModel = new ServicoViewModel();
            ServicoModel.Servico = model;
            ServicoModel.ServicoTexto = model.SERV_ServicoTexto.ToList();
            ServicoModel.Idioma = model.SERV_ServicoTexto.Select(x => x.CONF_Idioma).ToList();




            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                ServicoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                ServicoModel.FileDocumento.Add(doc);

            }


            var query = from tr in db.SERV_TipoServico
                        join trt in db.SERV_TipoServicoTexto on tr.Id equals trt.TipoServicoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoServicos = new SelectList(query, "Id", "Descricao");



            return View("EditarServico", ServicoModel);


        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarServico(ServicoViewModel model)
        {
         
         
            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoTexto[i].Banner = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoTexto[i].PDF = fileName;

                }
                i++;

            }


            model.Servico.Destaque = false;
            model.Servico.PartilhaFacebook = false;
            model.Servico.PartilhaTwitter = false;



            db.Database.ExecuteSqlCommand("Update SERV_Servico SET  Activo = '" + model.Servico.Activo
                  + "', TipoServicoId = "+ model.Servico.TipoServicoID +", Destaque = '"+ model.Servico.Destaque 
                  + "', PartilhaFacebook = '"+ model.Servico.PartilhaFacebook +"', PartilhaTwitter = '"+ model.Servico.PartilhaTwitter +"'   WHERE ID= " + model.Servico.Id);

            db.SaveChanges();

            i = 0;

            foreach (SERV_ServicoTexto texto in model.ServicoTexto)
            {


                db.Database.ExecuteSqlCommand("Update SERV_ServicoTexto SET  Descricao = '" + texto.Descricao + "', "
                  + "Introducao = '" + texto.Introducao + "', Banner='" + texto.Banner + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
                db.SaveChanges();
            }
            



            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarServico(int? id)
        {

            SERV_Servico model = db.SERV_ServicoTexto.Include("CONF_Idioma").Include("SERV_Servico")
                   .Select(x => x.SERV_Servico).Where(c => c.Id == id).FirstOrDefault();



            ServicoViewModel ServicoModel = new ServicoViewModel();
            ServicoModel.Servico = model;
            ServicoModel.ServicoTexto = model.SERV_ServicoTexto.ToList();
            ServicoModel.Idioma = model.SERV_ServicoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.SERV_TipoServico
                        join trt in db.SERV_TipoServicoTexto on tr.Id equals trt.TipoServicoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoServicos = new SelectList(query, "Id", "Descricao");




            return PartialView("_ApagarServicoPartial", ServicoModel);

        }


        [HttpPost]
        public ActionResult ApagarServicoConfirmar(ServicoViewModel model)
        {
            SERV_Servico Servico = db.SERV_ServicoTexto.Include("CONF_Idioma").Include("SERV_Servico")
                  .Select(x => x.SERV_Servico).Where(c => c.Id == model.Servico.Id).FirstOrDefault();

            foreach (var item in Servico.SERV_ServicoTexto.ToList())
            {
                db.SERV_ServicoTexto.Remove(item);
            }

            db.SaveChanges();

            db.SERV_Servico.Attach(Servico);

            db.SERV_Servico.Remove(Servico);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<SERV_ServicoBanner> query = db.SERV_ServicoBanner
                .Include("SERV_ServicoBannerTexto")

                .Include("CONF_Idioma")
                .Where(m => m.SERV_ServicoBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.SERV_ServicoBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.SERV_ServicoBannerTexto.FirstOrDefault().Titulo
                        : m.SERV_ServicoBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.SERV_ServicoBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.SERV_ServicoBannerTexto.FirstOrDefault().Introducao
                        : m.SERV_ServicoBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.SERV_ServicoBannerTexto.FirstOrDefault().Link,
                    PDF = m.SERV_ServicoBannerTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarServicoBanner()
        {
            ServicoBannerViewModel model = new ServicoBannerViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                SERV_ServicoBannerTexto ServicoTexto = new SERV_ServicoBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ServicoBannerTexto.Add(ServicoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarServicoBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarServicoBanner(ServicoBannerViewModel model)
        {


            model.ServicoBanner.DataCriacao = DateTime.Now;
            model.ServicoBanner.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (SERV_ServicoBannerTexto textos in model.ServicoBannerTexto)
            {

                textos.SERV_ServicoBanner = model.ServicoBanner;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.ServicoBanner.SERV_ServicoBannerTexto = model.ServicoBannerTexto;
            db.SERV_ServicoBanner.Add(model.ServicoBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesServicoBanner(int id)
        {
            SERV_ServicoBanner model = db.SERV_ServicoBannerTexto.Include("CONF_Idioma").Include("SERV_ServicoBanner")
               .Select(x => x.SERV_ServicoBanner).Where(c => c.Id == id).FirstOrDefault();



            ServicoBannerViewModel BannerModel = new ServicoBannerViewModel();
            BannerModel.ServicoBanner = model;
            BannerModel.ServicoBannerTexto = model.SERV_ServicoBannerTexto.ToList();
            BannerModel.Idioma = model.SERV_ServicoBannerTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesServicoBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarServicoBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERV_ServicoBanner model = db.SERV_ServicoBannerTexto.Include("CONF_Idioma").Include("SERV_ServicoBanner")
              .Select(x => x.SERV_ServicoBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ServicoBannerViewModel BannerModel = new ServicoBannerViewModel();
            BannerModel.ServicoBanner = model;
            BannerModel.ServicoBannerTexto = model.SERV_ServicoBannerTexto.ToList();
            BannerModel.Idioma = model.SERV_ServicoBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarServicoBanner", BannerModel);


        }


        [HttpPost]

        public ActionResult EditarServicoBanner(ServicoBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoBannerTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update SERV_ServicoBanner SET  Activo = '" + model.ServicoBanner.Activo
                  + "'  WHERE ID= " + model.ServicoBanner.Id);

            /*
            db.SERV_Servico.Attach(model.Servico);
            db.Entry(model.Servico).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (SERV_ServicoBannerTexto texto in model.ServicoBannerTexto)
            {


                db.Database.ExecuteSqlCommand("Update SERV_ServicoBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Servico");

                return PartialView("_EditarCategoriaServicoPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarServicoBanner(int? id)
        {

            SERV_ServicoBanner model = db.SERV_ServicoBannerTexto.Include("CONF_Idioma").Include("SERV_ServicoBanner")
                   .Select(x => x.SERV_ServicoBanner).Where(c => c.Id == id).FirstOrDefault();



            ServicoBannerViewModel BannerModel = new ServicoBannerViewModel();
            BannerModel.ServicoBanner = model;
            BannerModel.ServicoBannerTexto = model.SERV_ServicoBannerTexto.ToList();
            BannerModel.Idioma = model.SERV_ServicoBannerTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarServicoBannerPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult ServicoBannerImagem(int? id)
        {

            SERV_ServicoBannerTexto model = db.SERV_ServicoBannerTexto.Include("CONF_Idioma")
                .Where(m => m.ServicoBannerId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemServicoBannerPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarServicoBannerConfirmar(ServicoBannerViewModel model)
        {
            SERV_ServicoBanner ServicoBanner = db.SERV_ServicoBannerTexto.Include("CONF_Idioma").Include("SERV_Servico")
                  .Select(x => x.SERV_ServicoBanner).Where(c => c.Id == model.ServicoBanner.Id).FirstOrDefault();

            foreach (var item in ServicoBanner.SERV_ServicoBannerTexto.ToList())
            {
                db.SERV_ServicoBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.SERV_ServicoBanner.Attach(ServicoBanner);

            db.SERV_ServicoBanner.Remove(ServicoBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaServicoIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<SERV_ServicoIntroducao> query = db.SERV_ServicoIntroducao
                .Include("SERV_ServicoIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.SERV_ServicoIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.SERV_ServicoIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.SERV_ServicoIntroducaoTexto.FirstOrDefault().Titulo
                        : m.SERV_ServicoIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.SERV_ServicoIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.SERV_ServicoIntroducaoTexto.FirstOrDefault().Introducao
                        : m.SERV_ServicoIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarServicoIntroducao()
        {
            ServicoIntroducaoViewModel model = new ServicoIntroducaoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                SERV_ServicoIntroducaoTexto ServicoTexto = new SERV_ServicoIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ServicoIntroducaoTexto.Add(ServicoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarServicoIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarServicoIntroducao(ServicoIntroducaoViewModel model)
        {


            model.ServicoIntroducao.DataCriacao = DateTime.Now;
            model.ServicoIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (SERV_ServicoIntroducaoTexto textos in model.ServicoIntroducaoTexto)
            {

                textos.SERV_ServicoIntroducao = model.ServicoIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.ServicoIntroducao.SERV_ServicoIntroducaoTexto = model.ServicoIntroducaoTexto;
            db.SERV_ServicoIntroducao.Add(model.ServicoIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult DetalhesServicoIntroducao(int? id)
        {
            SERV_ServicoIntroducao model = db.SERV_ServicoIntroducaoTexto.Include("CONF_Idioma").Include("SERV_ServicoIntroducao")
               .Select(x => x.SERV_ServicoIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ServicoIntroducaoViewModel IntroducaoModel = new ServicoIntroducaoViewModel();
            IntroducaoModel.ServicoIntroducao = model;
            IntroducaoModel.ServicoIntroducaoTexto = model.SERV_ServicoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.SERV_ServicoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesServicoIntroducao", IntroducaoModel);

        }
        [HttpGet]
        public ActionResult EditarServicoIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERV_ServicoIntroducao model = db.SERV_ServicoIntroducaoTexto.Include("CONF_Idioma").Include("SERV_ServicoIntroducao")
              .Select(x => x.SERV_ServicoIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ServicoIntroducaoViewModel IntroducaoModel = new ServicoIntroducaoViewModel();
            IntroducaoModel.ServicoIntroducao = model;
            IntroducaoModel.ServicoIntroducaoTexto = model.SERV_ServicoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.SERV_ServicoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarServicoIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarServicoIntroducao(ServicoIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ServicoIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update SERV_ServicoIntroducao SET  Activo = '" + model.ServicoIntroducao.Activo
                  + "'  WHERE ID= " + model.ServicoIntroducao.Id);

            /*
            db.SERV_Servico.Attach(model.Servico);
            db.Entry(model.Servico).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (SERV_ServicoIntroducaoTexto texto in model.ServicoIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update SERV_ServicoIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Servico");

                return PartialView("_EditarCategoriaServicoPartial", model);
            }
            */


            return RedirectToAction("Index");


        }

        [HttpGet]
        public ActionResult ApagarServicoIntroducao(int id)
        {

            SERV_ServicoIntroducao model = db.SERV_ServicoIntroducaoTexto.Include("CONF_Idioma").Include("SERV_ServicoIntroducao")
                   .Select(x => x.SERV_ServicoIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ServicoIntroducaoViewModel IntroducaoModel = new ServicoIntroducaoViewModel();
            IntroducaoModel.ServicoIntroducao = model;
            IntroducaoModel.ServicoIntroducaoTexto = model.SERV_ServicoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.SERV_ServicoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarServicoIntroducaoPartial", IntroducaoModel);

        }

        [HttpGet]
        public ActionResult ServicoIntroducaoImagem(int? id)
        {

            SERV_ServicoIntroducaoTexto model = db.SERV_ServicoIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.ServicoIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemServicoPreviewPartial", imgModel);

        }

        [HttpPost]
        public ActionResult ApagarServicoIntroducaoConfirmar(ServicoIntroducaoViewModel model)
        {
            SERV_ServicoIntroducao ServicoIntroducao = db.SERV_ServicoIntroducaoTexto.Include("CONF_Idioma").Include("SERV_Servico")
                  .Select(x => x.SERV_ServicoIntroducao).Where(c => c.Id == model.ServicoIntroducao.Id).FirstOrDefault();

            foreach (var item in ServicoIntroducao.SERV_ServicoIntroducaoTexto.ToList())
            {
                db.SERV_ServicoIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.SERV_ServicoIntroducao.Attach(ServicoIntroducao);

            db.SERV_ServicoIntroducao.Remove(ServicoIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


 
        public JsonResult ListaTipoServico([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<SERV_TipoServico> query = db.SERV_TipoServico.Include("SERV_TipoServicoTexto")
                .Where(m => m.SERV_TipoServicoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));




            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();


                query = query
                    .Where(m => m.SERV_TipoServicoTexto.Any(mt => mt.Descricao.Contains(value)));



            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(m => new
                {
                    TipoServicoId = m.Id,
                    Descricao = (m.SERV_TipoServicoTexto.FirstOrDefault().Descricao.Length <= 70 ? m.SERV_TipoServicoTexto.FirstOrDefault().Descricao
                        : m.SERV_TipoServicoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                    Introducao = (m.SERV_TipoServicoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.SERV_TipoServicoTexto.FirstOrDefault().Introducao
                        : m.SERV_TipoServicoTexto.FirstOrDefault().Introducao.Substring(0, 70) + "..."),

                    Activo = m.Activo,

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);


        }

        //Tipo de Servicos
        [HttpGet]
        public ActionResult CriarTipoServico()
        {
            TipoServicoViewModel model = new TipoServicoViewModel();
            model.TipoServico.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                SERV_TipoServicoTexto texto = new SERV_TipoServicoTexto()
                {

                    CONF_Idioma = idioma

                };
                model.TipoServicoTexto.Add(texto);

            }

            return PartialView("_CriarTipoServico", model);

        }


        [HttpPost]
        public async Task<ActionResult> CriarTipoServico(TipoServicoViewModel model)
        {
            model.TipoServico.DataCriacao = DateTime.Now;


            int i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (SERV_TipoServicoTexto textos in model.TipoServicoTexto)
            {

                textos.SERV_TipoServico = model.TipoServico;
                textos.CONF_Idioma = idioma[i];
                i++;

            }

            model.TipoServico.SERV_TipoServicoTexto = model.TipoServicoTexto;
            db.SERV_TipoServico.Add(model.TipoServico);
            var task = db.SaveChangesAsync();
            await task;

            if (task.Exception != null)
            {
                ModelState.AddModelError("", "Impossivel criar tipo de receita");

                return View("_CriarTipoServico", model);
            }

            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }

            return RedirectToAction("Index");





        }

        [HttpGet]
        public ActionResult DetalhesTipoServico(int? id)
        {
            SERV_TipoServico model = db.SERV_TipoServico.Include("SERV_TipoServicoTexto")             
                 .Where(m => m.SERV_TipoServicoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")  
                    && mt.SERV_TipoServico.Id == id))
                 .FirstOrDefault();
               




            TipoServicoViewModel TipoServicoModel = new TipoServicoViewModel();
            TipoServicoModel.TipoServico = model;
            TipoServicoModel.TipoServicoTexto = model.SERV_TipoServicoTexto.ToList();
            TipoServicoModel.Idioma = model.SERV_TipoServicoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesTipoServicoPartial", TipoServicoModel);

        }


        [HttpGet]
        public ActionResult ApagarTipoServico(int? id)
        {
            SERV_TipoServico model = db.SERV_TipoServico.Include("SERV_TipoServicoTexto")
                .Include("SERV_Servico").Include("SERV_Servico")             
                   .Where(m => m.Id == id)
             .SingleOrDefault();



            if (model.SERV_Servico.Count > 0)
            {
                ViewBag.ApagarRelacao = "O Tipo não pode ser apagdo por ter receitas relacionadas";
            }

            TipoServicoViewModel TipoServicoModel = new TipoServicoViewModel();
            TipoServicoModel.TipoServico = model;
            TipoServicoModel.TipoServicoTexto = model.SERV_TipoServicoTexto.ToList();
            TipoServicoModel.Idioma = model.SERV_TipoServicoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_ApagarTipoServicoPartial", TipoServicoModel);

        }

        /*Apagar Menu Post*/
        [HttpPost]
        public ActionResult ApagarTipoServicoConfirmar(TipoServicoViewModel model)
        {
            SERV_TipoServico tipo = db.SERV_TipoServico.Include("SERV_TipoServicoTexto")
                .FirstOrDefault(x => x.Id == model.TipoServico.Id);



            foreach (var item in tipo.SERV_TipoServicoTexto.ToList())
            {
                db.SERV_TipoServicoTexto.Remove(item);
            }

            var task = db.SaveChanges();
         

        



            db.SERV_TipoServico.Attach(tipo);

            db.SERV_TipoServico.Remove(tipo);

            db.SaveChanges();
  
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditarTipoServico(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERV_TipoServico model = db.SERV_TipoServico
                .Include("SERV_TipoServicoTexto")                
                .Where(c => c.Id == id)
                .SingleOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            TipoServicoViewModel TipoServicoModel = new TipoServicoViewModel();
            TipoServicoModel.TipoServico = model;
            TipoServicoModel.TipoServicoTexto = model.SERV_TipoServicoTexto.ToList();
            TipoServicoModel.Idioma = model.SERV_TipoServicoTexto.Select(x => x.CONF_Idioma).ToList();


            return PartialView("_EditarTipoServicoPartial", TipoServicoModel);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarTipoServico(TipoServicoViewModel model)
        {




            db.SERV_TipoServico.Attach(model.TipoServico);
            db.Entry(model.TipoServico).State = EntityState.Modified;
            int i = 0;

            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                model.TipoServicoTexto[i].IdiomaId = idioma.Id;
                model.TipoServicoTexto[i].TipoServicoId = model.TipoServico.Id;
                model.TipoServicoTexto[i].CONF_Idioma = idioma;
                model.TipoServicoTexto[i].SERV_TipoServico = model.TipoServico;

                bool saveFailed;
                do
                {
                    saveFailed = false;

                    try
                    {
                        db.Entry(model.TipoServicoTexto[i]).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store 
                        ex.Entries.Single().Reload();
                    }

                } while (saveFailed);


                i++;
            }

            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }
    }
}