﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    [Authorize]
    public class QuemSomosController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        // GET: Quem
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<QUEM_QuemBanner> query = db.QUEM_QuemBanner
                .Include("QUEM_QuemBannerTexto")
                .Include("CONF_Idioma")
                .Where(m => m.QUEM_QuemBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.QUEM_QuemBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.QUEM_QuemBannerTexto.FirstOrDefault().Titulo
                        : m.QUEM_QuemBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.QUEM_QuemBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.QUEM_QuemBannerTexto.FirstOrDefault().Introducao
                        : m.QUEM_QuemBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.QUEM_QuemBannerTexto.FirstOrDefault().Link,
                    PDF = m.QUEM_QuemBannerTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarQuemBanner()
        {
            QuemBannerViewModel model = new QuemBannerViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                QUEM_QuemBannerTexto QuemTexto = new QUEM_QuemBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.QuemBannerTexto.Add(QuemTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarQuemBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarQuemBanner(QuemBannerViewModel model)
        {


            model.QuemBanner.DataCriacao = DateTime.Now;
            model.QuemBanner.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (QUEM_QuemBannerTexto textos in model.QuemBannerTexto)
            {

                textos.QUEM_QuemBanner = model.QuemBanner;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.QuemBanner.QUEM_QuemBannerTexto = model.QuemBannerTexto;
            db.QUEM_QuemBanner.Add(model.QuemBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesQuemBanner(int? id)
        {
            QUEM_QuemBanner model = db.QUEM_QuemBannerTexto.Include("CONF_Idioma").Include("QUEM_QuemBanner")
               .Select(x => x.QUEM_QuemBanner).Where(c => c.Id == id).FirstOrDefault();



            QuemBannerViewModel BannerModel = new QuemBannerViewModel();
            BannerModel.QuemBanner = model;
            BannerModel.QuemBannerTexto = model.QUEM_QuemBannerTexto.ToList();
            BannerModel.Idioma = model.QUEM_QuemBannerTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesQuemBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarQuemBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QUEM_QuemBanner model = db.QUEM_QuemBannerTexto.Include("CONF_Idioma").Include("QUEM_QuemBanner")
              .Select(x => x.QUEM_QuemBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            QuemBannerViewModel BannerModel = new QuemBannerViewModel();
            BannerModel.QuemBanner = model;
            BannerModel.QuemBannerTexto = model.QUEM_QuemBannerTexto.ToList();
            BannerModel.Idioma = model.QUEM_QuemBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarQuemBanner", BannerModel);


        }


        [HttpPost]

        public ActionResult EditarQuemBanner(QuemBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemBannerTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update QUEM_QuemBanner SET  Activo = '" + model.QuemBanner.Activo
                  + "'  WHERE ID= " + model.QuemBanner.Id);

            /*
            db.QUEM_Quem.Attach(model.Quem);
            db.Entry(model.Quem).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (QUEM_QuemBannerTexto texto in model.QuemBannerTexto)
            {


                db.Database.ExecuteSqlCommand("Update QUEM_QuemBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Quem");

                return PartialView("_EditarCategoriaQuemPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarQuemBanner(int? id)
        {

            QUEM_QuemBanner model = db.QUEM_QuemBannerTexto.Include("CONF_Idioma").Include("QUEM_QuemBanner")
                   .Select(x => x.QUEM_QuemBanner).Where(c => c.Id == id).FirstOrDefault();



            QuemBannerViewModel BannerModel = new QuemBannerViewModel();
            BannerModel.QuemBanner = model;
            BannerModel.QuemBannerTexto = model.QUEM_QuemBannerTexto.ToList();
            BannerModel.Idioma = model.QUEM_QuemBannerTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarQuemBannerPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult QuemBannerImagem(int? id)
        {

            QUEM_QuemBannerTexto model = db.QUEM_QuemBannerTexto.Include("CONF_Idioma")
                .Where(m => m.QuemBannerId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarQuemBannerConfirmar(QuemBannerViewModel model)
        {
            QUEM_QuemBanner QuemBanner = db.QUEM_QuemBannerTexto.Include("CONF_Idioma").Include("QUEM_Quem")
                  .Select(x => x.QUEM_QuemBanner).Where(c => c.Id == model.QuemBanner.Id).FirstOrDefault();

            foreach (var item in QuemBanner.QUEM_QuemBannerTexto.ToList())
            {
                db.QUEM_QuemBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.QUEM_QuemBanner.Attach(QuemBanner);

            db.QUEM_QuemBanner.Remove(QuemBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaQuemIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<QUEM_QuemIntroducao> query = db.QUEM_QuemIntroducao
                .Include("QUEM_QuemIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.QUEM_QuemIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.QUEM_QuemIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.QUEM_QuemIntroducaoTexto.FirstOrDefault().Titulo
                        : m.QUEM_QuemIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.QUEM_QuemIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.QUEM_QuemIntroducaoTexto.FirstOrDefault().Introducao
                        : m.QUEM_QuemIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarQuemIntroducao()
        {
            QuemIntroducaoViewModel model = new QuemIntroducaoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                QUEM_QuemIntroducaoTexto QuemTexto = new QUEM_QuemIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.QuemIntroducaoTexto.Add(QuemTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarQuemIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarQuemIntroducao(QuemIntroducaoViewModel model)
        {


            model.QuemIntroducao.DataCriacao = DateTime.Now;
            model.QuemIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (QUEM_QuemIntroducaoTexto textos in model.QuemIntroducaoTexto)
            {

                textos.QUEM_QuemIntroducao = model.QuemIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.QuemIntroducao.QUEM_QuemIntroducaoTexto = model.QuemIntroducaoTexto;
            db.QUEM_QuemIntroducao.Add(model.QuemIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesQuemIntroducao(int? id)
        {
            QUEM_QuemIntroducao model = db.QUEM_QuemIntroducaoTexto.Include("CONF_Idioma").Include("QUEM_QuemIntroducao")
               .Select(x => x.QUEM_QuemIntroducao).Where(c => c.Id == id).FirstOrDefault();



            QuemIntroducaoViewModel IntroducaoModel = new QuemIntroducaoViewModel();
            IntroducaoModel.QuemIntroducao = model;
            IntroducaoModel.QuemIntroducaoTexto = model.QUEM_QuemIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.QUEM_QuemIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesQuemIntroducao", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarQuemIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QUEM_QuemIntroducao model = db.QUEM_QuemIntroducaoTexto.Include("CONF_Idioma").Include("QUEM_QuemIntroducao")
              .Select(x => x.QUEM_QuemIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            QuemIntroducaoViewModel IntroducaoModel = new QuemIntroducaoViewModel();
            IntroducaoModel.QuemIntroducao = model;
            IntroducaoModel.QuemIntroducaoTexto = model.QUEM_QuemIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.QUEM_QuemIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarQuemIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarQuemIntroducao(QuemIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update QUEM_QuemIntroducao SET  Activo = '" + model.QuemIntroducao.Activo
                  + "'  WHERE ID= " + model.QuemIntroducao.Id);

            /*
            db.QUEM_Quem.Attach(model.Quem);
            db.Entry(model.Quem).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (QUEM_QuemIntroducaoTexto texto in model.QuemIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update QUEM_QuemIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Quem");

                return PartialView("_EditarCategoriaQuemPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarQuemIntroducao(int id)
        {

            QUEM_QuemIntroducao model = db.QUEM_QuemIntroducaoTexto.Include("CONF_Idioma").Include("QUEM_QuemIntroducao")
                   .Select(x => x.QUEM_QuemIntroducao).Where(c => c.Id == id).FirstOrDefault();



            QuemIntroducaoViewModel IntroducaoModel = new QuemIntroducaoViewModel();
            IntroducaoModel.QuemIntroducao = model;
            IntroducaoModel.QuemIntroducaoTexto = model.QUEM_QuemIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.QUEM_QuemIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarQuemIntroducaoPartial", IntroducaoModel);

        }

     
        [HttpPost]
        public ActionResult ApagarQuemIntroducaoConfirmar(QuemIntroducaoViewModel model)
        {
            QUEM_QuemIntroducao QuemIntroducao = db.QUEM_QuemIntroducaoTexto.Include("CONF_Idioma").Include("QUEM_Quem")
                  .Select(x => x.QUEM_QuemIntroducao).Where(c => c.Id == model.QuemIntroducao.Id).FirstOrDefault();

            foreach (var item in QuemIntroducao.QUEM_QuemIntroducaoTexto.ToList())
            {
                db.QUEM_QuemIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.QUEM_QuemIntroducao.Attach(QuemIntroducao);

            db.QUEM_QuemIntroducao.Remove(QuemIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }






        [HttpGet]
        public ActionResult QuemIntroducaoImagem(int? id)
        {

            QUEM_QuemIntroducaoTexto model = db.QUEM_QuemIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.QuemIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }



        public JsonResult ListaQuemSomos([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<QUEM_Acerca> query = db.QUEM_Acerca
                .Include("QUEM_AcercaTexto")

                .Include("CONF_Idioma")
                .Where(m => m.QUEM_AcercaTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.QUEM_AcercaTexto.FirstOrDefault().Titulo.Length <= 70 ? m.QUEM_AcercaTexto.FirstOrDefault().Titulo
                        : m.QUEM_AcercaTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.QUEM_AcercaTexto.FirstOrDefault().Introducao.Length <= 70 ? m.QUEM_AcercaTexto.FirstOrDefault().Introducao
                        : m.QUEM_AcercaTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                   
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarQuemSomos()
        {
            QuemSomosViewModel model = new QuemSomosViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                QUEM_AcercaTexto QuemTexto = new QUEM_AcercaTexto()
                {
                    CONF_Idioma = idioma
                };
                model.QuemSomosTexto.Add(QuemTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarQuemSomos", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarQuemSomos(QuemSomosViewModel model)
        {


            model.QuemSomos.DataCriacao = DateTime.Now;
            model.QuemSomos.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemSomosTexto[i].Banner = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemSomosTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (QUEM_AcercaTexto textos in model.QuemSomosTexto)
            {

                textos.QUEM_Acerca = model.QuemSomos;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.QuemSomos.QUEM_AcercaTexto = model.QuemSomosTexto;
            db.QUEM_Acerca.Add(model.QuemSomos);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesQuemSomos(int? id)
        {
            QUEM_Acerca model = db.QUEM_AcercaTexto.Include("CONF_Idioma").Include("QUEM_Acerca")
               .Select(x => x.QUEM_Acerca).Where(c => c.Id == id).FirstOrDefault();



            QuemSomosViewModel IntroducaoModel = new QuemSomosViewModel();
            IntroducaoModel.QuemSomos = model;
            IntroducaoModel.QuemSomosTexto = model.QUEM_AcercaTexto.ToList();
            IntroducaoModel.Idioma = model.QUEM_AcercaTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesQuemSomos", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarQuemSomos(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QUEM_Acerca model = db.QUEM_AcercaTexto.Include("CONF_Idioma").Include("QUEM_Acerca")
              .Select(x => x.QUEM_Acerca).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            QuemSomosViewModel IntroducaoModel = new QuemSomosViewModel();
            IntroducaoModel.QuemSomos = model;
            IntroducaoModel.QuemSomosTexto = model.QUEM_AcercaTexto.ToList();
            IntroducaoModel.Idioma = model.QUEM_AcercaTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarQuemSomos", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarQuemSomos(QuemSomosViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemSomosTexto[i].Banner = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.QuemSomosTexto[i].PDF = fileName;

                }
                i++;

            }




            /*
            db.QUEM_Quem.Attach(model.Quem);
            db.Entry(model.Quem).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (QUEM_AcercaTexto texto in model.QuemSomosTexto)
            {

                db.Database.ExecuteSqlCommand("Update QUEM_AcercaTexto SET  Titulo = '" + texto.Titulo + "', Conteudo = '" + texto.Conteudo + "', "
                  + " Introducao = '" + texto.Introducao + "', Banner='" + texto.Banner + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Quem");

                return PartialView("_EditarCategoriaQuemPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarQuemSomos(int id)
        {

            QUEM_Acerca model = db.QUEM_AcercaTexto.Include("CONF_Idioma").Include("QUEM_Acerca")
                   .Select(x => x.QUEM_Acerca).Where(c => c.Id == id).FirstOrDefault();



            QuemSomosViewModel IntroducaoModel = new QuemSomosViewModel();
            IntroducaoModel.QuemSomos = model;
            IntroducaoModel.QuemSomosTexto = model.QUEM_AcercaTexto.ToList();
            IntroducaoModel.Idioma = model.QUEM_AcercaTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarQuemSomosPartial", IntroducaoModel);

        }

        [HttpPost]
        public ActionResult ApagarQuemSomosConfirmar(QuemSomosViewModel model)
        {
            QUEM_Acerca QuemIntroducao = db.QUEM_AcercaTexto.Include("CONF_Idioma").Include("QUEM_Acerca")
                  .Select(x => x.QUEM_Acerca).Where(c => c.Id == model.QuemSomos.Id).FirstOrDefault();

            foreach (var item in QuemIntroducao.QUEM_AcercaTexto.ToList())
            {
                db.QUEM_AcercaTexto.Remove(item);
            }

            db.SaveChanges();

            db.QUEM_Acerca.Attach(QuemIntroducao);

            db.QUEM_Acerca.Remove(QuemIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult QuemSomosImagem(int? id)
        {

            QUEM_AcercaTexto model = db.QUEM_AcercaTexto.Include("CONF_Idioma")
                .Where(m => m.AcercaId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Banner;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }



    }
}