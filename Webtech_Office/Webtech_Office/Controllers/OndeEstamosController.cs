﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    public class OndeEstamosController : Controller
    {

        private CMS_DbContext db = new CMS_DbContext();

        // GET: OndeEstamos
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListaOndeEstamosIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<CONT_OndeEstamosIntroducao> query = db.CONT_OndeEstamosIntroducao
                .Include("CONT_OndeEstamosIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.CONT_OndeEstamosIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.CONT_OndeEstamosIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.CONT_OndeEstamosIntroducaoTexto.FirstOrDefault().Titulo
                        : m.CONT_OndeEstamosIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.CONT_OndeEstamosIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.CONT_OndeEstamosIntroducaoTexto.FirstOrDefault().Introducao
                        : m.CONT_OndeEstamosIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarOndeEstamosIntroducao()
        {
            OndeEstamosIntroducaoViewModel model = new OndeEstamosIntroducaoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                CONT_OndeEstamosIntroducaoTexto OndeEstamosTexto = new CONT_OndeEstamosIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.OndeEstamosIntroducaoTexto.Add(OndeEstamosTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarOndeEstamosIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarOndeEstamosIntroducao(OndeEstamosIntroducaoViewModel model)
        {


            model.OndeEstamosIntroducao.DataCriacao = DateTime.Now;
            model.OndeEstamosIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.OndeEstamosIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.OndeEstamosIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (CONT_OndeEstamosIntroducaoTexto textos in model.OndeEstamosIntroducaoTexto)
            {

                textos.CONT_OndeEstamosIntroducao = model.OndeEstamosIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.OndeEstamosIntroducao.CONT_OndeEstamosIntroducaoTexto = model.OndeEstamosIntroducaoTexto;
            db.CONT_OndeEstamosIntroducao.Add(model.OndeEstamosIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult DetalhesOndeEstamosIntroducao(int? id)
        {
            CONT_OndeEstamosIntroducao model = db.CONT_OndeEstamosIntroducaoTexto.Include("CONF_Idioma").Include("CONT_OndeEstamosIntroducao")
               .Select(x => x.CONT_OndeEstamosIntroducao).Where(c => c.Id == id).FirstOrDefault();



            OndeEstamosIntroducaoViewModel IntroducaoModel = new OndeEstamosIntroducaoViewModel();
            IntroducaoModel.OndeEstamosIntroducao = model;
            IntroducaoModel.OndeEstamosIntroducaoTexto = model.CONT_OndeEstamosIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.CONT_OndeEstamosIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesOndeEstamosIntroducao", IntroducaoModel);

        }
        [HttpGet]
        public ActionResult EditarOndeEstamosIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONT_OndeEstamosIntroducao model = db.CONT_OndeEstamosIntroducaoTexto.Include("CONF_Idioma").Include("CONT_OndeEstamosIntroducao")
              .Select(x => x.CONT_OndeEstamosIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            OndeEstamosIntroducaoViewModel IntroducaoModel = new OndeEstamosIntroducaoViewModel();
            IntroducaoModel.OndeEstamosIntroducao = model;
            IntroducaoModel.OndeEstamosIntroducaoTexto = model.CONT_OndeEstamosIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.CONT_OndeEstamosIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarOndeEstamosIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarOndeEstamosIntroducao(OndeEstamosIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.OndeEstamosIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.OndeEstamosIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update CONT_OndeEstamosIntroducao SET  Activo = '" + model.OndeEstamosIntroducao.Activo
                  + "'  WHERE ID= " + model.OndeEstamosIntroducao.Id);

            /*
            db.CONT_OndeEstamos.Attach(model.OndeEstamos);
            db.Entry(model.OndeEstamos).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (CONT_OndeEstamosIntroducaoTexto texto in model.OndeEstamosIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update CONT_OndeEstamosIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de OndeEstamos");

                return PartialView("_EditarCategoriaOndeEstamosPartial", model);
            }
            */


            return RedirectToAction("Index");


        }

        [HttpGet]
        public ActionResult ApagarOndeEstamosIntroducao(int id)
        {

            CONT_OndeEstamosIntroducao model = db.CONT_OndeEstamosIntroducaoTexto.Include("CONF_Idioma").Include("CONT_OndeEstamosIntroducao")
                   .Select(x => x.CONT_OndeEstamosIntroducao).Where(c => c.Id == id).FirstOrDefault();



            OndeEstamosIntroducaoViewModel IntroducaoModel = new OndeEstamosIntroducaoViewModel();
            IntroducaoModel.OndeEstamosIntroducao = model;
            IntroducaoModel.OndeEstamosIntroducaoTexto = model.CONT_OndeEstamosIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.CONT_OndeEstamosIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarOndeEstamosIntroducaoPartial", IntroducaoModel);

        }

        [HttpGet]
        public ActionResult OndeEstamosIntroducaoImagem(int? id)
        {

            CONT_OndeEstamosIntroducaoTexto model = db.CONT_OndeEstamosIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.OndeEstamosIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemOndeEstamosIntroducaoPreviewPartial", imgModel);

        }

        [HttpPost]
        public ActionResult ApagarOndeEstamosIntroducaoConfirmar(OndeEstamosIntroducaoViewModel model)
        {
            CONT_OndeEstamosIntroducao OndeEstamosIntroducao = db.CONT_OndeEstamosIntroducaoTexto.Include("CONF_Idioma").Include("CONT_OndeEstamos")
                  .Select(x => x.CONT_OndeEstamosIntroducao).Where(c => c.Id == model.OndeEstamosIntroducao.Id).FirstOrDefault();

            foreach (var item in OndeEstamosIntroducao.CONT_OndeEstamosIntroducaoTexto.ToList())
            {
                db.CONT_OndeEstamosIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.CONT_OndeEstamosIntroducao.Attach(OndeEstamosIntroducao);

            db.CONT_OndeEstamosIntroducao.Remove(OndeEstamosIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        public JsonResult OndeEstamos()
        {


            IQueryable<CONT_OndeEstamos> query = db.CONT_OndeEstamos.AsQueryable();




            var totalCount = query.Count();

            var data =
                query.Select(m => new
                {

                    Morada = m.Morada,
                    Mapa = m.Mapa,
                    Rua = m.Rua


                }).ToList();




            return Json(data, JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult EditarOndeEstamos(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONT_OndeEstamos model = db.CONT_OndeEstamos.Where(c => c.Id == id)
                              .SingleOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            OndeEstamosViewModel OndeEstamosModel = new OndeEstamosViewModel();
            OndeEstamosModel.OndeEstamos = model;



            return PartialView("_EditarOndeEstamosPartial", OndeEstamosModel);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarOndeEstamos(OndeEstamosViewModel model)
        {



            model.OndeEstamos.Rua = "";

            db.Database.ExecuteSqlCommand("Update CONT_OndeEstamos SET  Morada = '" + model.OndeEstamos.Morada + "', Mapa = '" + model.OndeEstamos.Mapa + "', "
                  + " Rua = '" + model.OndeEstamos.Rua + "'   WHERE ID= " + model.OndeEstamos.Id);


            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }



        public JsonResult ListaPontos([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<CONT_PontosAtendimento> query = db.CONT_PontosAtendimento;




            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();


              


            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Morada asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(m => new
                {
                    PontoId = m.Id,
                    Morada = (m.Morada.Length < 100 ? m.Morada : m.Morada.Substring(0, 100)),
                   

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);


        }

        //Tipo de Receitas
        [HttpGet]
        public ActionResult CriarPontoAtentimento()
        {

            CONT_PontosAtendimento pontos = new CONT_PontosAtendimento();

            return PartialView("_CriarPontoAtentimento",pontos);

        }


        [HttpPost]
        public async Task<ActionResult> CriarPontoAtentimento(CONT_PontosAtendimento model)
        {
      
          
            db.CONT_PontosAtendimento.Add(model);
            var task = db.SaveChangesAsync();
            await task;

            if (task.Exception != null)
            {
                ModelState.AddModelError("", "Impossivel criar tipo de receita");

                return View("_CriarTipoReceita", model);
            }

            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }

            return RedirectToAction("Index");





        }

        [HttpGet]
        public ActionResult DetalhesPontoAtentimento(int? id)
        {
            CONT_PontosAtendimento pontos = db.CONT_PontosAtendimento.Where(p => p.Id == id).FirstOrDefault();           

            return PartialView("_DetalhesPontoAtentimentoPartial", pontos);

        }


        [HttpGet]
        public ActionResult ApagarPontoAtentimento(int? id)
        {
            CONT_PontosAtendimento pontos = db.CONT_PontosAtendimento.Where(p => p.Id == id).FirstOrDefault();

            return PartialView("_ApagarPontoAtentimentoPartial", pontos);

        }

        /*Apagar Menu Post*/
        [HttpPost]
        public async Task<ActionResult> ApagarPontoAtentimentoConfirmar(CONT_PontosAtendimento model)
        {
          
            db.CONT_PontosAtendimento.Remove(model);

            var task = db.SaveChangesAsync();
            await task;




            return Content("sucesso");
          

        }

        [HttpGet]
        public ActionResult EditarPontoAtentimento(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONT_PontosAtendimento model = db.CONT_PontosAtendimento.Where(p => p.Id == id).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }


            return PartialView("_EditarPontoAtentimentoPartial", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarPontoAtentimento(CONT_PontosAtendimento model)
        {




            db.CONT_PontosAtendimento.Attach(model);
            db.Entry(model).State = EntityState.Modified;       

        

            db.SaveChanges();


            return RedirectToAction("Index");

        }




    }
}