﻿
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    [Authorize]
    public class MediaController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        public JsonResult ListaMediaIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<MEDI_MediaIntroducao> query = db.MEDI_MediaIntroducao
                .Include("MEDI_MediaIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.MEDI_MediaIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.MEDI_MediaIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.MEDI_MediaIntroducaoTexto.FirstOrDefault().Titulo
                        : m.MEDI_MediaIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.MEDI_MediaIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.MEDI_MediaIntroducaoTexto.FirstOrDefault().Introducao
                        : m.MEDI_MediaIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarMediaIntroducao()
        {
            MediaIntroducaoViewModel model = new MediaIntroducaoViewModel();
            model.MediaIntroducao.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                MEDI_MediaIntroducaoTexto MediaTexto = new MEDI_MediaIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.MediaIntroducaoTexto.Add(MediaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarMediaIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarMediaIntroducao(MediaIntroducaoViewModel model)
        {


            model.MediaIntroducao.DataCriacao = DateTime.Now;
            model.MediaIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (MEDI_MediaIntroducaoTexto textos in model.MediaIntroducaoTexto)
            {

                textos.MEDI_MediaIntroducao = model.MediaIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.MediaIntroducao.MEDI_MediaIntroducaoTexto = model.MediaIntroducaoTexto;
            db.MEDI_MediaIntroducao.Add(model.MediaIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMedia = new SelectList(db.MIDI_TipoMedia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMediaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult DetalhesMediaIntroducao(int? id)
        {
            MEDI_MediaIntroducao model = db.MEDI_MediaIntroducaoTexto.Include("CONF_Idioma").Include("MEDI_MediaIntroducao")
               .Select(x => x.MEDI_MediaIntroducao).Where(c => c.Id == id).FirstOrDefault();



            MediaIntroducaoViewModel IntroducaoModel = new MediaIntroducaoViewModel();
            IntroducaoModel.MediaIntroducao = model;
            IntroducaoModel.MediaIntroducaoTexto = model.MEDI_MediaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.MEDI_MediaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesMediaIntroducao", IntroducaoModel);

        }
        [HttpGet]
        public ActionResult EditarMediaIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MEDI_MediaIntroducao model = db.MEDI_MediaIntroducaoTexto.Include("CONF_Idioma").Include("MEDI_MediaIntroducao")
              .Select(x => x.MEDI_MediaIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            MediaIntroducaoViewModel IntroducaoModel = new MediaIntroducaoViewModel();
            IntroducaoModel.MediaIntroducao = model;
            IntroducaoModel.MediaIntroducaoTexto = model.MEDI_MediaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.MEDI_MediaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarMediaIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarMediaIntroducao(MediaIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update MEDI_MediaIntroducao SET  Activo = '" + model.MediaIntroducao.Activo
                  + "'  WHERE ID= " + model.MediaIntroducao.Id);

            /*
            db.MEDI_Media.Attach(model.Media);
            db.Entry(model.Media).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (MEDI_MediaIntroducaoTexto texto in model.MediaIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update MEDI_MediaIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Media");

                return PartialView("_EditarCategoriaMediaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }

        [HttpGet]
        public ActionResult ApagarMediaIntroducao(int id)
        {

            MEDI_MediaIntroducao model = db.MEDI_MediaIntroducaoTexto.Include("CONF_Idioma").Include("MEDI_MediaIntroducao")
                   .Select(x => x.MEDI_MediaIntroducao).Where(c => c.Id == id).FirstOrDefault();



            MediaIntroducaoViewModel IntroducaoModel = new MediaIntroducaoViewModel();
            IntroducaoModel.MediaIntroducao = model;
            IntroducaoModel.MediaIntroducaoTexto = model.MEDI_MediaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.MEDI_MediaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarMediaIntroducaoPartial", IntroducaoModel);

        }

        [HttpGet]
        public ActionResult MediaIntroducaoImagem(int? id)
        {

            MEDI_MediaIntroducaoTexto model = db.MEDI_MediaIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.MediaIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemMediaPreviewPartial", imgModel);

        }

        [HttpPost]
        public ActionResult ApagarMediaIntroducaoConfirmar(MediaIntroducaoViewModel model)
        {
            MEDI_MediaIntroducao MediaIntroducao = db.MEDI_MediaIntroducaoTexto.Include("CONF_Idioma").Include("MEDI_Media")
                  .Select(x => x.MEDI_MediaIntroducao).Where(c => c.Id == model.MediaIntroducao.Id).FirstOrDefault();

            foreach (var item in MediaIntroducao.MEDI_MediaIntroducaoTexto.ToList())
            {
                db.MEDI_MediaIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.MEDI_MediaIntroducao.Attach(MediaIntroducao);

            db.MEDI_MediaIntroducao.Remove(MediaIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        // GET: Media
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListaMediaConteudo([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            /*
            IQueryable<MEDI_Media> query = db.MEDI_MediaTexto
                .Where(m => m.CONF_Idioma.Codigo.ToUpper() == ("PT"))
                .Select(m => m.MEDI_Media);
                */


            IQueryable<MEDI_Media> query = db.MEDI_Media.Include("MediaTexto").Include("CONF_Idioma")
                .Where(m => m.MEDI_MediaTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();


                query = query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                         .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(m => new
                {
                    MediaID = m.Id,
                    Media = ( m.MEDI_TipoMedia.MEDI_TipoMediaTexto.FirstOrDefault().Descricao.Length <  70) ?
                           m.MEDI_TipoMedia.MEDI_TipoMediaTexto.FirstOrDefault().Descricao : m.MEDI_TipoMedia.MEDI_TipoMediaTexto.FirstOrDefault().Descricao.Substring(0,66) + "...",
                    Descricao = (m.MEDI_MediaTexto.FirstOrDefault().Descricao.Length < 70) ?
                           m.MEDI_MediaTexto.FirstOrDefault().Descricao : m.MEDI_MediaTexto.FirstOrDefault().Descricao.Substring(0, 66) + "...",

                    Data = SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", m.Data).Trim().Length) + SqlFunctions.DateName("dd", m.Data).Trim() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)m.Data.Month).TrimStart().Length) + SqlFunctions.StringConvert((double)m.Data.Month).TrimStart() + "/" + SqlFunctions.DateName("year", m.Data),
                    Activo = m.Activo,
                    Imagem = m.Id,
                   

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarMediaConteudo(int? id)
        {
            MediaViewModel midiaModel = new MediaViewModel();
            midiaModel.Media.Activo = true;
            midiaModel.Media.Data = DateTime.Now;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                MEDI_MediaTexto midiaTexto = new MEDI_MediaTexto()
                {
                    CONF_Idioma = idioma
                };
                midiaModel.MediaTexto.Add(midiaTexto);
                ImageUpload file = new ImageUpload();
                midiaModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                midiaModel.FileDocumento.Add(doc);

            }



            var query = from tr in db.MEDI_TipoMedia
                        join trt in db.MEDI_TipoMediaTexto on tr.Id equals trt.TipoMediaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = trt.TipoMediaId, Descricao = trt.Descricao };


            ViewBag.TipoMedia = new SelectList(query, "Id", "Descricao");


            return View("CriarMediaConteudo", midiaModel);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarMediaConteudo(MediaViewModel model)
        {




            int i = 0;
            
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaTexto[i].PDF = fileName;

                }
                i++;

            }
            i = 0;

            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;

            model.Media.DataCriacao = DateTime.Now;
            model.Media.DataModificacao = DateTime.Now;

            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (MEDI_MediaTexto textos in model.MediaTexto)
            {
            
                textos.MEDI_Media = model.Media;
                textos.CONF_Idioma = idioma[i];
                textos.Conteudo = System.Net.WebUtility.HtmlDecode(textos.Conteudo);

                i++;

            }



        
            
            model.Media.MEDI_MediaTexto = model.MediaTexto;
            db.MEDI_Media.Add(model.Media);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMedia = new SelectList(db.MIDI_TipoMedia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMediaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }

        // GET: MEDI_Media/Edit/5
        [HttpGet]
        public ActionResult EditarMediaConteudo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }          
            MEDI_Media model = db.MEDI_Media.Include(m => m.MEDI_MediaTexto).Include(m => m.MEDI_TipoMedia)
                .Include(m => m.MEDI_MediaTexto.Select(mt => mt.CONF_Idioma))
                .FirstOrDefault(x => x.Id == id);
           
            if (model == null)
            {
                return HttpNotFound();
            }
            MediaViewModel media = new MediaViewModel();
            media.Media = model;
            media.MediaTexto = model.MEDI_MediaTexto.ToList();
            
            media.TipoMedia = model.MEDI_TipoMedia;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {               
                
                ImageUpload file = new ImageUpload();
                media.File.Add(file);
                FileDocumento doc = new FileDocumento();
                media.FileDocumento.Add(doc);

            }

         




            var query = from tr in db.MEDI_TipoMedia
                        join trt in db.MEDI_TipoMediaTexto on tr.Id equals trt.TipoMediaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = trt.TipoMediaId, Descricao = trt.Descricao };

            ViewBag.TipoMedia = new SelectList(query, "Id", "Descricao");

            return View("EditarMediaConteudo", media);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditarMediaConteudo(MediaViewModel model)
        {

           
            model.Media.DataModificacao = DateTime.Now;
           //model.Media.Data =
           //     DateTime.ParseExact(model.Data, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.MediaTexto[i].Imagem = fileName;

                }
            }

            i = 0;
            db.Database.ExecuteSqlCommand("Update MEDI_Media SET  Activo = '" + model.Media.Activo + "', TipoMidia = " + model.TipoMedia.Id 
                + " , Data = '"+ model.Media.Data +"'  WHERE ID= " + model.Media.Id);

           
            db.SaveChanges();

       

            foreach (MEDI_MediaTexto texto in model.MediaTexto)
            {


                db.Database.ExecuteSqlCommand("Update MEDI_MediaTexto SET  Descricao = '" + texto.Descricao + "', Conteudo = '" + texto.Conteudo +"', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.id);
            }

            db.SaveChanges();
            return RedirectToAction("Index");        
           
        }

        //Detalhes do Menu
        public ActionResult DetalhesMediaConteudo(int id)
        {
            MEDI_Media model = db.MEDI_Media.Include(m => m.MEDI_MediaTexto).Include(m => m.MEDI_TipoMedia)
                .FirstOrDefault(x => x.Id == id);

            MediaViewModel midia = new MediaViewModel();
            midia.Media = model;
            midia.MediaTexto = model.MEDI_MediaTexto.ToList();
            midia.TipoMedia = model.MEDI_TipoMedia;




            var query = from tr in db.MEDI_TipoMedia
                        join trt in db.MEDI_TipoMediaTexto on tr.Id equals trt.TipoMediaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = trt.TipoMediaId, Descricao = trt.Descricao };


            ViewBag.TipoMedia = new SelectList(query, "Id", "Descricao");


            return PartialView("_DetalhesMediaConteudoPartial", midia);
        }

   
        public ActionResult ApagarMediaConteudo(int id)
        {

            MEDI_Media model = db.MEDI_Media.Include(m => m.MEDI_MediaTexto).Include(m => m.MEDI_TipoMedia)
                    .FirstOrDefault(x => x.Id == id);

            MediaViewModel midia = new MediaViewModel
            {
                Media = model,
                MediaTexto = model.MEDI_MediaTexto.ToList()
            };
            midia.TipoMedia = model.MEDI_TipoMedia;



            var query = from tr in db.MEDI_TipoMedia
                        join trt in db.MEDI_TipoMediaTexto on tr.Id equals trt.TipoMediaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = trt.TipoMediaId, Descricao = trt.Descricao };


            ViewBag.TipoMedia = new SelectList(query, "Id", "Descricao");


            return PartialView("ApagarMediaConteudo", midia);
        }
      
        [HttpPost]
        public ActionResult ApagarMediaConteudoConfirmar(MediaViewModel model)
        {
            MEDI_Media midia = db.MEDI_Media.Include(m => m.MEDI_MediaTexto).Include(m => m.MEDI_TipoMedia)
                .FirstOrDefault(x => x.Id == model.Media.Id);

            foreach (var item in midia.MEDI_MediaTexto.ToList())
            {
                db.MEDI_MediaTexto.Remove(item);
            }

            db.SaveChanges();

            db.MEDI_Media.Attach(midia);

            db.MEDI_Media.Remove(midia);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetVideo(int? id)
        {

            MEDI_MediaTexto model = db.MEDI_MediaTexto.Include(m => m.CONF_Idioma)
                .Where(m => m.MidiaId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")               
                .SingleOrDefault();

          

            LinkVideoModel videoModel = new LinkVideoModel();

            return PartialView("_VideoPreviewPartial", videoModel);

        }

        [HttpGet]
        public ActionResult GetPdf(int? id)
        {

            MEDI_MediaTexto model = db.MEDI_MediaTexto.Include(m => m.CONF_Idioma)
                .Where(m => m.MidiaId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .FirstOrDefault();



            PdfViewModel pdfModel = new PdfViewModel();
            pdfModel.Pdf = model.PDF;

            return PartialView("_PdfPreviewPartial", pdfModel);

        }

        [HttpGet]
        public ActionResult MediaImagem(int id)
        {

            MEDI_MediaTexto model = db.MEDI_MediaTexto.Include("CONF_Idioma")
                .Where(m => m.MidiaId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemMediaPreviewPartial", imgModel);

        }



    }
}