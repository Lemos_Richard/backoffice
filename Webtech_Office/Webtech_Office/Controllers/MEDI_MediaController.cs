﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
    [Authorize]
    public class MEDI_MediaController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        // GET: MEDI_Media
        public ActionResult Index()
        {
            var mEDI_Media = db.MEDI_Media.Include(m => m.MEDI_TipoMedia);
            return View(mEDI_Media.ToList());
        }

        // GET: MEDI_Media/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MEDI_Media mEDI_Media = db.MEDI_Media.Find(id);
            if (mEDI_Media == null)
            {
                return HttpNotFound();
            }
            return View(mEDI_Media);
        }

        // GET: MEDI_Media/Create
        public ActionResult Create()
        {
            ViewBag.MenuId = new SelectList(db.Menu, "Id", "TipoDeEstrutura");
            ViewBag.TipoMidia = new SelectList(db.MEDI_TipoMedia, "Id", "Descricao");
            return View();
        }

        // POST: MEDI_Media/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MenuId,TipoMidia,Destaque,PartilhaFacebook,PartilhaTwitter,Activo,DataCriacao,DataModificacao,Modificador")] MEDI_Media mEDI_Media)
        {
            if (ModelState.IsValid)
            {
                db.MEDI_Media.Add(mEDI_Media);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.MenuId = new SelectList(db.Menu, "Id", "TipoDeEstrutura", mEDI_Media.MenuId);
            ViewBag.TipoMidia = new SelectList(db.MEDI_TipoMedia, "Id", "Descricao", mEDI_Media.TipoMidia);
            return View(mEDI_Media);
        }

        // GET: MEDI_Media/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MEDI_Media mEDI_Media = db.MEDI_Media.Find(id);
            if (mEDI_Media == null)
            {
                return HttpNotFound();
            }
            //ViewBag.MenuId = new SelectList(db.Menu, "Id", "TipoDeEstrutura", mEDI_Media.MenuId);
            ViewBag.TipoMidia = new SelectList(db.MEDI_TipoMedia, "Id", "Descricao", mEDI_Media.TipoMidia);
            return View(mEDI_Media);
        }

        // POST: MEDI_Media/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MenuId,TipoMidia,Destaque,PartilhaFacebook,PartilhaTwitter,Activo,DataCriacao,DataModificacao,Modificador")] MEDI_Media mEDI_Media)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mEDI_Media).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.MenuId = new SelectList(db.Menu, "Id", "TipoDeEstrutura", mEDI_Media.MenuId);
            ViewBag.TipoMidia = new SelectList(db.MEDI_TipoMedia, "Id", "Descricao", mEDI_Media.TipoMidia);
            return View(mEDI_Media);
        }

        // GET: MEDI_Media/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MEDI_Media mEDI_Media = db.MEDI_Media.Find(id);
            if (mEDI_Media == null)
            {
                return HttpNotFound();
            }
            return View(mEDI_Media);
        }

        // POST: MEDI_Media/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MEDI_Media mEDI_Media = db.MEDI_Media.Find(id);
            db.MEDI_Media.Remove(mEDI_Media);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
