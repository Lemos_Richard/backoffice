﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webtech_Office.Models;

namespace Webtech_Office.Controllers
{
   [Authorize]
    public class ContactoController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        // GET: Contacto
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<CONT_ContactoBanner> query = db.CONT_ContactoBanner
                .Include("CONT_ContactoBannerTexto")

                .Include("CONF_Idioma")
                .Where(m => m.CONT_ContactoBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.CONT_ContactoBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.CONT_ContactoBannerTexto.FirstOrDefault().Titulo
                        : m.CONT_ContactoBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.CONT_ContactoBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.CONT_ContactoBannerTexto.FirstOrDefault().Introducao
                        : m.CONT_ContactoBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.CONT_ContactoBannerTexto.FirstOrDefault().Link,
                    PDF = m.CONT_ContactoBannerTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarContactoBanner()
        {
            ContactoBannerViewModel model = new ContactoBannerViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                CONT_ContactoBannerTexto ContactoTexto = new CONT_ContactoBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ContactoBannerTexto.Add(ContactoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarContactoBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarContactoBanner(ContactoBannerViewModel model)
        {


            model.ContactoBanner.DataCriacao = DateTime.Now;
            model.ContactoBanner.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (CONT_ContactoBannerTexto textos in model.ContactoBannerTexto)
            {

                textos.CONT_ContactoBanner = model.ContactoBanner;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.ContactoBanner.CONT_ContactoBannerTexto = model.ContactoBannerTexto;
            db.CONT_ContactoBanner.Add(model.ContactoBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesContactoBanner(int? id)
        {
            CONT_ContactoBanner model = db.CONT_ContactoBannerTexto.Include("CONF_Idioma").Include("CONT_ContactoBanner")
               .Select(x => x.CONT_ContactoBanner).Where(c => c.Id == id).FirstOrDefault();



            ContactoBannerViewModel BannerModel = new ContactoBannerViewModel();
            BannerModel.ContactoBanner = model;
            BannerModel.ContactoBannerTexto = model.CONT_ContactoBannerTexto.ToList();
            BannerModel.Idioma = model.CONT_ContactoBannerTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesContactoBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarContactoBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONT_ContactoBanner model = db.CONT_ContactoBannerTexto.Include("CONF_Idioma").Include("CONT_ContactoBanner")
              .Select(x => x.CONT_ContactoBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ContactoBannerViewModel BannerModel = new ContactoBannerViewModel();
            BannerModel.ContactoBanner = model;
            BannerModel.ContactoBannerTexto = model.CONT_ContactoBannerTexto.ToList();
            BannerModel.Idioma = model.CONT_ContactoBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarContactoBanner", BannerModel);


        }


        [HttpPost]

        public ActionResult EditarContactoBanner(ContactoBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoBannerTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update CONT_ContactoBanner SET  Activo = '" + model.ContactoBanner.Activo
                  + "'  WHERE ID= " + model.ContactoBanner.Id);

            /*
            db.CONT_Contacto.Attach(model.Contacto);
            db.Entry(model.Contacto).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (CONT_ContactoBannerTexto texto in model.ContactoBannerTexto)
            {


                db.Database.ExecuteSqlCommand("Update CONT_ContactoBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Contacto");

                return PartialView("_EditarCategoriaContactoPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarContactoBanner(int? id)
        {

            CONT_ContactoBanner model = db.CONT_ContactoBannerTexto.Include("CONF_Idioma").Include("CONT_ContactoBanner")
                   .Select(x => x.CONT_ContactoBanner).Where(c => c.Id == id).FirstOrDefault();



            ContactoBannerViewModel BannerModel = new ContactoBannerViewModel();
            BannerModel.ContactoBanner = model;
            BannerModel.ContactoBannerTexto = model.CONT_ContactoBannerTexto.ToList();
            BannerModel.Idioma = model.CONT_ContactoBannerTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarContactoBannerPartial", BannerModel);

        }

        [HttpGet]
        public ActionResult ContactoBannerImagem(int? id)
        {

            CONT_ContactoBannerTexto model = db.CONT_ContactoBannerTexto.Include("CONF_Idioma")
                .Where(m => m.ContactoBannerId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemContactoBannerPreviewPartial", imgModel);

        }


        [HttpPost]
        public ActionResult ApagarContactoBannerConfirmar(ContactoBannerViewModel model)
        {
            CONT_ContactoBanner ContactoBanner = db.CONT_ContactoBannerTexto.Include("CONF_Idioma").Include("CONT_Contacto")
                  .Select(x => x.CONT_ContactoBanner).Where(c => c.Id == model.ContactoBanner.Id).FirstOrDefault();

            foreach (var item in ContactoBanner.CONT_ContactoBannerTexto.ToList())
            {
                db.CONT_ContactoBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.CONT_ContactoBanner.Attach(ContactoBanner);

            db.CONT_ContactoBanner.Remove(ContactoBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        public JsonResult ListaContactoIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<CONT_ContactoIntroducao> query = db.CONT_ContactoIntroducao
                .Include("CONT_ContactoIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.CONT_ContactoIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.CONT_ContactoIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.CONT_ContactoIntroducaoTexto.FirstOrDefault().Titulo
                        : m.CONT_ContactoIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.CONT_ContactoIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.CONT_ContactoIntroducaoTexto.FirstOrDefault().Introducao
                        : m.CONT_ContactoIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarContactoIntroducao()
        {
            ContactoIntroducaoViewModel model = new ContactoIntroducaoViewModel();
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                CONT_ContactoIntroducaoTexto ContactoTexto = new CONT_ContactoIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ContactoIntroducaoTexto.Add(ContactoTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarContactoIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarContactoIntroducao(ContactoIntroducaoViewModel model)
        {


            model.ContactoIntroducao.DataCriacao = DateTime.Now;
            model.ContactoIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (CONT_ContactoIntroducaoTexto textos in model.ContactoIntroducaoTexto)
            {

                textos.CONT_ContactoIntroducao = model.ContactoIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.ContactoIntroducao.CONT_ContactoIntroducaoTexto = model.ContactoIntroducaoTexto;
            db.CONT_ContactoIntroducao.Add(model.ContactoIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesContactoIntroducao(int? id)
        {
            CONT_ContactoIntroducao model = db.CONT_ContactoIntroducaoTexto.Include("CONF_Idioma").Include("CONT_ContactoIntroducao")
               .Select(x => x.CONT_ContactoIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ContactoIntroducaoViewModel IntroducaoModel = new ContactoIntroducaoViewModel();
            IntroducaoModel.ContactoIntroducao = model;
            IntroducaoModel.ContactoIntroducaoTexto = model.CONT_ContactoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.CONT_ContactoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesContactoIntroducao", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarContactoIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONT_ContactoIntroducao model = db.CONT_ContactoIntroducaoTexto.Include("CONF_Idioma").Include("CONT_ContactoIntroducao")
              .Select(x => x.CONT_ContactoIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ContactoIntroducaoViewModel IntroducaoModel = new ContactoIntroducaoViewModel();
            IntroducaoModel.ContactoIntroducao = model;
            IntroducaoModel.ContactoIntroducaoTexto = model.CONT_ContactoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.CONT_ContactoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarContactoIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarContactoIntroducao(ContactoIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ContactoIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update CONT_ContactoIntroducao SET  Activo = '" + model.ContactoIntroducao.Activo
                  + "'  WHERE ID= " + model.ContactoIntroducao.Id);

            /*
            db.CONT_Contacto.Attach(model.Contacto);
            db.Entry(model.Contacto).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (CONT_ContactoIntroducaoTexto texto in model.ContactoIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update CONT_ContactoIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Contacto");

                return PartialView("_EditarCategoriaContactoPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarContactoIntroducao(int id)
        {

            CONT_ContactoIntroducao model = db.CONT_ContactoIntroducaoTexto.Include("CONF_Idioma").Include("CONT_ContactoIntroducao")
                   .Select(x => x.CONT_ContactoIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ContactoIntroducaoViewModel IntroducaoModel = new ContactoIntroducaoViewModel();
            IntroducaoModel.ContactoIntroducao = model;
            IntroducaoModel.ContactoIntroducaoTexto = model.CONT_ContactoIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.CONT_ContactoIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarContactoIntroducaoPartial", IntroducaoModel);

        }



        [HttpGet]
        public ActionResult ContactoIntroducaoImagem(int? id)
        {

            CONT_ContactoIntroducaoTexto model = db.CONT_ContactoIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.ContactoIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemContactoIntroducaoPreviewPartial", imgModel);

        }



        [HttpPost]
        public ActionResult ApagarContactoIntroducaoConfirmar(ContactoIntroducaoViewModel model)
        {
            CONT_ContactoIntroducao ContactoIntroducao = db.CONT_ContactoIntroducaoTexto.Include("CONF_Idioma").Include("CONT_Contacto")
                  .Select(x => x.CONT_ContactoIntroducao).Where(c => c.Id == model.ContactoIntroducao.Id).FirstOrDefault();

            foreach (var item in ContactoIntroducao.CONT_ContactoIntroducaoTexto.ToList())
            {
                db.CONT_ContactoIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.CONT_ContactoIntroducao.Attach(ContactoIntroducao);

            db.CONT_ContactoIntroducao.Remove(ContactoIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        

        public JsonResult ListaContacto([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {

            IQueryable<CONT_Contacto> query = db.CONT_Contacto

                            .Include("CONT_ContactoTexto")
                            .Include("CONT_TipoContacto")
                            .Include("CONT_TipoContactoTexto")
                            .Include("CONF_Idioma")
                            .Where(m => m.CONT_ContactoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Tipo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);




           

            var data =
                query.Select(m => new
                {

                    Ordem = m.Ordem,
                    Tipo = (m.CONT_TipoContacto.CONT_TipoContactoTexto.FirstOrDefault().Descricao.Length < 70
                                ? m.CONT_TipoContacto.CONT_TipoContactoTexto.FirstOrDefault().Descricao
                                : m.CONT_TipoContacto.CONT_TipoContactoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),

                    Descricao = (m.CONT_ContactoTexto.FirstOrDefault().Descricao.Length < 70
                                ? m.CONT_ContactoTexto.FirstOrDefault().Descricao
                                : m.CONT_ContactoTexto.FirstOrDefault().Descricao.Substring(0, 70) + "..."),


                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarContacto()
        {

            ContactoViewModel model = new ContactoViewModel();

            List<CONT_ContactoTexto> lista = new List<CONT_ContactoTexto>();

            CONT_ContactoTexto texto = new CONT_ContactoTexto();
            texto.CONF_Idioma = db.CONF_Idioma.Where(i => i.Codigo.ToUpper() == "PT").FirstOrDefault();
            lista.Add(texto);

            model.ContactoTexto = lista;


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");           

            return View("CriarContacto", model);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarContacto(ContactoViewModel model)
        {


            model.Contacto.DataCriacao = DateTime.Now;
            model.Contacto.DataModificacao = DateTime.Now;

            int i = 0;

            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (CONT_ContactoTexto textos in model.ContactoTexto)
            {

                textos.CONT_Contacto = model.Contacto;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.Contacto.CONT_ContactoTexto = model.ContactoTexto;
            db.CONT_Contacto.Add(model.Contacto);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesContacto(int? id)
        {
            CONT_Contacto model = db.CONT_Contacto
               .Include("CONT_ContactoTexto")
               .Where(c => c.Id == id).FirstOrDefault();




            ContactoViewModel ContactoModel = new ContactoViewModel();
            ContactoModel.Contacto = model;
            ContactoModel.ContactoTexto = model.CONT_ContactoTexto.ToList();
            //ContactoModel.Idioma = model._ContactoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
            //ViewBag.s = new SelectList(db.ToList(), "ID", "Nome");


            return PartialView("_DetalhesContactoPartial", ContactoModel);

        }



        [HttpGet]
        public ActionResult EditarContacto(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONT_Contacto model = db.CONT_ContactoTexto.Include("CONF_Idioma").Include("CONT_Contacto")
              .Select(x => x.CONT_Contacto).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ContactoViewModel ContactoModel = new ContactoViewModel();
            ContactoModel.Contacto = model;
            ContactoModel.ContactoTexto = model.CONT_ContactoTexto.ToList();
            ContactoModel.Idioma = model.CONT_ContactoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
           



            return View("EditarContacto", ContactoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarContacto(ContactoViewModel model)
        {


            //int i = 0;




            db.Database.ExecuteSqlCommand("Update CONT_Contacto SET TipoContactoId = " + model.Contacto.TipoContactoId + ",  "
                + " HorarioInicio = '" + model.Contacto.HorarioInicio + "', HorarioFim = '" + model.Contacto.HorarioFim + "',  Activo = '" + model.Contacto.Activo + "', Ordem = " + model.Contacto.Ordem + " "
                + "  WHERE ID= " + model.Contacto.Id);

            /*
            db.CONT_.Attach(model.);
            db.Entry(model.).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            //i = 0;

            foreach (CONT_ContactoTexto texto in model.ContactoTexto)
            {

                db.Database.ExecuteSqlCommand("Update CONT_ContactoTexto SET  Descricao = '" + texto.Descricao + "'  " + " WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de ");

                return PartialView("_EditarCategoriaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }



        [HttpGet]
        public ActionResult ApagarContacto(int id)
        {

            CONT_Contacto model = db.CONT_Contacto
             .Include("CONT_ContactoTexto")
             .Where(c => c.Id == id).FirstOrDefault();




            ContactoViewModel ContactoModel = new ContactoViewModel();
            ContactoModel.Contacto = model;
            ContactoModel.ContactoTexto = model.CONT_ContactoTexto.ToList();
            //ContactoModel.Idioma = model._ContactoTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.CONT_TipoContacto
                        join trt in db.CONT_TipoContactoTexto on tr.Id equals trt.TipoContactoId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };


            ViewBag.TipoContacto = new SelectList(query, "Id", "Descricao");
           

            return PartialView("_ApagarContactoPartial", ContactoModel);


        }


        [HttpPost]
        public ActionResult ApagarContactoConfirmar(ContactoViewModel model)
        {
            CONT_Contacto Contacto = db.CONT_ContactoTexto.Include("CONF_Idioma").Include("CONT_ContactoTexto")
                  .Select(x => x.CONT_Contacto).Where(c => c.Id == model.Contacto.Id).FirstOrDefault();

            foreach (var item in Contacto.CONT_ContactoTexto.ToList())
            {
                db.CONT_ContactoTexto.Remove(item);
            }

            db.SaveChanges();

            db.CONT_Contacto.Attach(Contacto);

            db.CONT_Contacto.Remove(Contacto);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


    }
}