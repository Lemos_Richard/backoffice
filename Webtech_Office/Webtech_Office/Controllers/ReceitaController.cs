﻿using Webtech_Office.Models;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace Webtech_Office.Controllers
{
    [Authorize]
    public class ReceitaController : Controller
    {
        private CMS_DbContext db = new CMS_DbContext();

        // GET: Receita
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListaBanner([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<RECE_ReceitaBanner> query = db.RECE_ReceitaBanner
                .Include("RECE_ReceitaBannerTexto")

                .Include("CONF_Idioma")
                .Where(m => m.RECE_ReceitaBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.RECE_ReceitaBannerTexto.FirstOrDefault().Titulo.Length <= 70 ? m.RECE_ReceitaBannerTexto.FirstOrDefault().Titulo
                        : m.RECE_ReceitaBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.RECE_ReceitaBannerTexto.FirstOrDefault().Introducao.Length <= 70 ? m.RECE_ReceitaBannerTexto.FirstOrDefault().Introducao
                        : m.RECE_ReceitaBannerTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,
                    Link = m.RECE_ReceitaBannerTexto.FirstOrDefault().Link,
                    PDF = m.RECE_ReceitaBannerTexto.FirstOrDefault().PDF,
                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }

        [HttpGet]
        public ActionResult CriarReceitaBanner()
        {
            ReceitaBannerViewModel model = new ReceitaBannerViewModel();
            model.ReceitaBanner.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RECE_ReceitaBannerTexto ReceitaTexto = new RECE_ReceitaBannerTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ReceitaBannerTexto.Add(ReceitaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }




            return View("CriarReceitaBanner", model);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarReceitaBanner(ReceitaBannerViewModel model)
        {


            model.ReceitaBanner.DataCriacao = DateTime.Now;
            model.ReceitaBanner.DataModificacao = DateTime.Now;



            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString()  + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaBannerTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (RECE_ReceitaBannerTexto textos in model.ReceitaBannerTexto)
            {

                textos.RECE_ReceitaBanner = model.ReceitaBanner;
                textos.CONF_Idioma = idioma[i];
              

                i++;

            }



            model.ReceitaBanner.RECE_ReceitaBannerTexto = model.ReceitaBannerTexto;
            db.RECE_ReceitaBanner.Add(model.ReceitaBanner);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        [HttpGet]
        public ActionResult DetalhesReceitaBanner(int? id)
        {
            RECE_ReceitaBanner model = db.RECE_ReceitaBannerTexto.Include("CONF_Idioma").Include("RECE_ReceitaBanner")
               .Select(x => x.RECE_ReceitaBanner).Where(c => c.Id == id).FirstOrDefault();



            ReceitaBannerViewModel BannerModel = new ReceitaBannerViewModel();
            BannerModel.ReceitaBanner = model;
            BannerModel.ReceitaBannerTexto = model.RECE_ReceitaBannerTexto.ToList();
            BannerModel.Idioma = model.RECE_ReceitaBannerTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesReceitaBanner", BannerModel);

        }


        [HttpGet]
        public ActionResult EditarReceitaBanner(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RECE_ReceitaBanner model = db.RECE_ReceitaBannerTexto.Include("CONF_Idioma").Include("RECE_ReceitaBanner")
              .Select(x => x.RECE_ReceitaBanner).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ReceitaBannerViewModel BannerModel = new ReceitaBannerViewModel();
            BannerModel.ReceitaBanner = model;
            BannerModel.ReceitaBannerTexto = model.RECE_ReceitaBannerTexto.ToList();
            BannerModel.Idioma = model.RECE_ReceitaBannerTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                BannerModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                BannerModel.FileDocumento.Add(doc);

            }


            return View("EditarReceitaBanner", BannerModel);


        }


        [HttpPost]
        public ActionResult EditarReceitaBanner(ReceitaBannerViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaBannerTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaBannerTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update RECE_ReceitaBanner SET  Activo = '" + model.ReceitaBanner.Activo
                  + "'  WHERE ID= " + model.ReceitaBanner.Id);

            /*
            db.RECE_Receita.Attach(model.Receita);
            db.Entry(model.Receita).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */
            db.SaveChanges();

            i = 0;

            foreach (RECE_ReceitaBannerTexto texto in model.ReceitaBannerTexto)
            {


                db.Database.ExecuteSqlCommand("Update RECE_ReceitaBannerTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Receita");

                return PartialView("_EditarCategoriaReceitaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarReceitaBanner(int? id)
        {

            RECE_ReceitaBanner model = db.RECE_ReceitaBannerTexto.Include("CONF_Idioma").Include("RECE_ReceitaBanner")
                   .Select(x => x.RECE_ReceitaBanner).Where(c => c.Id == id).FirstOrDefault();



            ReceitaBannerViewModel BannerModel = new ReceitaBannerViewModel();
            BannerModel.ReceitaBanner = model;
            BannerModel.ReceitaBannerTexto = model.RECE_ReceitaBannerTexto.ToList();
            BannerModel.Idioma = model.RECE_ReceitaBannerTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarReceitaBannerPartial", BannerModel);

        }


        [HttpPost]
        public ActionResult ApagarReceitaBannerConfirmar(ReceitaBannerViewModel model)
        {
            RECE_ReceitaBanner ReceitaBanner = db.RECE_ReceitaBannerTexto.Include("CONF_Idioma").Include("RECE_Receita")
                  .Select(x => x.RECE_ReceitaBanner).Where(c => c.Id == model.ReceitaBanner.Id).FirstOrDefault();

            foreach (var item in ReceitaBanner.RECE_ReceitaBannerTexto.ToList())
            {
                db.RECE_ReceitaBannerTexto.Remove(item);
            }

            db.SaveChanges();

            db.RECE_ReceitaBanner.Attach(ReceitaBanner);

            db.RECE_ReceitaBanner.Remove(ReceitaBanner);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ReceitaBannerImagem(int? id)
        {

            RECE_ReceitaBannerTexto model = db.RECE_ReceitaBannerTexto.Include("CONF_Idioma")
                .Include(m => m.RECE_ReceitaBanner)
                .Where(m => m.RECE_ReceitaBanner.Id == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }

        public JsonResult ListaReceitaIntroducao([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {


            IQueryable<RECE_ReceitaIntroducao> query = db.RECE_ReceitaIntroducao
                .Include("RECE_ReceitaIntroducaoTexto")

                .Include("CONF_Idioma")
                .Where(m => m.RECE_ReceitaIntroducaoTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();

                /*
                  query = 

                      query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                           .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                           */

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);



            var data =
                query.Select(m => new
                {

                    Titulo = (m.RECE_ReceitaIntroducaoTexto.FirstOrDefault().Titulo.Length <= 70 ? m.RECE_ReceitaIntroducaoTexto.FirstOrDefault().Titulo
                        : m.RECE_ReceitaIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),
                    Introducao = (m.RECE_ReceitaIntroducaoTexto.FirstOrDefault().Introducao.Length <= 70 ? m.RECE_ReceitaIntroducaoTexto.FirstOrDefault().Introducao
                        : m.RECE_ReceitaIntroducaoTexto.FirstOrDefault().Titulo.Substring(0, 70) + "..."),


                    Imagem = m.Id,

                    Activo = m.Activo,
                    Opcoes = m.Id

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        [HttpGet]
        public ActionResult CriarReceitaIntroducao()
        {
            ReceitaIntroducaoViewModel model = new ReceitaIntroducaoViewModel();
            model.ReceitaIntroducao.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RECE_ReceitaIntroducaoTexto ReceitaTexto = new RECE_ReceitaIntroducaoTexto()
                {
                    CONF_Idioma = idioma
                };
                model.ReceitaIntroducaoTexto.Add(ReceitaTexto);
                ImageUpload file = new ImageUpload();
                model.File.Add(file);
                FileDocumento doc = new FileDocumento();
                model.FileDocumento.Add(doc);

            }

            return View("CriarReceitaIntroducao", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarReceitaIntroducao(ReceitaIntroducaoViewModel model)
        {


            model.ReceitaIntroducao.DataCriacao = DateTime.Now;
            model.ReceitaIntroducao.DataModificacao = DateTime.Now;

            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }




            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (RECE_ReceitaIntroducaoTexto textos in model.ReceitaIntroducaoTexto)
            {

                textos.RECE_ReceitaIntroducao = model.ReceitaIntroducao;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.ReceitaIntroducao.RECE_ReceitaIntroducaoTexto = model.ReceitaIntroducaoTexto;
            db.RECE_ReceitaIntroducao.Add(model.ReceitaIntroducao);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }





        [HttpGet]
        public ActionResult DetalhesReceitaIntroducao(int? id)
        {
            RECE_ReceitaIntroducao model = db.RECE_ReceitaIntroducaoTexto.Include("CONF_Idioma").Include("RECE_ReceitaIntroducao")
               .Select(x => x.RECE_ReceitaIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ReceitaIntroducaoViewModel IntroducaoModel = new ReceitaIntroducaoViewModel();
            IntroducaoModel.ReceitaIntroducao = model;
            IntroducaoModel.ReceitaIntroducaoTexto = model.RECE_ReceitaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.RECE_ReceitaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesReceitaIntroducao", IntroducaoModel);

        }


        [HttpGet]
        public ActionResult EditarReceitaIntroducao(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RECE_ReceitaIntroducao model = db.RECE_ReceitaIntroducaoTexto.Include("CONF_Idioma").Include("RECE_ReceitaIntroducao")
              .Select(x => x.RECE_ReceitaIntroducao).Where(c => c.Id == id).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }
            ReceitaIntroducaoViewModel IntroducaoModel = new ReceitaIntroducaoViewModel();
            IntroducaoModel.ReceitaIntroducao = model;
            IntroducaoModel.ReceitaIntroducaoTexto = model.RECE_ReceitaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.RECE_ReceitaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();


            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {


                ImageUpload file = new ImageUpload();
                IntroducaoModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                IntroducaoModel.FileDocumento.Add(doc);

            }


            return View("EditarReceitaIntroducao", IntroducaoModel);


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarReceitaIntroducao(ReceitaIntroducaoViewModel model)
        {


            int i = 0;
            foreach (ImageUpload file in model.File)
            {
                if (file.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.File.FileName.ToLower());
                    file.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaIntroducaoTexto[i].Imagem = fileName;

                }
                i++;

            }
            i = 0;
            foreach (FileDocumento docFile in model.FileDocumento)
            {

                if (docFile.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }



                    var fileName = Path.GetFileName(docFile.File.FileName);
                    docFile.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.ReceitaIntroducaoTexto[i].PDF = fileName;

                }
                i++;

            }



            db.Database.ExecuteSqlCommand("Update RECE_ReceitaIntroducao SET  Activo = '" + model.ReceitaIntroducao.Activo
                  + "'  WHERE ID= " + model.ReceitaIntroducao.Id);

            /*
            db.RECE_Receita.Attach(model.Receita);
            db.Entry(model.Receita).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            */

            i = 0;

            foreach (RECE_ReceitaIntroducaoTexto texto in model.ReceitaIntroducaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update RECE_ReceitaIntroducaoTexto SET  Titulo = '" + texto.Titulo + "', "
                  + "Introducao = '" + texto.Introducao + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            /*
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Impossivel editar Categoria de Receita");

                return PartialView("_EditarCategoriaReceitaPartial", model);
            }
            */


            return RedirectToAction("Index");


        }


        [HttpGet]
        public ActionResult ApagarReceitaIntroducao(int id)
        {

            RECE_ReceitaIntroducao model = db.RECE_ReceitaIntroducaoTexto.Include("CONF_Idioma").Include("RECE_ReceitaIntroducao")
                   .Select(x => x.RECE_ReceitaIntroducao).Where(c => c.Id == id).FirstOrDefault();



            ReceitaIntroducaoViewModel IntroducaoModel = new ReceitaIntroducaoViewModel();
            IntroducaoModel.ReceitaIntroducao = model;
            IntroducaoModel.ReceitaIntroducaoTexto = model.RECE_ReceitaIntroducaoTexto.ToList();
            IntroducaoModel.Idioma = model.RECE_ReceitaIntroducaoTexto.Select(x => x.CONF_Idioma).ToList();



            return PartialView("_ApagarReceitaIntroducaoPartial", IntroducaoModel);

        }



        [HttpGet]
        public ActionResult ReceitaIntroducaoImagem(int? id)
        {

            RECE_ReceitaIntroducaoTexto model = db.RECE_ReceitaIntroducaoTexto.Include("CONF_Idioma")
                .Where(m => m.ReceitaIntroducaoId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }



        [HttpPost]
        public ActionResult ApagarReceitaIntroducaoConfirmar(ReceitaIntroducaoViewModel model)
        {
            RECE_ReceitaIntroducao ReceitaIntroducao = db.RECE_ReceitaIntroducaoTexto.Include("CONF_Idioma").Include("RECE_Receita")
                  .Select(x => x.RECE_ReceitaIntroducao).Where(c => c.Id == model.ReceitaIntroducao.Id).FirstOrDefault();

            foreach (var item in ReceitaIntroducao.RECE_ReceitaIntroducaoTexto.ToList())
            {
                db.RECE_ReceitaIntroducaoTexto.Remove(item);
            }

            db.SaveChanges();

            db.RECE_ReceitaIntroducao.Attach(ReceitaIntroducao);

            db.RECE_ReceitaIntroducao.Remove(ReceitaIntroducao);

            db.SaveChanges();


            return RedirectToAction("Index");
        }



        public JsonResult ListaReceitaCozinheiros([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<RECE_Cozinheiro> query = db.RECE_Cozinheiro.Include("RECE_CozinheiroTexto")
                .Where(m => m.RECE_CozinheiroTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));
             

          

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();


                query = query.Where(x => x.Nome.Contains(value) || x.Email.Contains(value));
                    
                

            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Nome asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(m => new
                {
                    CozinheiroId = m.Id,
                    Nome = m.Nome,
                    Email = m.Email,
                    Introducao = m.RECE_CozinheiroTexto.FirstOrDefault().Introducao.Substring(1, 70) + "...",                  
                    Imagem = m.Imagem,
                    Activo = m.Activo

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);


        }

        [HttpGet]
        public ActionResult CriarReceitaCozinheiro()
        {
            CozinheiroViewModel model = new CozinheiroViewModel();
            model.Cozinheiro.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RECE_CozinheiroTexto midiaTexto = new RECE_CozinheiroTexto()
                {

                    CONF_Idioma = idioma,
                    Introducao = ""

                };
                model.CozinheiroTexto.Add(midiaTexto);
             
            }

            return View("CriarReceitaCozinheiro", model);

        }     

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CriarReceitaCozinheiro(CozinheiroViewModel model)
        {
            model.Cozinheiro.DataCriacao = DateTime.Now;
            model.Cozinheiro.DataModificacao = DateTime.Now;

            int i = 0;
          
                if (model.File.File != null)
                {
                    string directory = "~/Content/files/anexos/";
                    if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                    }

                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension((model.File.File.FileName.ToLower()));
                model.File.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                    model.Cozinheiro.Imagem = fileName;

                }
              
          
            i = 0;
          



            i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (RECE_CozinheiroTexto textos in model.CozinheiroTexto)
            {

                textos.RECE_Cozinheiro = model.Cozinheiro;
                textos.CONF_Idioma = idioma[i];
                textos.Introducao = System.Net.WebUtility.HtmlDecode(textos.Introducao);

                i++;

            }



            model.Cozinheiro.RECE_CozinheiroTexto = model.CozinheiroTexto;
            db.RECE_Cozinheiro.Add(model.Cozinheiro);
            db.SaveChanges();


            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("index");


        }


        [HttpGet]
        public ActionResult DetalhesReceitaCozinheiro(int? id)
        {
            RECE_Cozinheiro model = db.RECE_Cozinheiro.Include(x => x.RECE_CozinheiroTexto)
                .Include(m => m.RECE_CozinheiroTexto.Select(e => e.CONF_Idioma))
                .Where(c => c.Id == id).SingleOrDefault();


               


            CozinheiroViewModel cozinheiroModel = new CozinheiroViewModel();
            cozinheiroModel.Cozinheiro = model;
            cozinheiroModel.CozinheiroTexto = model.RECE_CozinheiroTexto.ToList();
            cozinheiroModel.Idioma = model.RECE_CozinheiroTexto.Select(x => x.CONF_Idioma).ToList();
           
            return PartialView("_DetalhesReceitaCozinheiroPartial", cozinheiroModel);

        }

        [HttpGet]
        public ActionResult ApagarReceitaCozinheiro(int? id)
        {
            RECE_Cozinheiro model = db.RECE_Cozinheiro.Include(x => x.RECE_CozinheiroTexto)
                .Include(m => m.RECE_Receita)
                .Include(m => m.RECE_CozinheiroTexto.Select(e => e.CONF_Idioma))

                .Where(c => c.Id == id).SingleOrDefault();

            if(model.RECE_Receita.Count > 0)
            {
                ViewBag.ApagarRelacao = "O Cozinheiro não pode ser apagdo por ter receitas relacionadas";
            }



            CozinheiroViewModel cozinheiroModel = new CozinheiroViewModel();
            cozinheiroModel.Cozinheiro = model;
            cozinheiroModel.CozinheiroTexto = model.RECE_CozinheiroTexto.ToList();
            cozinheiroModel.Idioma = model.RECE_CozinheiroTexto.Select(x => x.CONF_Idioma).ToList();
            return PartialView("_ApagarReceitaCozinheiroPartial", cozinheiroModel);

        }

      
        [HttpPost]
        public ActionResult ApagarReceitaCozinheiroConfirmar(CozinheiroViewModel model)
        {
            RECE_Cozinheiro cozinheiro = db.RECE_Cozinheiro.Include(m => m.RECE_CozinheiroTexto)                
                .FirstOrDefault(x => x.Id == model.Cozinheiro.Id);

            foreach (var item in cozinheiro.RECE_CozinheiroTexto.ToList())
            {
                db.RECE_CozinheiroTexto.Remove(item);
            }

            db.SaveChanges();

            db.RECE_Cozinheiro.Attach(cozinheiro);

            db.RECE_Cozinheiro.Remove(cozinheiro);

            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditarReceitaCozinheiro(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RECE_Cozinheiro cozinheiro = db.RECE_Cozinheiro.Include(x => x.RECE_CozinheiroTexto)
               .Include(m => m.RECE_CozinheiroTexto.Select(e => e.CONF_Idioma))
               .Where(c => c.Id == id).SingleOrDefault();


            if (cozinheiro == null)
            {
                return HttpNotFound();
            }


            CozinheiroViewModel cozinheiroModel = new CozinheiroViewModel();
            cozinheiroModel.Cozinheiro = cozinheiro;
            cozinheiroModel.CozinheiroTexto = cozinheiro.RECE_CozinheiroTexto.ToList();
            cozinheiroModel.Idioma = db.CONF_Idioma.ToList();
            

            return View("EditarReceitaCozinheiro", cozinheiroModel);

           


        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarReceitaCozinheiro(CozinheiroViewModel model)
        {
            
            model.Cozinheiro.DataModificacao = DateTime.Now;


            //int i = 0;

            if (model.File.File != null)
            {
                string directory = "~/Content/files/anexos/";
                if (!Directory.Exists(Server.MapPath("~/Content/files/anexos/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Content/files/anexos/"));
                }

                var fileName = Guid.NewGuid().ToString() + Path.GetExtension(model.File.File.FileName.ToLower());
                model.File.File.SaveAs(Path.Combine(Server.MapPath(directory), fileName));

                model.Cozinheiro.Imagem = fileName;

            }


            //i = 0;



            db.Database.ExecuteSqlCommand("Update RECE_Cozinheiro SET  Activo = '" + model.Cozinheiro.Activo +"', Nome = '" + model.Cozinheiro.Nome + "', Email = '" + model.Cozinheiro.Email 
                + "', Imagem='" + model.Cozinheiro.Imagem  + "'  WHERE ID= " + model.Cozinheiro.Id);


            db.SaveChanges();

            foreach (RECE_CozinheiroTexto texto in model.CozinheiroTexto)
            {

                db.Database.ExecuteSqlCommand("Update RECE_CozinheiroTexto SET  "
                  + "Introducao = '" + texto.Introducao + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID = " + texto.Id);
            }


            db.SaveChanges();

            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult GetCozinheiroImagem(int? id)
        {

            RECE_Cozinheiro model = db.RECE_Cozinheiro
                .Where(m => m.Id == id).FirstOrDefault();
                



            ImagemViewModel imagemModel = new ImagemViewModel();
            imagemModel.Imagem = model.Imagem; ;

            return PartialView("_ImagemPreviewPartial", imagemModel );

        }


        //Tipo de Receita - ActionMethods
        public JsonResult ListaTipoReceitas([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<RECE_TipoReceita> query = db.RECE_TipoReceita.Include("RECE_TipoReceitaTexto")
                .Where(m => m.RECE_TipoReceitaTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));




            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();


                query = query
                    .Where(m => m.RECE_TipoReceitaTexto.Any(mt => mt.Descricao.Contains(value)));



            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(m => new
                {
                    TipoReceitaId = m.Id,
                    Descricao = m.RECE_TipoReceitaTexto.FirstOrDefault().Descricao,
                    Activo = m.Activo,
                    
                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);


        }

        //Tipo de Receitas
        [HttpGet]
        public ActionResult CriarTipoReceita()
        {
            TipoReceitaViewModel model = new TipoReceitaViewModel();
            model.TipoReceita.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RECE_TipoReceitaTexto texto = new RECE_TipoReceitaTexto()
                {

                    CONF_Idioma = idioma

                };
                model.TipoReceitaTexto.Add(texto);

            }

            return PartialView("_CriarTipoReceita", model);

        }


        [HttpPost] 
        public async Task<ActionResult> CriarTipoReceita(TipoReceitaViewModel model)
        {
            model.TipoReceita.DataCriacao = DateTime.Now;
       

            int i = 0;
            List<CONF_Idioma> idioma = db.CONF_Idioma.ToList();
            foreach (RECE_TipoReceitaTexto textos in model.TipoReceitaTexto)
            {
               
                textos.RECE_TipoReceita = model.TipoReceita;
                textos.CONF_Idioma = idioma[i];
                i++;

            }

            model.TipoReceita.RECE_TipoReceitaTexto = model.TipoReceitaTexto;
            db.RECE_TipoReceita.Add(model.TipoReceita);
            var task =  db.SaveChangesAsync();
            await task;

            if(task.Exception != null)
            {
                ModelState.AddModelError("", "Impossivel criar tipo de receita");
                
                return View("_CriarTipoReceita",model);
            }

            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }

            return RedirectToAction("Index");





        }

        [HttpGet]
        public ActionResult DetalhesTipoReceita(int? id)
        {
            RECE_TipoReceita model = db.RECE_TipoReceita.Include(x => x.RECE_TipoReceitaTexto)
                .Include(m => m.RECE_TipoReceitaTexto.Select(e => e.CONF_Idioma))
                .Where(c => c.Id == id).SingleOrDefault();





            TipoReceitaViewModel TipoReceitaModel = new TipoReceitaViewModel();
            TipoReceitaModel.TipoReceita = model;
            TipoReceitaModel.TipoReceitaTexto = model.RECE_TipoReceitaTexto.ToList();
            TipoReceitaModel.Idioma = model.RECE_TipoReceitaTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_DetalhesTipoReceitaPartial", TipoReceitaModel);

        }


        [HttpGet]
        public ActionResult ApagarTipoReceita(int? id)
        {
            RECE_TipoReceita model = db.RECE_TipoReceita.Include(x => x.RECE_TipoReceitaTexto)
                .Include(m => m.RECE_Receita)
                .Include(m => m.RECE_TipoReceitaTexto.Select(e => e.CONF_Idioma))
                .Where(c => c.Id == id).SingleOrDefault();



            if (model.RECE_Receita.Count > 0)
            {
                ViewBag.ApagarRelacao = "O Tipo não pode ser apagdo por ter receitas relacionadas";
            }

            TipoReceitaViewModel TipoReceitaModel = new TipoReceitaViewModel();
            TipoReceitaModel.TipoReceita = model;
            TipoReceitaModel.TipoReceitaTexto = model.RECE_TipoReceitaTexto.ToList();
            TipoReceitaModel.Idioma = model.RECE_TipoReceitaTexto.Select(x => x.CONF_Idioma).ToList();

            return PartialView("_ApagarTipoReceitaPartial", TipoReceitaModel);

        }

        /*Apagar Menu Post*/
        [HttpPost]
        public async Task<ActionResult> ApagarTipoReceitaConfirmar(TipoReceitaViewModel model)
        {
            RECE_TipoReceita receita = db.RECE_TipoReceita.Include(m => m.RECE_TipoReceitaTexto)
                .FirstOrDefault(x => x.Id == model.TipoReceita.Id);



            foreach (var item in receita.RECE_TipoReceitaTexto.ToList())
            {
                db.RECE_TipoReceitaTexto.Remove(item);
            }

            var task = db.SaveChangesAsync();
            await task;

            if (task.Exception != null)
            {
                ModelState.AddModelError("", "Impossivel apagar tipo receita");

                return View("_ApagarTipoReceitaPartial", model);
            }

            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }

         

            db.RECE_TipoReceita.Attach(receita);

            db.RECE_TipoReceita.Remove(receita);

           task = db.SaveChangesAsync();
            await task;

            


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditarTipoReceita(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RECE_TipoReceita model = db.RECE_TipoReceita.Where(c => c.Id == id)
                .Include(x => x.RECE_TipoReceitaTexto)
                .Include(m => m.RECE_TipoReceitaTexto.Select(e => e.CONF_Idioma))
                .SingleOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            TipoReceitaViewModel TipoReceitaModel = new TipoReceitaViewModel();
            TipoReceitaModel.TipoReceita = model;
            TipoReceitaModel.TipoReceitaTexto = model.RECE_TipoReceitaTexto.ToList();
            TipoReceitaModel.Idioma = model.RECE_TipoReceitaTexto.Select(x => x.CONF_Idioma).ToList();
        

            return PartialView("_EditarTipoReceitaPartial", TipoReceitaModel);

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarTipoReceita(TipoReceitaViewModel model)
        {
          
           

            
            db.RECE_TipoReceita.Attach(model.TipoReceita);
            db.Entry(model.TipoReceita).State = EntityState.Modified;
            int i = 0;
         
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                model.TipoReceitaTexto[i].IdiomaId = idioma.Id;
                model.TipoReceitaTexto[i].TipoReceitaId = model.TipoReceita.Id;
                model.TipoReceitaTexto[i].CONF_Idioma = idioma;
                model.TipoReceitaTexto[i].RECE_TipoReceita = model.TipoReceita;

                bool saveFailed;
                do
                {
                    saveFailed = false;

                    try
                    {
                        db.Entry(model.TipoReceitaTexto[i]).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store 
                        ex.Entries.Single().Reload();
                    }

                } while (saveFailed);             
                
          
                i++;
            }
            
            db.SaveChanges();

            /*
            if (!ModelState.IsValid)
            {
                ViewBag.TipoMidia = new SelectList(db.MIDI_TipoMidia.ToList(), "Id", "Descricao");
                ModelState.AddModelError("", "Impossivel criar o Conteudo.");
                return View("CriarMidiaConteudo", model);
            }*/

            return RedirectToAction("Index");

        }


        //CRUD conteudo das Receitas   
        public JsonResult ListaReceita([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
          IQueryable<RECE_Receita> query = db.RECE_Receita.Include(r => r.RECE_ReceitaTexto)
                .Include(r => r.RECE_TipoReceita).Include(r => r.RECE_TipoReceita.RECE_TipoReceitaTexto)
                .Include(r => r.RECE_Cozinheiro).Include(r => r.RECE_Cozinheiro.RECE_CozinheiroTexto)
                .Where(r => r.RECE_ReceitaTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();
                /*

                query = query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                         .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                         */
            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(r => new
                {
                    ReceitaId = r.Id,
                    Tipo = r.RECE_TipoReceita.RECE_TipoReceitaTexto.FirstOrDefault().Descricao,
                    Cozinheiro = r.RECE_Cozinheiro.Nome + "...",
                    Descricao = (r.RECE_ReceitaTexto.FirstOrDefault().Descricao.Length < 70 ? r.RECE_ReceitaTexto.FirstOrDefault().Descricao 
                                    : r.RECE_ReceitaTexto.FirstOrDefault().Descricao.Substring(0,70) + "..."),
                    Introducao = (r.RECE_ReceitaTexto.FirstOrDefault().Introducao.Length < 70 ? r.RECE_ReceitaTexto.FirstOrDefault().Introducao
                                    : r.RECE_ReceitaTexto.FirstOrDefault().Introducao.Substring(0, 70) + "..."),
                    Activo = r.Activo,
                    Imagem = r.Id,
                    PDF = r.RECE_ReceitaTexto.FirstOrDefault().PDF,      


                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }


        //Criar Receita Conteudo
        [HttpGet]       
        public ActionResult CriarReceita()
        {

           
            ReceitaViewModel receitaModel = new ReceitaViewModel();
            receitaModel.Receita.Activo = true;
            foreach (CONF_Idioma idioma in db.CONF_Idioma.ToList())
            {
                RECE_ReceitaTexto receitaTexto = new RECE_ReceitaTexto()
                {
                    CONF_Idioma = idioma
                };
                receitaModel.ReceitaTexto.Add(receitaTexto);
               
            
                ImageUpload file = new ImageUpload();
                receitaModel.File.Add(file);
                FileDocumento doc = new FileDocumento();
                receitaModel.FileDocumento.Add(doc);
               

            }

            var query = from tr in db.RECE_TipoReceita
                        join trt in db.RECE_TipoReceitaTexto on tr.Id equals trt.TipoReceitaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };

            ViewBag.TipoReceita = new SelectList(query, "Id", "Descricao");

            ViewBag.Cozinheiro = new SelectList(db.RECE_Cozinheiro.ToList(), "Id", "Nome");

            ViewBag.ReceitaIdProvisario = DateTime.Now.ToString();

            ViewBag.Operacao = "Receita";



            return View("CriarReceita", receitaModel);

        }

        [HttpPost]
        [ValidateInput(false)]
      //  [ValidateAntiForgeryToken]
        public ActionResult CriarReceita(int? id)
        {

   
            ReceitaViewModel model = new ReceitaViewModel();
        

            model.Receita.DataCriacao = DateTime.Now;
            model.Receita.DataModificacao = DateTime.Now;
            model.Receita.Ordem = Convert.ToInt32(Request.Form["Ordem"]);
            model.Receita.TipoReceitaId = Convert.ToInt32(Request.Form["Tipo"]);
            model.Receita.CozinheiroId = Convert.ToInt32(Request.Form["Cozinheiro"]);
            model.Receita.DuracaoMinutos = Convert.ToInt32(Request.Form["Duracao"]);
            model.Receita.QuantidadePessoas =  Convert.ToInt32(Request.Form["Pessoas"]);
          
            model.Receita.Activo = bool.Parse(Request.Form["Activo"]);


            RECE_ReceitaTexto receita = new RECE_ReceitaTexto();
            receita.RECE_Receita = model.Receita;
            receita.IdiomaId = 1;
            receita.Descricao = Request.Form["Descricao"];
            receita.Introducao = Request.Form["Introducao"];



            var NomeUnico = "";



            if (Request.Files["FileAnexo"] != null)
            {
                var file = Request.Files["FileAnexo"];

                if (file.FileName != "")
                {
                    var ext = System.IO.Path.GetExtension(file.FileName);
                    NomeUnico = Guid.NewGuid() + ext;
                    string directory = "~/Content/files/anexos/";

                    file.SaveAs(Path.Combine(Server.MapPath(directory), NomeUnico));
                    receita.PDF = NomeUnico;
                }
            }


            if (Request.Files["FileImagem"] != null)
            {
                var file = Request.Files["FileImagem"];
                if (file.FileName != "")
                {

                    var ext = System.IO.Path.GetExtension(file.FileName);

                    NomeUnico = Guid.NewGuid() + ext;
                    string directory = "~/Content/files/anexos/";

                    file.SaveAs(Path.Combine(Server.MapPath(directory), NomeUnico));
                    receita.Imagem = NomeUnico;

                }
            }

          




            model.Receita.RECE_ReceitaTexto.Add(receita);
            db.RECE_Receita.Add(model.Receita);
            db.SaveChanges();

           
            return new JsonResult { Data = model.Receita.Id};

        }


        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
   
        public ActionResult CriarIngredientes()
        {

            int id = Convert.ToInt32(Request.Form["ReceitaId"]);
            string ingredientes = Request.Form["Ingredientes"];

            RECE_ReceitaTexto model = db.RECE_ReceitaTexto.Include("RECE_Receita")
                .Where(r => r.ReceitaId == id).FirstOrDefault();

            model.Ingredientes = Request.Unvalidated.Form["Ingredientes"];

            db.Database.ExecuteSqlCommand("Update RECE_ReceitaTexto SET Descricao = '" + model.Descricao + "',   Introducao = '" + model.Introducao + "', "
                  + " Ingredientes = '" + ingredientes + "', Imagem='" + model.Imagem + "', Link = '" + model.Link + "', PDF ='" + model.PDF + "'  WHERE ID= " + model.Id);

            db.SaveChanges();


            return new JsonResult { Data = model.ReceitaId};
        }





        public ActionResult ApagarReceita(int id)
        {
            RECE_Receita receita = db.RECE_Receita.Include(m => m.RECE_ReceitaTexto).Include(m => m.RECE_TipoReceita)
                .FirstOrDefault(x => x.Id == id);

            ReceitaViewModel modelView = new ReceitaViewModel();
            modelView.Receita = receita;
            modelView.ReceitaTexto = receita.RECE_ReceitaTexto.ToList();
            modelView.TipoReceita = receita.RECE_TipoReceita;

            var query = from tr in db.RECE_TipoReceita
                        join trt in db.RECE_TipoReceitaTexto on tr.Id equals trt.TipoReceitaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };

            ViewBag.TipoReceita = new SelectList(query, "Id", "Descricao");

            ViewBag.Cozinheiro = new SelectList(db.RECE_Cozinheiro.ToList(), "Id", "Nome");


            return PartialView("_ApagarReceitaPartial", modelView);
        }

        [HttpPost]
        public ActionResult ApagarReceitaConteudoConfirmar(ReceitaViewModel model)
        {
            RECE_Receita receita = db.RECE_Receita.Include(m => m.RECE_ReceitaTexto).Include(m => m.RECE_TipoReceita)               
                .FirstOrDefault(x => x.Id == model.Receita.Id);


            List<RECE_ReceitaPreparacao> preparacao = db.RECE_ReceitaPreparacao
                .Include(m => m.RECE_ReceitaPreparacaoTexto).Where(x => x.Id == receita.Id)
                .ToList();

            foreach(RECE_ReceitaPreparacao item in preparacao)
            {
                foreach(RECE_ReceitaPreparacaoTexto texto in item.RECE_ReceitaPreparacaoTexto.ToList())
                {
                    db.RECE_ReceitaPreparacaoTexto.Remove(texto);
                }

                db.RECE_ReceitaPreparacao.Remove(item);

            }

            db.SaveChanges();

            foreach (var item in receita.RECE_ReceitaTexto.ToList())
            {
                db.RECE_ReceitaTexto.Remove(item);
            }

            db.SaveChanges();

            db.RECE_Receita.Attach(receita);

            db.RECE_Receita.Remove(receita);

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult EditarReceita(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RECE_Receita receita = db.RECE_Receita.Include(x => x.RECE_ReceitaTexto)
               .Include(m => m.RECE_ReceitaTexto.Select(e => e.CONF_Idioma))
               .Where(c => c.Id == id).SingleOrDefault();


            if (receita == null)
            {
                return HttpNotFound();
            }


            ReceitaViewModel receitaModel = new ReceitaViewModel();
            receitaModel.Receita = receita;
            receitaModel.ReceitaTexto = receita.RECE_ReceitaTexto.ToList();
            receitaModel.Idioma = db.CONF_Idioma.ToList();
            foreach(var item in receitaModel.Idioma)
            {
                receitaModel.File.Add(new ImageUpload());
                receitaModel.FileDocumento.Add(new FileDocumento());
            }

            var query = from tr in db.RECE_TipoReceita
                        join trt in db.RECE_TipoReceitaTexto on tr.Id equals trt.TipoReceitaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };

            ViewBag.TipoReceita = new SelectList(query, "Id", "Descricao");

            ViewBag.Cozinheiro = new SelectList(db.RECE_Cozinheiro.ToList(), "Id", "Nome");

            return View("EditarReceita", receitaModel);




        }


        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]  

        public ActionResult EditarReceitaConfirmar()
        {

            int id =  Convert.ToInt32(Request.Form["IdReceita"]);
            
            RECE_Receita receita = db.RECE_Receita.Include(x => x.RECE_ReceitaTexto)
             .Include(m => m.RECE_ReceitaTexto.Select(e => e.CONF_Idioma))
             .Where(c => c.Id == id).SingleOrDefault();


            if (receita == null)
            {
                return HttpNotFound();
            }

            ReceitaViewModel model = new ReceitaViewModel();
            model.Receita = receita;
            model.ReceitaTexto = receita.RECE_ReceitaTexto.ToList();

            model.Receita.Id = id;
            model.Receita.DataModificacao = DateTime.Now;
            model.Receita.Ordem = Convert.ToInt32(Request.Form["Ordem"]);
            model.Receita.TipoReceitaId = Convert.ToInt32(Request.Form["Tipo"]);
            model.Receita.CozinheiroId = Convert.ToInt32(Request.Form["Cozinheiro"]);
            model.Receita.DuracaoMinutos = Convert.ToInt32(Request.Form["Duracao"]);
            model.Receita.QuantidadePessoas = Convert.ToInt32(Request.Form["Pessoas"]);
          
            model.Receita.Activo = bool.Parse(Request.Form["Activo"]);



            model.ReceitaTexto.FirstOrDefault().RECE_Receita = model.Receita;
            model.ReceitaTexto.FirstOrDefault().IdiomaId = 1;
            model.ReceitaTexto.FirstOrDefault().Descricao = Request.Form["Descricao"];
            model.ReceitaTexto.FirstOrDefault().Introducao = Request.Form["Introducao"];

            

            var NomeUnico = "";

            if (Request.Form["Imagem"] != null)
            {
                model.ReceitaTexto.FirstOrDefault().Imagem = Request.Form["Anexo"];
            }
            else
            {
                if (Request.Files["FileAnexo"] != null)
                {
                    var file = Request.Files["FileAnexo"];

                    if (file.FileName != "")
                    {
                        var ext = System.IO.Path.GetExtension(file.FileName);
                        NomeUnico = Guid.NewGuid() + ext;
                        string directory = "~/Content/files/anexos/";

                        file.SaveAs(Path.Combine(Server.MapPath(directory), NomeUnico));
                        model.ReceitaTexto.FirstOrDefault().PDF = NomeUnico;
                    }
                }


            }


            if (Request.Form["Imagem"] != null)
            {
                model.ReceitaTexto.FirstOrDefault().Imagem = Request.Form["Imagem"];
            }
            else
            {

                if (Request.Files["FileImagem"] != null)
                {
                    var file = Request.Files["FileImagem"];
                    if (file.FileName != "")
                    {

                        var ext = System.IO.Path.GetExtension(file.FileName);

                        NomeUnico = Guid.NewGuid() + ext;
                        string directory = "~/Content/files/anexos/";

                        file.SaveAs(Path.Combine(Server.MapPath(directory), NomeUnico));
                        model.ReceitaTexto.FirstOrDefault().Imagem = NomeUnico;

                    }
                }
            }








         


            db.Database.ExecuteSqlCommand("Update RECE_Receita SET TipoReceitaId = " + model.Receita.TipoReceitaId + ", CozinheiroId = " + model.Receita.CozinheiroId + ", "
                   + " DuracaoMinutos = "+ model.Receita.DuracaoMinutos + ", QuantidadePessoas = "+ model.Receita.QuantidadePessoas+", Activo = '" + model.Receita.Activo + "'  WHERE ID = " + model.Receita.Id);



            db.SaveChanges();

            foreach (RECE_ReceitaTexto texto in model.ReceitaTexto)
            {

                db.Database.ExecuteSqlCommand("Update RECE_ReceitaTexto SET Descricao = '" + texto.Descricao + "',   Introducao = '" + texto.Introducao + "',  "
                  + " Conteudo = '" + texto.Conteudo + "', Imagem='" + texto.Imagem + "', Link = '" + texto.Link + "', PDF ='" + texto.PDF + "'  WHERE ID= " + texto.Id);
            }

            db.SaveChanges();


            return new JsonResult { Data = model.Receita.Id };


        }

        [HttpGet]
        public ActionResult DetalhesReceita(int id)
        {
            RECE_Receita model = db.RECE_ReceitaTexto.Include("CONF_Idioma").Include("RECE_Receita")
               .Select(x => x.RECE_Receita).Where(c => c.Id == id).FirstOrDefault();



            ReceitaViewModel receitaModel = new ReceitaViewModel();
            receitaModel.Receita = model;
            receitaModel.ReceitaTexto = model.RECE_ReceitaTexto.ToList();
            receitaModel.Idioma = model.RECE_ReceitaTexto.Select(x => x.CONF_Idioma).ToList();


            var query = from tr in db.RECE_TipoReceita
                        join trt in db.RECE_TipoReceitaTexto on tr.Id equals trt.TipoReceitaId
                        join ci in db.CONF_Idioma on trt.IdiomaId equals ci.Id
                        where ci.Codigo.ToUpper() == "PT"
                        select new { Id = tr.Id, Descricao = trt.Descricao };

            ViewBag.TipoReceita = new SelectList(query, "Id", "Descricao");

            ViewBag.Cozinheiro = new SelectList(db.RECE_Cozinheiro.ToList(), "Id", "Nome");

            return PartialView("_DetalhesReceita", receitaModel);


        }

        [HttpGet]
        public ActionResult ReceitaImagem(int id)
        {

            RECE_ReceitaTexto model = db.RECE_ReceitaTexto.Include("CONF_Idioma")
                .Where(m => m.ReceitaId == id && m.CONF_Idioma.Codigo.ToUpper() == "PT")
                .SingleOrDefault();



            ImagemViewModel imgModel = new ImagemViewModel();
            imgModel.Imagem = model.Imagem;

            return PartialView("_ImagemPreviewPartial", imgModel);

        }

        //CRUD conteudo das Receitas   
        public JsonResult ListaReceitaBanners([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            IQueryable<RECE_ReceitaBanner> query = db.RECE_ReceitaBanner.Include(r => r.RECE_ReceitaBannerTexto)              
                
                  .Where(r => r.RECE_ReceitaBannerTexto.Any(mt => mt.CONF_Idioma.Codigo.ToUpper().Equals("PT")));

            var totalCount = query.Count();

            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();
                /*

                query = query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                         .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                         */
            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Titulo asc" : orderByString);

            #endregion Sorting  

            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(r => new
                {
                    Banner = r.Id,
                    Titulo = r.RECE_ReceitaBannerTexto.FirstOrDefault().Titulo,
                  
                    Introducao = r.RECE_ReceitaBannerTexto.FirstOrDefault().Titulo.Substring(1, 70) + "...",
                    Activo = r.Activo,
                   

                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }



        public JsonResult ListaPassos([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {

            int ReceitaId = Convert.ToInt32(Request.Params["ReceitaId"]);

            IQueryable<RECE_ReceitaPreparacao> query = db.RECE_ReceitaPreparacao.Include(r => r.RECE_ReceitaPreparacaoTexto)
                .Where(r => r.ReceitaId == ReceitaId);
               

            var totalCount = query.Count();

         
         
            #region Filtering  
            // Applicar filtros para pesquisas 
            if (requestModel.Search.Value != string.Empty)
            {
                var value = requestModel.Search.Value.Trim();
                /*

                query = query.Include(m => m.MEDI_MediaTexto.Where(mt => mt.Descricao.Contains(value)))
                         .Include(m => m.MEDI_MediaTexto.Where(mt => mt.Introducao.Contains(value)));
                         */
            }

            var filteredCount = query.Count();

            #endregion Filtering  


            #region Sorting  
            // Sorting  
            var sortedColumns = requestModel.Columns.GetSortedColumns();
            var orderByString = String.Empty;

            foreach (var column in sortedColumns)
            {
                orderByString += orderByString != String.Empty ? "," : "";
                orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            }

            query = query.OrderBy(x => orderByString == string.Empty ? "Descricao asc" : orderByString);

            #endregion Sorting  




            // Paging  
            query = query.Skip(requestModel.Start).Take(requestModel.Length);


            var data =
                query.Select(r => new
                {
                    PassoId = r.Id,
                    Ordem = r.Ordem,
                    Descricao = r.RECE_ReceitaPreparacaoTexto.FirstOrDefault().Descricao,
              


                }).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);





        }



        [HttpPost]
        public void CriarPasso()
        {
      

            RECE_ReceitaPreparacao Preparacao = new RECE_ReceitaPreparacao();
            Preparacao.ReceitaId = Convert.ToInt32(Request.Form["ReceitaId"]);
            Preparacao.Ordem = Convert.ToInt32(Request.Form["Ordem"]);

        


            RECE_ReceitaPreparacaoTexto PreparacaoTexto = new RECE_ReceitaPreparacaoTexto();
            PreparacaoTexto.CONF_Idioma = db.CONF_Idioma.Where(p => p.Codigo.ToUpper() == "PT".ToUpper()).FirstOrDefault();
            PreparacaoTexto.RECE_ReceitaPreparacao = Preparacao;
            PreparacaoTexto.Descricao = Request.Form["Descricao"];

            List<RECE_ReceitaPreparacaoTexto> PreparacaoLista = new List<RECE_ReceitaPreparacaoTexto>();
            PreparacaoLista.Add(PreparacaoTexto);

            Preparacao.RECE_ReceitaPreparacaoTexto = PreparacaoLista;

            db.RECE_ReceitaPreparacao.Add(Preparacao);
            db.SaveChanges();


        }

        [HttpGet]
        public ActionResult EditarPasso(int id)
        {
            RECE_ReceitaPreparacao receita = db.RECE_ReceitaPreparacao.Include(m => m.RECE_ReceitaPreparacaoTexto)
                .FirstOrDefault(x => x.Id == id);


     

            var myResult = new
            {
                PassoId  = receita.Id,
                Ordem = receita.Ordem,
                PassoDescricao = receita.RECE_ReceitaPreparacaoTexto.FirstOrDefault().Descricao
            };

            return Json(myResult, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public void EditarPasso()
        {
            int id = Convert.ToInt32(Request.Form["PassoId"]);

            RECE_ReceitaPreparacao Preparacao = db.RECE_ReceitaPreparacao.Include(m => m.RECE_ReceitaPreparacaoTexto)
                .FirstOrDefault(x => x.Id == id);
          

          
            Preparacao.Ordem = Convert.ToInt32(Request.Form["Ordem"]);
            Preparacao.RECE_ReceitaPreparacaoTexto.FirstOrDefault().Descricao = Request.Form["Descricao"];


            db.Database.ExecuteSqlCommand("Update RECE_ReceitaPreparacao SET  Ordem = " + Preparacao.Ordem 
               + "  WHERE ID= " + Preparacao.Id);


            db.SaveChanges();

            foreach (RECE_ReceitaPreparacaoTexto texto in Preparacao.RECE_ReceitaPreparacaoTexto)
            {

                db.Database.ExecuteSqlCommand("Update RECE_ReceitaPreparacaoTexto SET  "
                  + "Descrição = '" + texto.Descricao +"'    WHERE ID = " + texto.Id);
            }


          db.SaveChanges();
        
        }

        [HttpPost]
        public async Task<ActionResult> ApagarPasso(int id)
        {
            RECE_ReceitaPreparacao receita = db.RECE_ReceitaPreparacao.Include(m => m.RECE_ReceitaPreparacaoTexto)
                .FirstOrDefault(x => x.Id == id);



            foreach (var item in receita.RECE_ReceitaPreparacaoTexto.ToList())
            {
                db.RECE_ReceitaPreparacaoTexto.Remove(item);
            }

            var task = db.SaveChangesAsync();
            await task;





            db.RECE_ReceitaPreparacao.Attach(receita);

            db.RECE_ReceitaPreparacao.Remove(receita);

            task = db.SaveChangesAsync();
            await task;

          
            if (Request.IsAjaxRequest())
            {
                return Content("sucesso");
            }



            return Content("sucesso");
        }


        [HttpGet]
        public ActionResult EditarPaginaInactiva()
        {
            
            Menu model = db.Menu.Include("MenuTexto")
               .Where(c => c.Id == 11).FirstOrDefault();


            if (model == null)
            {
                return HttpNotFound();
            }


            MenuViewModel menu = new MenuViewModel();
            menu.Menu = model;
            menu.MenuTexto = model.MenuTexto.ToList();
            //menu.Idioma = model.MenuTexto.Select(x => x.CONF_Idioma).ToList();


            return PartialView("_PaginaInactivaPartial", menu);

        }

        [HttpPost]        
        public void EditarPaginaInactiva(MenuViewModel model)
        {


            db.Menu.Attach(model.Menu);
            db.Entry(model.Menu).State = System.Data.Entity.EntityState.Modified;
           db.SaveChanges();
         
            



        }

       


    }


}