// <auto-generated />
namespace Webtech_Office.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddedCaixa : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedCaixa));
        
        string IMigrationMetadata.Id
        {
            get { return "201712151055244_AddedCaixa"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
