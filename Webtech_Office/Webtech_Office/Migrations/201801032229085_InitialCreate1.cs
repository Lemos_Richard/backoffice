namespace Webtech_Office.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            CreateTable(
                "dbo.Categoria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SeccaoId = c.Int(),
                        DataCriacao = c.DateTime(precision: 7, storeType: "datetime2"),
                        DataModificao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Seccao", t => t.SeccaoId)
                .Index(t => t.SeccaoId);
            
            CreateTable(
                "dbo.CategoriaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .ForeignKey("dbo.Categoria", t => t.CategoriaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.CONF_Idioma",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CONT_ContactoBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ContactoBannerId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONT_ContactoBanner", t => t.ContactoBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ContactoBannerId);
            
            CreateTable(
                "dbo.CONT_ContactoBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CONT_ContactoIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ContactoIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONT_ContactoIntroducao", t => t.ContactoIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ContactoIntroducaoId);
            
            CreateTable(
                "dbo.CONT_ContactoIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CONT_ContactoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ContactoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONT_Contacto", t => t.ContactoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ContactoId);
            
            CreateTable(
                "dbo.CONT_Contacto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        TipoContactoId = c.Int(nullable: false),
                        HorarioInicio = c.DateTime(precision: 7, storeType: "datetime2"),
                        HorarioFim = c.DateTime(precision: 7, storeType: "datetime2"),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONT_TipoContacto", t => t.TipoContactoId)
                .Index(t => t.TipoContactoId);
            
            CreateTable(
                "dbo.CONT_TipoContacto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CONT_TipoContactoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        TipoContactoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONT_TipoContacto", t => t.TipoContactoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.TipoContactoId);
            
            CreateTable(
                "dbo.LOJA_Contacto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        TipoContactoId = c.Int(nullable: false),
                        LojaId = c.Int(nullable: false),
                        HorarioInicio = c.DateTime(precision: 7, storeType: "datetime2"),
                        HorarioFim = c.DateTime(precision: 7, storeType: "datetime2"),
                        UsarIntroducao = c.Boolean(nullable: false),
                        UsarLink = c.Boolean(nullable: false),
                        UsarPdf = c.Boolean(nullable: false),
                        UsarImagem = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Loja", t => t.LojaId)
                .ForeignKey("dbo.CONT_TipoContacto", t => t.TipoContactoId)
                .Index(t => t.TipoContactoId)
                .Index(t => t.LojaId);
            
            CreateTable(
                "dbo.Loja_ContactoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ContactoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Contacto", t => t.ContactoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ContactoId);
            
            CreateTable(
                "dbo.LOJA_Loja",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 300),
                        Morada = c.String(nullable: false, maxLength: 300),
                        Email = c.String(nullable: false, maxLength: 300),
                        Telefone = c.String(maxLength: 300),
                        Horarios = c.String(maxLength: 300),
                        HorarioInicio = c.Time(precision: 7),
                        HorarioFim = c.Time(precision: 7),
                        Responsavel = c.String(maxLength: 150),
                        Provincia = c.Int(nullable: false),
                        Municipio = c.Int(),
                        Mapa = c.String(),
                        Latitude = c.String(maxLength: 100),
                        Longitude = c.String(maxLength: 100),
                        Destaque = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(nullable: false),
                        PartilhaTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONF_Provincia", t => t.Provincia)
                .ForeignKey("dbo.CONF_Municipio", t => t.Municipio)
                .Index(t => t.Provincia)
                .Index(t => t.Municipio);
            
            CreateTable(
                "dbo.CONF_Municipio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProvinciaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONF_Provincia", t => t.ProvinciaId)
                .Index(t => t.ProvinciaId);
            
            CreateTable(
                "dbo.CONF_Provincia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECR_Registo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150),
                        Apelido = c.String(maxLength: 150),
                        Email = c.String(maxLength: 150),
                        Telefone = c.String(maxLength: 50),
                        PDF = c.String(maxLength: 300),
                        ProvinviaId = c.Int(),
                        Assunto = c.String(),
                        Mensagem = c.String(),
                        Data = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONF_Provincia", t => t.ProvinviaId)
                .Index(t => t.ProvinviaId);
            
            CreateTable(
                "dbo.LOJA_Anexo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LojaId = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Loja", t => t.LojaId)
                .Index(t => t.LojaId);
            
            CreateTable(
                "dbo.LOJA_AnexoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        AnexoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        PDF = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Video = c.String(maxLength: 300),
                        Outro = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Anexo", t => t.AnexoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.AnexoId);
            
            CreateTable(
                "dbo.LOJA_LojaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        LojaId = c.Int(nullable: false),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Loja", t => t.LojaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.LojaId);
            
            CreateTable(
                "dbo.LOJA_Servico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LojaId = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Loja", t => t.LojaId)
                .Index(t => t.LojaId);
            
            CreateTable(
                "dbo.Loja_ServicoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        LojaServicoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_Servico", t => t.LojaServicoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.LojaServicoId);
            
            CreateTable(
                "dbo.CONT_OndeEstamosIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        OndeEstamosIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONT_OndeEstamosIntroducao", t => t.OndeEstamosIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.OndeEstamosIntroducaoId);
            
            CreateTable(
                "dbo.CONT_OndeEstamosIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EMPR_AjudaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        AjudaId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 300),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EMPR_Ajuda", t => t.AjudaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.AjudaId);
            
            CreateTable(
                "dbo.EMPR_Ajuda",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EMPR_BlogTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        BlogId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EMPR_Blog", t => t.BlogId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.BlogId);
            
            CreateTable(
                "dbo.EMPR_Blog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PartilharFacebook = c.Boolean(nullable: false),
                        PartilharTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EMPR_PrivacidadeTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        PrivacidadeId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EMPR_Privacidade", t => t.PrivacidadeId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.PrivacidadeId);
            
            CreateTable(
                "dbo.EMPR_Privacidade",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EMPR_TermosCondicoesTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        TermoCondicoesId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EMPR_TermosCondicoes", t => t.TermoCondicoesId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.TermoCondicoesId);
            
            CreateTable(
                "dbo.EMPR_TermosCondicoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HOME_DestaquesTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        DestaquesId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HOME_Destaques", t => t.DestaquesId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.DestaquesId);
            
            CreateTable(
                "dbo.HOME_Destaques",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HOME_HomePageBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        HomePageBannerId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HOME_HomePageBanner", t => t.HomePageBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.HomePageBannerId);
            
            CreateTable(
                "dbo.HOME_HomePageBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HOME_NovidadesTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        NovidadeId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HOME_Novidades", t => t.NovidadeId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.NovidadeId);
            
            CreateTable(
                "dbo.HOME_Novidades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCricao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HOME_PromocoesTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        PromocoesId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HOME_Promocoes", t => t.PromocoesId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.PromocoesId);
            
            CreateTable(
                "dbo.HOME_Promocoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UsarCaixaTexto = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LOJA_LojaBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        LojaBannerId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_LojaBanner", t => t.LojaBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.LojaBannerId);
            
            CreateTable(
                "dbo.LOJA_LojaBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LOJA_LojaIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        LojaIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LOJA_LojaIntroducao", t => t.LojaIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.LojaIntroducaoId);
            
            CreateTable(
                "dbo.LOJA_LojaIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MEDI_AnexoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        AnexoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Video = c.String(maxLength: 300),
                        Outro = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MEDI_Anexo", t => t.AnexoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.AnexoId);
            
            CreateTable(
                "dbo.MEDI_Anexo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MidiaId = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MEDI_Media", t => t.MidiaId)
                .Index(t => t.MidiaId);
            
            CreateTable(
                "dbo.MEDI_Media",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        TipoMidia = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false, storeType: "date"),
                        Destaque = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(nullable: false),
                        PartilhaTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MEDI_TipoMedia", t => t.TipoMidia)
                .Index(t => t.TipoMidia);
            
            CreateTable(
                "dbo.MEDI_MediaTexto",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        MidiaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.MEDI_Media", t => t.MidiaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.MidiaId);
            
            CreateTable(
                "dbo.MEDI_TipoMedia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MEDI_TipoMediaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        TipoMediaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MEDI_TipoMedia", t => t.TipoMediaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.TipoMediaId);
            
            CreateTable(
                "dbo.MEDI_MediaBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        MediaBannerId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Introducao = c.String(maxLength: 10, fixedLength: true),
                        Imagem = c.String(maxLength: 10, fixedLength: true),
                        Link = c.String(maxLength: 10, fixedLength: true),
                        PDF = c.String(maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MEDI_MediaBanner", t => t.MediaBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.MediaBannerId);
            
            CreateTable(
                "dbo.MEDI_MediaBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MEDI_MediaIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        MediaIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MEDI_MediaIntroducao", t => t.MediaIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.MediaIntroducaoId);
            
            CreateTable(
                "dbo.MEDI_MediaIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        MenuId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        PalavraChave = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menu", t => t.MenuId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.MenuId);
            
            CreateTable(
                "dbo.Menu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Dependencia = c.Int(),
                        TipoDeEstrutura = c.String(nullable: false, maxLength: 50, unicode: false),
                        Estado = c.Boolean(nullable: false),
                        Inactivo = c.Boolean(),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificação = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menu", t => t.Dependencia)
                .Index(t => t.Dependencia);
            
            CreateTable(
                "dbo.NOTI_CategoriaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NOTI_Categoria", t => t.CategoriaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.NOTI_Categoria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NOTI_Noticia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoriaId = c.Int(nullable: false),
                        Fonte = c.String(unicode: false),
                        Destaque = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(),
                        PartilhaTwitter = c.Boolean(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NOTI_Categoria", t => t.CategoriaId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.NOTI_NoticiaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        NoticiaId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Banner = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NOTI_Noticia", t => t.IdiomaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId);
            
            CreateTable(
                "dbo.PROD_AnexoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        IdiomaId = c.Int(nullable: false),
                        AnexoId = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 150),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Video = c.String(maxLength: 300),
                        Outro = c.String(maxLength: 300),
                        DataCriacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_Anexo", t => t.AnexoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.AnexoId)
                .Index(t => t.AnexoId);
            
            CreateTable(
                "dbo.PROD_Anexo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProdutoId = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_Produto", t => t.ProdutoId)
                .Index(t => t.ProdutoId);
            
            CreateTable(
                "dbo.PROD_Produto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                        Preco = c.Decimal(nullable: false, precision: 18, scale: 0),
                        Desconto = c.Decimal(precision: 18, scale: 0),
                        Promocoes = c.Boolean(nullable: false),
                        Destaques = c.Boolean(nullable: false),
                        Novidades = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(nullable: false),
                        PartilhaTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PrecoComDesconto = c.Decimal(nullable: false, precision: 18, scale: 0),
                        Codigo = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_CategoriaProduto", t => t.CategoriaId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.PROD_CategoriaProduto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PROD_CategoriaProdutoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_CategoriaProduto", t => t.CategoriaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.PROD_Imagens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProdutoIdProvisorio = c.String(unicode: false),
                        ProdutoId = c.Int(),
                        Fotografia = c.String(unicode: false),
                        FotografiaCaminho = c.String(unicode: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_Produto", t => t.ProdutoId)
                .Index(t => t.ProdutoId);
            
            CreateTable(
                "dbo.PROD_ProdutoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ProdutoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_Produto", t => t.ProdutoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ProdutoId);
            
            CreateTable(
                "dbo.PROD_ProdutoBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        LojaBannerId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_ProdutoBanner", t => t.LojaBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.LojaBannerId);
            
            CreateTable(
                "dbo.PROD_ProdutoBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PROD_ProdutoIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ProdutoIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PROD_ProdutoIntroducao", t => t.ProdutoIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ProdutoIntroducaoId);
            
            CreateTable(
                "dbo.PROD_ProdutoIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QUEM_AcercaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        AcercaId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Banner = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QUEM_Acerca", t => t.AcercaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.AcercaId);
            
            CreateTable(
                "dbo.QUEM_Acerca",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Fonte = c.String(maxLength: 300),
                        Destaque = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(nullable: false),
                        PartilhaTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QUEM_QuemBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        QuemBannerId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QUEM_QuemBanner", t => t.QuemBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.QuemBannerId);
            
            CreateTable(
                "dbo.QUEM_QuemBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QUEM_QuemIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        QuemIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 300),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QUEM_QuemIntroducao", t => t.QuemIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.QuemIntroducaoId);
            
            CreateTable(
                "dbo.QUEM_QuemIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECE_AnexoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        AnexoId = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 150),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Video = c.String(maxLength: 300),
                        Outro = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_Anexo", t => t.AnexoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.AnexoId);
            
            CreateTable(
                "dbo.RECE_Anexo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReceitaId = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_Receita", t => t.ReceitaId)
                .Index(t => t.ReceitaId);
            
            CreateTable(
                "dbo.RECE_Receita",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        TipoReceitaId = c.Int(nullable: false),
                        CozinheiroId = c.Int(),
                        DuracaoMinutos = c.Int(),
                        QuantidadePessoas = c.Int(),
                        Fonte = c.String(maxLength: 300),
                        ReceitaDaSemana = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(nullable: false),
                        PartilhaTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_Cozinheiro", t => t.CozinheiroId)
                .ForeignKey("dbo.RECE_TipoReceita", t => t.TipoReceitaId)
                .Index(t => t.TipoReceitaId)
                .Index(t => t.CozinheiroId);
            
            CreateTable(
                "dbo.RECE_Cozinheiro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 150),
                        Email = c.String(maxLength: 300, unicode: false),
                        Imagem = c.String(unicode: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECE_CozinheiroTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        CozinheiroId = c.Int(nullable: false),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_Cozinheiro", t => t.CozinheiroId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.CozinheiroId);
            
            CreateTable(
                "dbo.RECE_ReceitaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ReceitaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Ingredientes = c.String(),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_Receita", t => t.ReceitaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ReceitaId);
            
            CreateTable(
                "dbo.RECE_TipoReceita",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECE_TipoReceitaTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        TipoReceitaId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_TipoReceita", t => t.TipoReceitaId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.TipoReceitaId);
            
            CreateTable(
                "dbo.RECE_ReceitaBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        BannerId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_ReceitaBanner", t => t.BannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.BannerId);
            
            CreateTable(
                "dbo.RECE_ReceitaBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECE_ReceitaIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ReceitaIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(nullable: false, maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_ReceitaIntroducao", t => t.ReceitaIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ReceitaIntroducaoId);
            
            CreateTable(
                "dbo.RECE_ReceitaIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECE_ReceitaPreparacaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ReceitaPreparacaoId = c.Int(nullable: false),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECE_ReceitaPreparacao", t => t.ReceitaPreparacaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ReceitaPreparacaoId);
            
            CreateTable(
                "dbo.RECE_ReceitaPreparacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        ReceitaId = c.Int(),
                        ReceitaIdProvisorio = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RECR_Anexo",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        RecrutamentoId = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CONF_Idioma", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.RECR_AnexoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        AnexoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        PDF = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Video = c.String(maxLength: 300),
                        Outro = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECR_Anexo", t => t.AnexoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.AnexoId);
            
            CreateTable(
                "dbo.RECR_ConteudoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ConteudoId = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RECR_Conteudo", t => t.ConteudoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ConteudoId);
            
            CreateTable(
                "dbo.RECR_Conteudo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Fonte = c.String(maxLength: 300),
                        Destaque = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(nullable: false),
                        PartilhaTwitter = c.Boolean(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RESP_RespSocialIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        RespSocialIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RESP_RespSocialIntroducao", t => t.RespSocialIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.RespSocialIntroducaoId);
            
            CreateTable(
                "dbo.RESP_RespSocialIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RESP_RespSocialTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        RespSocialId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RESP_RespSocial", t => t.RespSocialId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.RespSocialId);
            
            CreateTable(
                "dbo.RESP_RespSocial",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        DataEvento = c.DateTime(nullable: false, storeType: "date"),
                        Activo = c.Boolean(),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SeccaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        SeccaoId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Seccao", t => t.SeccaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.SeccaoId);
            
            CreateTable(
                "dbo.Seccao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Estado = c.Boolean(),
                        UsaCategoria = c.Boolean(nullable: false),
                        DataCricao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HTML",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SeccaoId = c.Int(nullable: false),
                        Ordem = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        ConteudoHTML = c.String(),
                        ConteudoData = c.DateTime(precision: 7, storeType: "datetime2"),
                        ConteudoString = c.String(),
                        Opcao = c.Boolean(),
                        Opcao2 = c.Boolean(),
                        Opcao3 = c.Boolean(),
                        Banner = c.String(),
                        Imagem = c.String(maxLength: 50),
                        pdf = c.String(),
                        Estado = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Seccao", t => t.SeccaoId)
                .Index(t => t.SeccaoId);
            
            CreateTable(
                "dbo.SERV_ServicoBannerTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ServicoBannerId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SERV_ServicoBanner", t => t.ServicoBannerId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ServicoBannerId);
            
            CreateTable(
                "dbo.SERV_ServicoBanner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SERV_ServicoIntroducaoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ServicoIntroducaoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        Imagem = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SERV_ServicoIntroducao", t => t.ServicoIntroducaoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ServicoIntroducaoId);
            
            CreateTable(
                "dbo.SERV_ServicoIntroducao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SERV_ServicoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        ServicoId = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                        Conteudo = c.String(),
                        Banner = c.String(maxLength: 300),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        PalavraChave = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SERV_Servico", t => t.ServicoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.ServicoId);
            
            CreateTable(
                "dbo.SERV_Servico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipoServicoID = c.Int(nullable: false),
                        Fonte = c.String(),
                        Destaque = c.Boolean(nullable: false),
                        PartilhaFacebook = c.Boolean(),
                        PartilhaTwitter = c.Boolean(),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SERV_TipoServico", t => t.TipoServicoID)
                .Index(t => t.TipoServicoID);
            
            CreateTable(
                "dbo.SERV_TipoServico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataModificacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificador = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SERV_TipoServicoTexto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdiomaId = c.Int(nullable: false),
                        TipoServicoId = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Introducao = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SERV_TipoServico", t => t.TipoServicoId)
                .ForeignKey("dbo.CONF_Idioma", t => t.IdiomaId)
                .Index(t => t.IdiomaId)
                .Index(t => t.TipoServicoId);
            
            CreateTable(
                "dbo.CONT_FormularioContacto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ordem = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 50),
                        Telefone = c.String(maxLength: 50),
                        Assunto = c.String(maxLength: 300),
                        Morada = c.String(maxLength: 300),
                        MunicipioId = c.Int(),
                        ProvinciaId = c.Int(),
                        Link = c.String(maxLength: 300),
                        PDF = c.String(maxLength: 300),
                        Foto = c.String(maxLength: 300),
                        Outro = c.String(maxLength: 300),
                        Activo = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CONT_Newsletters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 300),
                        Data = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CONT_OndeEstamos",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Morada = c.String(nullable: false, maxLength: 300),
                        Rua = c.String(nullable: false, maxLength: 300),
                        Mapa = c.String(nullable: false, maxLength: 128),
                        Harario = c.String(maxLength: 300),
                        ContactoInstitucional = c.String(maxLength: 300),
                        ContactosComercial = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => new { t.Id, t.Morada, t.Rua, t.Mapa });
            
            CreateTable(
                "dbo.CONT_PontosAtendimento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Morada = c.String(nullable: false, maxLength: 300),
                        Rua = c.String(maxLength: 300),
                        Mapa = c.String(),
                        Horario = c.String(maxLength: 300),
                        Contactos = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUserLogins");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId });
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId });
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.CategoriaTexto", "CategoriaId", "dbo.Categoria");
            DropForeignKey("dbo.SERV_TipoServicoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.SERV_ServicoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.SERV_TipoServicoTexto", "TipoServicoId", "dbo.SERV_TipoServico");
            DropForeignKey("dbo.SERV_Servico", "TipoServicoID", "dbo.SERV_TipoServico");
            DropForeignKey("dbo.SERV_ServicoTexto", "ServicoId", "dbo.SERV_Servico");
            DropForeignKey("dbo.SERV_ServicoIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.SERV_ServicoIntroducaoTexto", "ServicoIntroducaoId", "dbo.SERV_ServicoIntroducao");
            DropForeignKey("dbo.SERV_ServicoBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.SERV_ServicoBannerTexto", "ServicoBannerId", "dbo.SERV_ServicoBanner");
            DropForeignKey("dbo.SeccaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.SeccaoTexto", "SeccaoId", "dbo.Seccao");
            DropForeignKey("dbo.HTML", "SeccaoId", "dbo.Seccao");
            DropForeignKey("dbo.Categoria", "SeccaoId", "dbo.Seccao");
            DropForeignKey("dbo.RESP_RespSocialTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RESP_RespSocialTexto", "RespSocialId", "dbo.RESP_RespSocial");
            DropForeignKey("dbo.RESP_RespSocialIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RESP_RespSocialIntroducaoTexto", "RespSocialIntroducaoId", "dbo.RESP_RespSocialIntroducao");
            DropForeignKey("dbo.RECR_ConteudoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECR_ConteudoTexto", "ConteudoId", "dbo.RECR_Conteudo");
            DropForeignKey("dbo.RECR_AnexoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECR_Anexo", "Id", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECR_AnexoTexto", "AnexoId", "dbo.RECR_Anexo");
            DropForeignKey("dbo.RECE_TipoReceitaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_ReceitaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_ReceitaPreparacaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_ReceitaPreparacaoTexto", "ReceitaPreparacaoId", "dbo.RECE_ReceitaPreparacao");
            DropForeignKey("dbo.RECE_ReceitaIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_ReceitaIntroducaoTexto", "ReceitaIntroducaoId", "dbo.RECE_ReceitaIntroducao");
            DropForeignKey("dbo.RECE_ReceitaBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_ReceitaBannerTexto", "BannerId", "dbo.RECE_ReceitaBanner");
            DropForeignKey("dbo.RECE_CozinheiroTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_AnexoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.RECE_TipoReceitaTexto", "TipoReceitaId", "dbo.RECE_TipoReceita");
            DropForeignKey("dbo.RECE_Receita", "TipoReceitaId", "dbo.RECE_TipoReceita");
            DropForeignKey("dbo.RECE_ReceitaTexto", "ReceitaId", "dbo.RECE_Receita");
            DropForeignKey("dbo.RECE_Receita", "CozinheiroId", "dbo.RECE_Cozinheiro");
            DropForeignKey("dbo.RECE_CozinheiroTexto", "CozinheiroId", "dbo.RECE_Cozinheiro");
            DropForeignKey("dbo.RECE_Anexo", "ReceitaId", "dbo.RECE_Receita");
            DropForeignKey("dbo.RECE_AnexoTexto", "AnexoId", "dbo.RECE_Anexo");
            DropForeignKey("dbo.QUEM_QuemIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.QUEM_QuemIntroducaoTexto", "QuemIntroducaoId", "dbo.QUEM_QuemIntroducao");
            DropForeignKey("dbo.QUEM_QuemBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.QUEM_QuemBannerTexto", "QuemBannerId", "dbo.QUEM_QuemBanner");
            DropForeignKey("dbo.QUEM_AcercaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.QUEM_AcercaTexto", "AcercaId", "dbo.QUEM_Acerca");
            DropForeignKey("dbo.PROD_ProdutoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.PROD_ProdutoIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.PROD_ProdutoIntroducaoTexto", "ProdutoIntroducaoId", "dbo.PROD_ProdutoIntroducao");
            DropForeignKey("dbo.PROD_ProdutoBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.PROD_ProdutoBannerTexto", "LojaBannerId", "dbo.PROD_ProdutoBanner");
            DropForeignKey("dbo.PROD_CategoriaProdutoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.PROD_AnexoTexto", "AnexoId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.PROD_ProdutoTexto", "ProdutoId", "dbo.PROD_Produto");
            DropForeignKey("dbo.PROD_Imagens", "ProdutoId", "dbo.PROD_Produto");
            DropForeignKey("dbo.PROD_Produto", "CategoriaId", "dbo.PROD_CategoriaProduto");
            DropForeignKey("dbo.PROD_CategoriaProdutoTexto", "CategoriaId", "dbo.PROD_CategoriaProduto");
            DropForeignKey("dbo.PROD_Anexo", "ProdutoId", "dbo.PROD_Produto");
            DropForeignKey("dbo.PROD_AnexoTexto", "AnexoId", "dbo.PROD_Anexo");
            DropForeignKey("dbo.NOTI_NoticiaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.NOTI_CategoriaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.NOTI_Noticia", "CategoriaId", "dbo.NOTI_Categoria");
            DropForeignKey("dbo.NOTI_NoticiaTexto", "IdiomaId", "dbo.NOTI_Noticia");
            DropForeignKey("dbo.NOTI_CategoriaTexto", "CategoriaId", "dbo.NOTI_Categoria");
            DropForeignKey("dbo.MenuTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.MenuTexto", "MenuId", "dbo.Menu");
            DropForeignKey("dbo.Menu", "Dependencia", "dbo.Menu");
            DropForeignKey("dbo.MEDI_TipoMediaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.MEDI_MediaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.MEDI_MediaIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.MEDI_MediaIntroducaoTexto", "MediaIntroducaoId", "dbo.MEDI_MediaIntroducao");
            DropForeignKey("dbo.MEDI_MediaBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.MEDI_MediaBannerTexto", "MediaBannerId", "dbo.MEDI_MediaBanner");
            DropForeignKey("dbo.MEDI_AnexoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.MEDI_TipoMediaTexto", "TipoMediaId", "dbo.MEDI_TipoMedia");
            DropForeignKey("dbo.MEDI_Media", "TipoMidia", "dbo.MEDI_TipoMedia");
            DropForeignKey("dbo.MEDI_MediaTexto", "MidiaId", "dbo.MEDI_Media");
            DropForeignKey("dbo.MEDI_Anexo", "MidiaId", "dbo.MEDI_Media");
            DropForeignKey("dbo.MEDI_AnexoTexto", "AnexoId", "dbo.MEDI_Anexo");
            DropForeignKey("dbo.Loja_ServicoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.LOJA_LojaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.LOJA_LojaIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.LOJA_LojaIntroducaoTexto", "LojaIntroducaoId", "dbo.LOJA_LojaIntroducao");
            DropForeignKey("dbo.LOJA_LojaBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.LOJA_LojaBannerTexto", "LojaBannerId", "dbo.LOJA_LojaBanner");
            DropForeignKey("dbo.Loja_ContactoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.LOJA_AnexoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.HOME_PromocoesTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.HOME_PromocoesTexto", "PromocoesId", "dbo.HOME_Promocoes");
            DropForeignKey("dbo.HOME_NovidadesTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.HOME_NovidadesTexto", "NovidadeId", "dbo.HOME_Novidades");
            DropForeignKey("dbo.HOME_HomePageBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.HOME_HomePageBannerTexto", "HomePageBannerId", "dbo.HOME_HomePageBanner");
            DropForeignKey("dbo.HOME_DestaquesTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.HOME_DestaquesTexto", "DestaquesId", "dbo.HOME_Destaques");
            DropForeignKey("dbo.EMPR_TermosCondicoesTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.EMPR_TermosCondicoesTexto", "TermoCondicoesId", "dbo.EMPR_TermosCondicoes");
            DropForeignKey("dbo.EMPR_PrivacidadeTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.EMPR_PrivacidadeTexto", "PrivacidadeId", "dbo.EMPR_Privacidade");
            DropForeignKey("dbo.EMPR_BlogTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.EMPR_BlogTexto", "BlogId", "dbo.EMPR_Blog");
            DropForeignKey("dbo.EMPR_AjudaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.EMPR_AjudaTexto", "AjudaId", "dbo.EMPR_Ajuda");
            DropForeignKey("dbo.CONT_TipoContactoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.CONT_OndeEstamosIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.CONT_OndeEstamosIntroducaoTexto", "OndeEstamosIntroducaoId", "dbo.CONT_OndeEstamosIntroducao");
            DropForeignKey("dbo.CONT_ContactoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.LOJA_Contacto", "TipoContactoId", "dbo.CONT_TipoContacto");
            DropForeignKey("dbo.LOJA_Servico", "LojaId", "dbo.LOJA_Loja");
            DropForeignKey("dbo.Loja_ServicoTexto", "LojaServicoId", "dbo.LOJA_Servico");
            DropForeignKey("dbo.LOJA_LojaTexto", "LojaId", "dbo.LOJA_Loja");
            DropForeignKey("dbo.LOJA_Contacto", "LojaId", "dbo.LOJA_Loja");
            DropForeignKey("dbo.LOJA_Anexo", "LojaId", "dbo.LOJA_Loja");
            DropForeignKey("dbo.LOJA_AnexoTexto", "AnexoId", "dbo.LOJA_Anexo");
            DropForeignKey("dbo.LOJA_Loja", "Municipio", "dbo.CONF_Municipio");
            DropForeignKey("dbo.RECR_Registo", "ProvinviaId", "dbo.CONF_Provincia");
            DropForeignKey("dbo.LOJA_Loja", "Provincia", "dbo.CONF_Provincia");
            DropForeignKey("dbo.CONF_Municipio", "ProvinciaId", "dbo.CONF_Provincia");
            DropForeignKey("dbo.Loja_ContactoTexto", "ContactoId", "dbo.LOJA_Contacto");
            DropForeignKey("dbo.CONT_TipoContactoTexto", "TipoContactoId", "dbo.CONT_TipoContacto");
            DropForeignKey("dbo.CONT_Contacto", "TipoContactoId", "dbo.CONT_TipoContacto");
            DropForeignKey("dbo.CONT_ContactoTexto", "ContactoId", "dbo.CONT_Contacto");
            DropForeignKey("dbo.CONT_ContactoIntroducaoTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.CONT_ContactoIntroducaoTexto", "ContactoIntroducaoId", "dbo.CONT_ContactoIntroducao");
            DropForeignKey("dbo.CONT_ContactoBannerTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropForeignKey("dbo.CONT_ContactoBannerTexto", "ContactoBannerId", "dbo.CONT_ContactoBanner");
            DropForeignKey("dbo.CategoriaTexto", "IdiomaId", "dbo.CONF_Idioma");
            DropIndex("dbo.SERV_TipoServicoTexto", new[] { "TipoServicoId" });
            DropIndex("dbo.SERV_TipoServicoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.SERV_Servico", new[] { "TipoServicoID" });
            DropIndex("dbo.SERV_ServicoTexto", new[] { "ServicoId" });
            DropIndex("dbo.SERV_ServicoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.SERV_ServicoIntroducaoTexto", new[] { "ServicoIntroducaoId" });
            DropIndex("dbo.SERV_ServicoIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.SERV_ServicoBannerTexto", new[] { "ServicoBannerId" });
            DropIndex("dbo.SERV_ServicoBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.HTML", new[] { "SeccaoId" });
            DropIndex("dbo.SeccaoTexto", new[] { "SeccaoId" });
            DropIndex("dbo.SeccaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RESP_RespSocialTexto", new[] { "RespSocialId" });
            DropIndex("dbo.RESP_RespSocialTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RESP_RespSocialIntroducaoTexto", new[] { "RespSocialIntroducaoId" });
            DropIndex("dbo.RESP_RespSocialIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECR_ConteudoTexto", new[] { "ConteudoId" });
            DropIndex("dbo.RECR_ConteudoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECR_AnexoTexto", new[] { "AnexoId" });
            DropIndex("dbo.RECR_AnexoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECR_Anexo", new[] { "Id" });
            DropIndex("dbo.RECE_ReceitaPreparacaoTexto", new[] { "ReceitaPreparacaoId" });
            DropIndex("dbo.RECE_ReceitaPreparacaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECE_ReceitaIntroducaoTexto", new[] { "ReceitaIntroducaoId" });
            DropIndex("dbo.RECE_ReceitaIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECE_ReceitaBannerTexto", new[] { "BannerId" });
            DropIndex("dbo.RECE_ReceitaBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECE_TipoReceitaTexto", new[] { "TipoReceitaId" });
            DropIndex("dbo.RECE_TipoReceitaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECE_ReceitaTexto", new[] { "ReceitaId" });
            DropIndex("dbo.RECE_ReceitaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECE_CozinheiroTexto", new[] { "CozinheiroId" });
            DropIndex("dbo.RECE_CozinheiroTexto", new[] { "IdiomaId" });
            DropIndex("dbo.RECE_Receita", new[] { "CozinheiroId" });
            DropIndex("dbo.RECE_Receita", new[] { "TipoReceitaId" });
            DropIndex("dbo.RECE_Anexo", new[] { "ReceitaId" });
            DropIndex("dbo.RECE_AnexoTexto", new[] { "AnexoId" });
            DropIndex("dbo.RECE_AnexoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.QUEM_QuemIntroducaoTexto", new[] { "QuemIntroducaoId" });
            DropIndex("dbo.QUEM_QuemIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.QUEM_QuemBannerTexto", new[] { "QuemBannerId" });
            DropIndex("dbo.QUEM_QuemBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.QUEM_AcercaTexto", new[] { "AcercaId" });
            DropIndex("dbo.QUEM_AcercaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.PROD_ProdutoIntroducaoTexto", new[] { "ProdutoIntroducaoId" });
            DropIndex("dbo.PROD_ProdutoIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.PROD_ProdutoBannerTexto", new[] { "LojaBannerId" });
            DropIndex("dbo.PROD_ProdutoBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.PROD_ProdutoTexto", new[] { "ProdutoId" });
            DropIndex("dbo.PROD_ProdutoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.PROD_Imagens", new[] { "ProdutoId" });
            DropIndex("dbo.PROD_CategoriaProdutoTexto", new[] { "CategoriaId" });
            DropIndex("dbo.PROD_CategoriaProdutoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.PROD_Produto", new[] { "CategoriaId" });
            DropIndex("dbo.PROD_Anexo", new[] { "ProdutoId" });
            DropIndex("dbo.PROD_AnexoTexto", new[] { "AnexoId" });
            DropIndex("dbo.NOTI_NoticiaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.NOTI_Noticia", new[] { "CategoriaId" });
            DropIndex("dbo.NOTI_CategoriaTexto", new[] { "CategoriaId" });
            DropIndex("dbo.NOTI_CategoriaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.Menu", new[] { "Dependencia" });
            DropIndex("dbo.MenuTexto", new[] { "MenuId" });
            DropIndex("dbo.MenuTexto", new[] { "IdiomaId" });
            DropIndex("dbo.MEDI_MediaIntroducaoTexto", new[] { "MediaIntroducaoId" });
            DropIndex("dbo.MEDI_MediaIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.MEDI_MediaBannerTexto", new[] { "MediaBannerId" });
            DropIndex("dbo.MEDI_MediaBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.MEDI_TipoMediaTexto", new[] { "TipoMediaId" });
            DropIndex("dbo.MEDI_TipoMediaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.MEDI_MediaTexto", new[] { "MidiaId" });
            DropIndex("dbo.MEDI_MediaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.MEDI_Media", new[] { "TipoMidia" });
            DropIndex("dbo.MEDI_Anexo", new[] { "MidiaId" });
            DropIndex("dbo.MEDI_AnexoTexto", new[] { "AnexoId" });
            DropIndex("dbo.MEDI_AnexoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.LOJA_LojaIntroducaoTexto", new[] { "LojaIntroducaoId" });
            DropIndex("dbo.LOJA_LojaIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.LOJA_LojaBannerTexto", new[] { "LojaBannerId" });
            DropIndex("dbo.LOJA_LojaBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.HOME_PromocoesTexto", new[] { "PromocoesId" });
            DropIndex("dbo.HOME_PromocoesTexto", new[] { "IdiomaId" });
            DropIndex("dbo.HOME_NovidadesTexto", new[] { "NovidadeId" });
            DropIndex("dbo.HOME_NovidadesTexto", new[] { "IdiomaId" });
            DropIndex("dbo.HOME_HomePageBannerTexto", new[] { "HomePageBannerId" });
            DropIndex("dbo.HOME_HomePageBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.HOME_DestaquesTexto", new[] { "DestaquesId" });
            DropIndex("dbo.HOME_DestaquesTexto", new[] { "IdiomaId" });
            DropIndex("dbo.EMPR_TermosCondicoesTexto", new[] { "TermoCondicoesId" });
            DropIndex("dbo.EMPR_TermosCondicoesTexto", new[] { "IdiomaId" });
            DropIndex("dbo.EMPR_PrivacidadeTexto", new[] { "PrivacidadeId" });
            DropIndex("dbo.EMPR_PrivacidadeTexto", new[] { "IdiomaId" });
            DropIndex("dbo.EMPR_BlogTexto", new[] { "BlogId" });
            DropIndex("dbo.EMPR_BlogTexto", new[] { "IdiomaId" });
            DropIndex("dbo.EMPR_AjudaTexto", new[] { "AjudaId" });
            DropIndex("dbo.EMPR_AjudaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.CONT_OndeEstamosIntroducaoTexto", new[] { "OndeEstamosIntroducaoId" });
            DropIndex("dbo.CONT_OndeEstamosIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.Loja_ServicoTexto", new[] { "LojaServicoId" });
            DropIndex("dbo.Loja_ServicoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.LOJA_Servico", new[] { "LojaId" });
            DropIndex("dbo.LOJA_LojaTexto", new[] { "LojaId" });
            DropIndex("dbo.LOJA_LojaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.LOJA_AnexoTexto", new[] { "AnexoId" });
            DropIndex("dbo.LOJA_AnexoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.LOJA_Anexo", new[] { "LojaId" });
            DropIndex("dbo.RECR_Registo", new[] { "ProvinviaId" });
            DropIndex("dbo.CONF_Municipio", new[] { "ProvinciaId" });
            DropIndex("dbo.LOJA_Loja", new[] { "Municipio" });
            DropIndex("dbo.LOJA_Loja", new[] { "Provincia" });
            DropIndex("dbo.Loja_ContactoTexto", new[] { "ContactoId" });
            DropIndex("dbo.Loja_ContactoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.LOJA_Contacto", new[] { "LojaId" });
            DropIndex("dbo.LOJA_Contacto", new[] { "TipoContactoId" });
            DropIndex("dbo.CONT_TipoContactoTexto", new[] { "TipoContactoId" });
            DropIndex("dbo.CONT_TipoContactoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.CONT_Contacto", new[] { "TipoContactoId" });
            DropIndex("dbo.CONT_ContactoTexto", new[] { "ContactoId" });
            DropIndex("dbo.CONT_ContactoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.CONT_ContactoIntroducaoTexto", new[] { "ContactoIntroducaoId" });
            DropIndex("dbo.CONT_ContactoIntroducaoTexto", new[] { "IdiomaId" });
            DropIndex("dbo.CONT_ContactoBannerTexto", new[] { "ContactoBannerId" });
            DropIndex("dbo.CONT_ContactoBannerTexto", new[] { "IdiomaId" });
            DropIndex("dbo.CategoriaTexto", new[] { "CategoriaId" });
            DropIndex("dbo.CategoriaTexto", new[] { "IdiomaId" });
            DropIndex("dbo.Categoria", new[] { "SeccaoId" });
            DropTable("dbo.CONT_PontosAtendimento");
            DropTable("dbo.CONT_OndeEstamos");
            DropTable("dbo.CONT_Newsletters");
            DropTable("dbo.CONT_FormularioContacto");
            DropTable("dbo.SERV_TipoServicoTexto");
            DropTable("dbo.SERV_TipoServico");
            DropTable("dbo.SERV_Servico");
            DropTable("dbo.SERV_ServicoTexto");
            DropTable("dbo.SERV_ServicoIntroducao");
            DropTable("dbo.SERV_ServicoIntroducaoTexto");
            DropTable("dbo.SERV_ServicoBanner");
            DropTable("dbo.SERV_ServicoBannerTexto");
            DropTable("dbo.HTML");
            DropTable("dbo.Seccao");
            DropTable("dbo.SeccaoTexto");
            DropTable("dbo.RESP_RespSocial");
            DropTable("dbo.RESP_RespSocialTexto");
            DropTable("dbo.RESP_RespSocialIntroducao");
            DropTable("dbo.RESP_RespSocialIntroducaoTexto");
            DropTable("dbo.RECR_Conteudo");
            DropTable("dbo.RECR_ConteudoTexto");
            DropTable("dbo.RECR_AnexoTexto");
            DropTable("dbo.RECR_Anexo");
            DropTable("dbo.RECE_ReceitaPreparacao");
            DropTable("dbo.RECE_ReceitaPreparacaoTexto");
            DropTable("dbo.RECE_ReceitaIntroducao");
            DropTable("dbo.RECE_ReceitaIntroducaoTexto");
            DropTable("dbo.RECE_ReceitaBanner");
            DropTable("dbo.RECE_ReceitaBannerTexto");
            DropTable("dbo.RECE_TipoReceitaTexto");
            DropTable("dbo.RECE_TipoReceita");
            DropTable("dbo.RECE_ReceitaTexto");
            DropTable("dbo.RECE_CozinheiroTexto");
            DropTable("dbo.RECE_Cozinheiro");
            DropTable("dbo.RECE_Receita");
            DropTable("dbo.RECE_Anexo");
            DropTable("dbo.RECE_AnexoTexto");
            DropTable("dbo.QUEM_QuemIntroducao");
            DropTable("dbo.QUEM_QuemIntroducaoTexto");
            DropTable("dbo.QUEM_QuemBanner");
            DropTable("dbo.QUEM_QuemBannerTexto");
            DropTable("dbo.QUEM_Acerca");
            DropTable("dbo.QUEM_AcercaTexto");
            DropTable("dbo.PROD_ProdutoIntroducao");
            DropTable("dbo.PROD_ProdutoIntroducaoTexto");
            DropTable("dbo.PROD_ProdutoBanner");
            DropTable("dbo.PROD_ProdutoBannerTexto");
            DropTable("dbo.PROD_ProdutoTexto");
            DropTable("dbo.PROD_Imagens");
            DropTable("dbo.PROD_CategoriaProdutoTexto");
            DropTable("dbo.PROD_CategoriaProduto");
            DropTable("dbo.PROD_Produto");
            DropTable("dbo.PROD_Anexo");
            DropTable("dbo.PROD_AnexoTexto");
            DropTable("dbo.NOTI_NoticiaTexto");
            DropTable("dbo.NOTI_Noticia");
            DropTable("dbo.NOTI_Categoria");
            DropTable("dbo.NOTI_CategoriaTexto");
            DropTable("dbo.Menu");
            DropTable("dbo.MenuTexto");
            DropTable("dbo.MEDI_MediaIntroducao");
            DropTable("dbo.MEDI_MediaIntroducaoTexto");
            DropTable("dbo.MEDI_MediaBanner");
            DropTable("dbo.MEDI_MediaBannerTexto");
            DropTable("dbo.MEDI_TipoMediaTexto");
            DropTable("dbo.MEDI_TipoMedia");
            DropTable("dbo.MEDI_MediaTexto");
            DropTable("dbo.MEDI_Media");
            DropTable("dbo.MEDI_Anexo");
            DropTable("dbo.MEDI_AnexoTexto");
            DropTable("dbo.LOJA_LojaIntroducao");
            DropTable("dbo.LOJA_LojaIntroducaoTexto");
            DropTable("dbo.LOJA_LojaBanner");
            DropTable("dbo.LOJA_LojaBannerTexto");
            DropTable("dbo.HOME_Promocoes");
            DropTable("dbo.HOME_PromocoesTexto");
            DropTable("dbo.HOME_Novidades");
            DropTable("dbo.HOME_NovidadesTexto");
            DropTable("dbo.HOME_HomePageBanner");
            DropTable("dbo.HOME_HomePageBannerTexto");
            DropTable("dbo.HOME_Destaques");
            DropTable("dbo.HOME_DestaquesTexto");
            DropTable("dbo.EMPR_TermosCondicoes");
            DropTable("dbo.EMPR_TermosCondicoesTexto");
            DropTable("dbo.EMPR_Privacidade");
            DropTable("dbo.EMPR_PrivacidadeTexto");
            DropTable("dbo.EMPR_Blog");
            DropTable("dbo.EMPR_BlogTexto");
            DropTable("dbo.EMPR_Ajuda");
            DropTable("dbo.EMPR_AjudaTexto");
            DropTable("dbo.CONT_OndeEstamosIntroducao");
            DropTable("dbo.CONT_OndeEstamosIntroducaoTexto");
            DropTable("dbo.Loja_ServicoTexto");
            DropTable("dbo.LOJA_Servico");
            DropTable("dbo.LOJA_LojaTexto");
            DropTable("dbo.LOJA_AnexoTexto");
            DropTable("dbo.LOJA_Anexo");
            DropTable("dbo.RECR_Registo");
            DropTable("dbo.CONF_Provincia");
            DropTable("dbo.CONF_Municipio");
            DropTable("dbo.LOJA_Loja");
            DropTable("dbo.Loja_ContactoTexto");
            DropTable("dbo.LOJA_Contacto");
            DropTable("dbo.CONT_TipoContactoTexto");
            DropTable("dbo.CONT_TipoContacto");
            DropTable("dbo.CONT_Contacto");
            DropTable("dbo.CONT_ContactoTexto");
            DropTable("dbo.CONT_ContactoIntroducao");
            DropTable("dbo.CONT_ContactoIntroducaoTexto");
            DropTable("dbo.CONT_ContactoBanner");
            DropTable("dbo.CONT_ContactoBannerTexto");
            DropTable("dbo.CONF_Idioma");
            DropTable("dbo.CategoriaTexto");
            DropTable("dbo.Categoria");
            CreateIndex("dbo.AspNetUserLogins", "UserId");
            CreateIndex("dbo.AspNetUserClaims", "UserId");
            CreateIndex("dbo.AspNetUsers", "UserName", unique: true, name: "UserNameIndex");
            CreateIndex("dbo.AspNetUserRoles", "RoleId");
            CreateIndex("dbo.AspNetUserRoles", "UserId");
            CreateIndex("dbo.AspNetRoles", "Name", unique: true, name: "RoleNameIndex");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
        }
    }
}
