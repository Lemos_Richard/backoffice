﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Webtech_Office.Startup))]
namespace Webtech_Office
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
