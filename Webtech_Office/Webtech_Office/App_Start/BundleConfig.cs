﻿using System.Web.Optimization;
using WebHelpers.Mvc5;


namespace Webtech_Office
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval")
              .Include("~/Scripts/jquery.validate*")
                .Include("~/Scripts/jquery.unobtrusive-ajax.min.js")

             );

            bundles.Add(new ScriptBundle("~/bundles/jquery")
             .Include("~/Scripts/jquery-3.2.1.js")
          );

            /*
             .Include("~/Scripts/jquery.unobtrusive-ajax.min.js")
             */



            bundles.Add(new StyleBundle("~/Bundles/css")
                .Include("~/Content/css/bootstrap.min.css", new CssRewriteUrlTransformAbsolute())
                .Include("~/Content/css/select2.min.css")
                .Include("~/Content/css/bootstrap-datepicker3.min.css")
                .Include("~/Content/css/font-awesome.min.css", new CssRewriteUrlTransformAbsolute())
                .Include("~/Content/css/icheck/blue.min.css", new CssRewriteUrlTransformAbsolute())
                .Include("~/Content/css/AdminLTE.css", new CssRewriteUrlTransformAbsolute())
                .Include("~/Content/css/skins/skin-blue.css")
                .Include("~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/Bundles/js")
                
                .Include("~/Content/js/plugins/bootstrap/bootstrap.js")
                .Include("~/Scripts/bootstrap-waitingfor.js")
                .Include("~/Content/js/plugins/fastclick/fastclick.js")
                .Include("~/Content/js/plugins/slimscroll/jquery.slimscroll.js")
                .Include("~/Content/js/plugins/select2/select2.full.js")
                .Include("~/Content/js/plugins/moment/moment.js")
                .Include("~/Content/js/plugins/datepicker/bootstrap-datepicker.js")
                .Include("~/Content/js/plugins/icheck/icheck.js")
                
                .Include("~/Content/js/plugins/inputmask/jquery.inputmask.bundle.js")
                .Include("~/Content/js/adminlte.js")
                .Include("~/Content/js/init.js"));

            // jquery datataables js files
            bundles.Add(new ScriptBundle("~/bundles/datatables")
                .Include("~/Scripts/DataTables/jquery.dataTables.min.js")
                .Include("~/Scripts/DataTables/dataTables.bootstrap.min.js")
               .Include("~/Scripts/DataTables/dataTables.fixedColumns.min.js")
               );


            // jquery datatables css file
            bundles.Add(new StyleBundle("~/Content/datatables")
                .Include("~/Content/DataTables/css/dataTables.bootstrap.min.css")
                .Include("~/Content/DataTables/css/dataTables.bootstrap.min.css")
                      .Include("~/Content/DataTables/css/dataTables.fixedColumns.min.css")

                     );
        }
    }
}
