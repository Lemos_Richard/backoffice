//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webtech_Office.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class NOTI_Noticia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NOTI_Noticia()
        {
            this.NOTI_NoticiaTexto = new HashSet<NOTI_NoticiaTexto>();
        }
    
        public int Id { get; set; }
        public int CategoriaId { get; set; }
        public string Fonte { get; set; }
        public bool Destaque { get; set; }
        public Nullable<bool> PartilhaFacebook { get; set; }
        public Nullable<bool> PartilhaTwitter { get; set; }
        public bool Activo { get; set; }
        public System.DateTime DataCriacao { get; set; }
        public System.DateTime DataModificacao { get; set; }
        public Nullable<int> Modificador { get; set; }
    
        public virtual NOTI_Categoria NOTI_Categoria { get; set; }
        public virtual NOTI_Categoria NOTI_Categoria1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NOTI_NoticiaTexto> NOTI_NoticiaTexto { get; set; }
    }
}
