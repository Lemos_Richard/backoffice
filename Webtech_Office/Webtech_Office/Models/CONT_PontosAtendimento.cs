//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webtech_Office.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONT_PontosAtendimento
    {
        public int Id { get; set; }
        public string Morada { get; set; }
        public string Rua { get; set; }
        public string Mapa { get; set; }
        public string Horario { get; set; }
        public string Contactos { get; set; }
    }
}
