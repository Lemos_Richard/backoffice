//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webtech_Office.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HOME_PromocoesTexto
    {
        public int Id { get; set; }
        public int IdiomaId { get; set; }
        public int PromocoesId { get; set; }
        public string Titulo { get; set; }
        public string Introducao { get; set; }
        public string Imagem { get; set; }
        public string Link { get; set; }
        public string PDF { get; set; }
    
        public virtual CONF_Idioma CONF_Idioma { get; set; }
        public virtual HOME_Promocoes HOME_Promocoes { get; set; }
    }
}
