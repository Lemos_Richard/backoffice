﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class HomePageBannerViewModel
    {

        public HOME_HomePageBanner HomePageBanner { get; set; }

        public List<HOME_HomePageBannerTexto> HomePageBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public HomePageBannerViewModel()
        {

            HomePageBanner = new HOME_HomePageBanner();

            HomePageBannerTexto = new List<HOME_HomePageBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}