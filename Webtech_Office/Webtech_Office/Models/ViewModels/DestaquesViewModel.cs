﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class DestaquesViewModel
    {

        public HOME_Destaques Destaques { get; set; }

        public List<HOME_DestaquesTexto> DestaquesTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public DestaquesViewModel()
        {

            Destaques = new HOME_Destaques();

            DestaquesTexto = new List<HOME_DestaquesTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}