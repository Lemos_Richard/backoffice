﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class QuemBannerViewModel
    {
        public QUEM_QuemBanner QuemBanner { get; set; }

        public List<QUEM_QuemBannerTexto> QuemBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public QuemBannerViewModel()
        {

            QuemBanner = new QUEM_QuemBanner();

            QuemBannerTexto = new List<QUEM_QuemBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}