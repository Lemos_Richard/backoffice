﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class RespSocialViewModel
    {

        public RESP_RespSocial RespSocial { get; set; }

        public List<RESP_RespSocialTexto> RespSocialTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public RespSocialViewModel()
        {



            RespSocial = new RESP_RespSocial();

            RespSocialTexto = new List<RESP_RespSocialTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}