﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ImageUpload
    {
        
        [Display(Name = "Imagem")]
        [FileExtensions(Extensions = "gif,jpg,jpeg,png", ErrorMessage = "Formato Inválido. Apenas .gif, .jpg, .jpeg e .png")]
        public HttpPostedFileBase File { get; set; }
    }

    public class FileDocumento
    {

        [Display(Name ="Documento")]
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.pdf|.doc)$", ErrorMessage = "O ficheiro deve estar no formato .pdf")]
        public HttpPostedFileBase File { get; set; }
    }
}