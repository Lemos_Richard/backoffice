﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ServicoIntroducaoViewModel
    {

        public SERV_ServicoIntroducao ServicoIntroducao { get; set; }

        public List<SERV_ServicoIntroducaoTexto> ServicoIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }



        public ServicoIntroducaoViewModel()
        {

            ServicoIntroducao = new SERV_ServicoIntroducao();

            ServicoIntroducaoTexto = new List<SERV_ServicoIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}