﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models.ViewModels
{
    public class DatePickerModel
    {
        public DateTime Date { get; set; }
    }
}