﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class LojaViewModel
    {

        public LOJA_Loja Loja { get; set; }

        public List<LOJA_LojaTexto> LojaTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public LojaViewModel()
        {

           

            Loja = new LOJA_Loja();

            LojaTexto = new List<LOJA_LojaTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}