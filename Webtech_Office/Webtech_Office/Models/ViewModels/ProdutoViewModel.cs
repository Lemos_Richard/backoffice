﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ProdutoViewModel
    {
        public PROD_CategoriaProduto CategoriaProduto { get; set; }

        public PROD_Produto Produto { get; set; }

        public List<PROD_ProdutoTexto> ProdutoTexto { get; set; }  

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }
        public int PrecoComDesconto { get; set; }
        public ProdutoViewModel()
        {

            CategoriaProduto = new PROD_CategoriaProduto();

            Produto = new PROD_Produto();

            ProdutoTexto = new List<PROD_ProdutoTexto>();
           
            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}