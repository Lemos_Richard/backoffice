﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class RespSocialIntroducaoViewModel
    {

        public RESP_RespSocialIntroducao RespSocialIntroducao { get; set; }

        public List<RESP_RespSocialIntroducaoTexto> RespSocialIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }



        public RespSocialIntroducaoViewModel()
        {

            RespSocialIntroducao = new RESP_RespSocialIntroducao();

            RespSocialIntroducaoTexto = new List<RESP_RespSocialIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}