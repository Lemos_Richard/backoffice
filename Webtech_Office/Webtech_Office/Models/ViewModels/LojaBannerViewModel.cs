﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class LojaBannerViewModel
    {

        public LOJA_LojaBanner LojaBanner { get; set; }

        public List<LOJA_LojaBannerTexto> LojaBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public LojaBannerViewModel()
        {

            LojaBanner = new LOJA_LojaBanner();

            LojaBannerTexto = new List<LOJA_LojaBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}