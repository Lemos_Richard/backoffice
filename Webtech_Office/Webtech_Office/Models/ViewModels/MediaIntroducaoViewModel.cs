﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class MediaIntroducaoViewModel
    {

        public MEDI_MediaIntroducao MediaIntroducao { get; set; }

        public List<MEDI_MediaIntroducaoTexto> MediaIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }



        public MediaIntroducaoViewModel()
        {

            MediaIntroducao = new MEDI_MediaIntroducao();

            MediaIntroducaoTexto = new List<MEDI_MediaIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}