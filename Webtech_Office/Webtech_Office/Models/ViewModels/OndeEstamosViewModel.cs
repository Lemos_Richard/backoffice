﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class OndeEstamosViewModel
    {
        public CONT_OndeEstamos OndeEstamos {get;set;}

        public OndeEstamosViewModel()
        {
            OndeEstamos = new CONT_OndeEstamos();
        }

    }
}