﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class TipoReceitaViewModel
    {
        public RECE_TipoReceita TipoReceita { get; set; }
        public List<RECE_TipoReceitaTexto> TipoReceitaTexto { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }        

        public TipoReceitaViewModel()
        {
            TipoReceita = new RECE_TipoReceita();
            TipoReceitaTexto = new List<RECE_TipoReceitaTexto>();
            Idioma = new List<CONF_Idioma>();
           
        }
    }
}