﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class OndeEstamosIntroducaoViewModel
    {

        public CONT_OndeEstamosIntroducao OndeEstamosIntroducao { get; set; }

        public List<CONT_OndeEstamosIntroducaoTexto> OndeEstamosIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }



        public OndeEstamosIntroducaoViewModel()
        {

            OndeEstamosIntroducao = new CONT_OndeEstamosIntroducao();

            OndeEstamosIntroducaoTexto = new List<CONT_OndeEstamosIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}