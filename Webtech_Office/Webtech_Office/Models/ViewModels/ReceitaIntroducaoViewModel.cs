﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ReceitaIntroducaoViewModel
    {
        public RECE_ReceitaIntroducao ReceitaIntroducao { get; set; }

        public List<RECE_ReceitaIntroducaoTexto> ReceitaIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }



        public ReceitaIntroducaoViewModel()
        {

            ReceitaIntroducao = new RECE_ReceitaIntroducao();

            ReceitaIntroducaoTexto = new List<RECE_ReceitaIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}