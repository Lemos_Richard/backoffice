﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{

    public class ContactoBannerViewModel
    {
        public CONT_ContactoBanner ContactoBanner { get; set; }

        public List<CONT_ContactoBannerTexto> ContactoBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public ContactoBannerViewModel()
        {

            ContactoBanner = new CONT_ContactoBanner();

            ContactoBannerTexto = new List<CONT_ContactoBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}