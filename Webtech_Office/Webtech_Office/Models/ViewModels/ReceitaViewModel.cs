﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ReceitaViewModel
    {

           
        public RECE_TipoReceita TipoReceita { get; set; }
        public RECE_Receita Receita { get; set; }
        public List<RECE_ReceitaTexto> ReceitaTexto { get; set; }
        public List<CONF_Idioma> Idioma = new List<CONF_Idioma>();
        public List<ImageUpload> File { get; set; }
        public List<FileDocumento> FileDocumento { get; set; }

        public ReceitaViewModel()
        {
            Receita = new RECE_Receita();
            ReceitaTexto = new List<RECE_ReceitaTexto>();
          
            File = new List<ImageUpload>();
            FileDocumento = new List<Models.FileDocumento>();
        }
    }
}