﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class MenuViewModel
    {
       

        public Menu Menu { get; set; }
        public List<MenuTexto> MenuTexto { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }
       
        
        public MenuViewModel()
        {
            Menu = new Menu();
            MenuTexto = new List<MenuTexto>();
            Idioma = new List<CONF_Idioma>();
           
            
        }

  
    }
}