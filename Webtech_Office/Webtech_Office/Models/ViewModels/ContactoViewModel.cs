﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ContactoViewModel
    {

        public CONT_Contacto Contacto { get; set; }

        public List<CONT_ContactoTexto> ContactoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public ContactoViewModel()
        {

            Contacto = new CONT_Contacto();

            ContactoTexto = new List<CONT_ContactoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}