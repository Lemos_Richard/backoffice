﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class CozinheiroViewModel
    {

        public RECE_Cozinheiro Cozinheiro { get; set; }

        public List<RECE_CozinheiroTexto> CozinheiroTexto { get; set; }

        public ImageUpload File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public CozinheiroViewModel()
        {

            Cozinheiro = new RECE_Cozinheiro();

            CozinheiroTexto = new List<RECE_CozinheiroTexto>();

            File = new ImageUpload();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}