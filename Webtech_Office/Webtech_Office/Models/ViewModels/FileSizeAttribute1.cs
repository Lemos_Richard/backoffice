﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice2.Models
{
    public class FileSizeAttribute : System.ComponentModel.DataAnnotations.ValidationAttribute
    {
        private readonly int _maxSize;

        public FileSizeAttribute(int maxSize)
        {
            _maxSize = maxSize;
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;

            return (value as HttpPostedFileBase).ContentLength <= _maxSize;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("O tamanho do ficheiro não deve exceder os {0}", _maxSize);
        }
    }

}