﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{

    public class ProdutoBannerViewModel
    {

        
        public PROD_ProdutoBanner ProdutoBanner { get; set; }

        public List<PROD_ProdutoBannerTexto> ProdutoBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public ProdutoBannerViewModel()
        {        

            ProdutoBanner = new PROD_ProdutoBanner();

            ProdutoBannerTexto = new List<PROD_ProdutoBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }



    }


}