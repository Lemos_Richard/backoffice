﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ServicoViewModel
    {

        public SERV_Servico Servico { get; set; }

        public List<SERV_ServicoTexto> ServicoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public ServicoViewModel()
        {



            Servico = new SERV_Servico();

            ServicoTexto = new List<SERV_ServicoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}