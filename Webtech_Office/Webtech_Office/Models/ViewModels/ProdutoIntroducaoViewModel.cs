﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ProdutoIntroducaoViewModel
    {
        public PROD_ProdutoIntroducao ProdutoIntroducao { get; set; }

        public List<PROD_ProdutoIntroducaoTexto> ProdutoIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        

        public ProdutoIntroducaoViewModel()
        {

            ProdutoIntroducao = new PROD_ProdutoIntroducao();

            ProdutoIntroducaoTexto = new List<PROD_ProdutoIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }


}