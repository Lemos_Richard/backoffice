﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class TipoServicoViewModel
    {
        public SERV_TipoServico TipoServico { get; set; }
        public List<SERV_TipoServicoTexto> TipoServicoTexto { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public TipoServicoViewModel()
        {
            TipoServico = new SERV_TipoServico();
            TipoServicoTexto = new List<SERV_TipoServicoTexto>();
            Idioma = new List<CONF_Idioma>();

        }

    }
}