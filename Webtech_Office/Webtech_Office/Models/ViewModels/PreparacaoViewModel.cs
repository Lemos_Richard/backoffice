﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class PreparacaoViewModel
    {
        public string PassoOrdem { get; set; }
        public string PassoDescricao { get; set; }
    }
}