﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class MediaViewModel
    {
        public MEDI_TipoMedia TipoMedia { get; set; }
        public MEDI_Media Media { get; set; }
        public List<MEDI_MediaTexto> MediaTexto { get; set; }
        public MEDI_Anexo Anexo { get; set; }
        public List<MEDI_AnexoTexto> AnexoTexto{get;set;}

        public List<ImageUpload> File { get; set; }
        public List<FileDocumento> FileDocumento { get; set; }

        [Required(ErrorMessage="Obrigatório")]
        public string Data { get; set; }

        public MediaViewModel()
        {
            TipoMedia = new MEDI_TipoMedia();
            Media = new MEDI_Media();
            MediaTexto = new List<MEDI_MediaTexto>();
            Anexo = new MEDI_Anexo();
            AnexoTexto = new List<MEDI_AnexoTexto>();
            File = new List<ImageUpload>();
            FileDocumento = new List<Models.FileDocumento>();
        }
    }
}