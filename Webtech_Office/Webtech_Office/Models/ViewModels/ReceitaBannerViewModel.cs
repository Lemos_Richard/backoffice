﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ReceitaBannerViewModel
    {

        public RECE_ReceitaBanner ReceitaBanner { get; set; }

        public List<RECE_ReceitaBannerTexto> ReceitaBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public ReceitaBannerViewModel()
        {

            ReceitaBanner = new RECE_ReceitaBanner();

            ReceitaBannerTexto = new List<RECE_ReceitaBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }



    }
}