﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class PromocoesViewModel
    {

        public HOME_Promocoes Promocoes { get; set; }

        public List<HOME_PromocoesTexto> PromocoesTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }
        public bool UsarCaixaTexto { get; set; }
        public PromocoesViewModel()
        {

            Promocoes = new HOME_Promocoes();

            PromocoesTexto = new List<HOME_PromocoesTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}