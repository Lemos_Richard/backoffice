﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class CategoriaProdutoViewModel
    {
        public PROD_CategoriaProduto CategoriaProduto { get; set; }
        public List<PROD_CategoriaProdutoTexto> CategoriaProdutoTexto { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public CategoriaProdutoViewModel()
        {
            CategoriaProduto = new PROD_CategoriaProduto();
            CategoriaProdutoTexto = new List<PROD_CategoriaProdutoTexto>();
            Idioma = new List<CONF_Idioma>();

        }

    }
}