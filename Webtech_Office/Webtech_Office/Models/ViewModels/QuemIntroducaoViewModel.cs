﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class QuemIntroducaoViewModel
    {

        public QUEM_QuemIntroducao QuemIntroducao { get; set; }

        public List<QUEM_QuemIntroducaoTexto> QuemIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public QuemIntroducaoViewModel()
        {

            QuemIntroducao = new QUEM_QuemIntroducao();

            QuemIntroducaoTexto = new List<QUEM_QuemIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}