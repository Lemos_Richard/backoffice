﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ServicoBannerViewModel
    {
        public SERV_ServicoBanner ServicoBanner { get; set; }

        public List<SERV_ServicoBannerTexto> ServicoBannerTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }

        public int Preco { get; set; }
        public int Desconto { get; set; }

        public ServicoBannerViewModel()
        {

            ServicoBanner = new SERV_ServicoBanner();

            ServicoBannerTexto = new List<SERV_ServicoBannerTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }



    }
}