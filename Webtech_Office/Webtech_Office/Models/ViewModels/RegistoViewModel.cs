﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class RegistoViewModel
    {
        public RECR_Registo Registo { get; set; }

        public List<ImageUpload> File { get; set; }

        public RegistoViewModel()
        {
            Registo = new RECR_Registo();
            File = new List<ImageUpload>();
        }


    }
}