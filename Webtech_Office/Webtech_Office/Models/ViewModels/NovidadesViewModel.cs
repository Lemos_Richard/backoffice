﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class NovidadesViewModel
    {
        public HOME_Novidades Novidades { get; set; }

        public List<HOME_NovidadesTexto> NovidadesTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public NovidadesViewModel()
        {

            Novidades = new HOME_Novidades();

            NovidadesTexto = new List<HOME_NovidadesTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}