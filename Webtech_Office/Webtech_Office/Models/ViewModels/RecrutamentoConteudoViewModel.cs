﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Webtech_Office.Models
{
    public class RecrutamentoConteudoViewModel
    {
        public RECR_Conteudo Conteudo {get;set;}
        public List<RECR_ConteudoTexto> ConteudoTexto { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }
       
        public AnexoViewModel Anexo { get; set; }


        public List<ImageUpload> File { get; set; }

    
        public RecrutamentoConteudoViewModel()
        {
            Conteudo = new RECR_Conteudo();
            ConteudoTexto = new List<RECR_ConteudoTexto>();
            Idioma = new List<CONF_Idioma>();
            File = new List<ImageUpload>();
            
            Anexo = new AnexoViewModel();
        }
   
    }
}