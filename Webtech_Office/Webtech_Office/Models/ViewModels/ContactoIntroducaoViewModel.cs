﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class ContactoIntroducaoViewModel
    {

        public CONT_ContactoIntroducao ContactoIntroducao { get; set; }

        public List<CONT_ContactoIntroducaoTexto> ContactoIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public ContactoIntroducaoViewModel()
        {

            ContactoIntroducao = new CONT_ContactoIntroducao();

            ContactoIntroducaoTexto = new List<CONT_ContactoIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}