﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class QuemSomosViewModel
    {

        public QUEM_Acerca QuemSomos { get; set; }

        public List<QUEM_AcercaTexto> QuemSomosTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public QuemSomosViewModel()
        {

            QuemSomos = new QUEM_Acerca();

            QuemSomosTexto = new List<QUEM_AcercaTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }


    }
}