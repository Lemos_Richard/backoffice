﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class LojaIntroducaoViewModel
    {


        public LOJA_LojaIntroducao LojaIntroducao { get; set; }

        public List<LOJA_LojaIntroducaoTexto> LojaIntroducaoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }



        public LojaIntroducaoViewModel()
        {

            LojaIntroducao = new LOJA_LojaIntroducao();

            LojaIntroducaoTexto = new List<LOJA_LojaIntroducaoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

        }
    }