﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class AnexoViewModel
    {
        public RECR_Anexo Anexo { get; set; }
        public List<RECR_AnexoTexto> AnexoTexto { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }
        public List<ImageUpload> File { get; set; }


        public AnexoViewModel()
        {
            Anexo = new RECR_Anexo();
            AnexoTexto = new List<RECR_AnexoTexto>();
            Idioma = new List<CONF_Idioma>();
            File = new List<ImageUpload>();

        }


    }
}