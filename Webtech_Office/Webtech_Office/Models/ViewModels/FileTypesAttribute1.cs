﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Webtech_Office.Models
{
    public class FileTypesAttribute : System.ComponentModel.DataAnnotations.ValidationAttribute
    {
        private readonly List<string> _types;

        public FileTypesAttribute(string types)
        {
            //_types = types.Split(',').ToList();
            _types.Add("pdf");
            _types.Add("doc");
            _types.Add("xls");
           
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;

            var fileExt = System.IO.Path.GetExtension((value as HttpPostedFileBase).FileName);
            Console.WriteLine(fileExt);
            return _types.Contains(fileExt, StringComparer.OrdinalIgnoreCase);
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("Tipo de ficheiro invalido. Apenas os seguintes tipos de ficheiros são suportados {0} são suportados.", String.Join(", ", _types));
        }
    }
}