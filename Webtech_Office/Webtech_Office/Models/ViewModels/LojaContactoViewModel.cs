﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webtech_Office.Models
{
    public class LojaContactoViewModel
    {

        public LOJA_Contacto LojaContacto { get; set; }

        public List<Loja_ContactoTexto> LojaContactoTexto { get; set; }

        public List<ImageUpload> File { get; set; }

        public List<FileDocumento> FileDocumento { get; set; }
        public List<CONF_Idioma> Idioma { get; set; }


        public LojaContactoViewModel()
        {

            LojaContacto = new LOJA_Contacto();

            LojaContactoTexto = new List<Loja_ContactoTexto>();

            File = new List<ImageUpload>();
            FileDocumento = new List<FileDocumento>();

            Idioma = new List<CONF_Idioma>();

        }

    }
}