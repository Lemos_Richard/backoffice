//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webtech_Office.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONT_Contacto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONT_Contacto()
        {
            this.CONT_ContactoTexto = new HashSet<CONT_ContactoTexto>();
        }
    
        public int Id { get; set; }
        public int Ordem { get; set; }
        public int TipoContactoId { get; set; }
        public Nullable<System.DateTime> HorarioInicio { get; set; }
        public Nullable<System.DateTime> HorarioFim { get; set; }
        public bool Activo { get; set; }
        public System.DateTime DataCriacao { get; set; }
        public System.DateTime DataModificacao { get; set; }
        public Nullable<int> Modificador { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONT_ContactoTexto> CONT_ContactoTexto { get; set; }
        public virtual CONT_TipoContacto CONT_TipoContacto { get; set; }
    }
}
