//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webtech_Office.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONT_OndeEstamosIntroducao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONT_OndeEstamosIntroducao()
        {
            this.CONT_OndeEstamosIntroducaoTexto = new HashSet<CONT_OndeEstamosIntroducaoTexto>();
        }
    
        public int Id { get; set; }
        public int Ordem { get; set; }
        public bool Activo { get; set; }
        public System.DateTime DataCriacao { get; set; }
        public System.DateTime DataModificacao { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONT_OndeEstamosIntroducaoTexto> CONT_OndeEstamosIntroducaoTexto { get; set; }
    }
}
