//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Webtech_Office.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONT_TipoContactoTexto
    {
        public int Id { get; set; }
        public int IdiomaId { get; set; }
        public int TipoContactoId { get; set; }
        public string Descricao { get; set; }
    
        public virtual CONF_Idioma CONF_Idioma { get; set; }
        public virtual CONT_TipoContacto CONT_TipoContacto { get; set; }
    }
}
